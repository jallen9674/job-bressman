<?php get_header(); ?>



	<div id="inner-content" class="wrapper">

			<div id="main" class="content-container">
				
				<div class="breadcrumbs-wrapper">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>

				<?php if (is_category()) { ?>
					<h1 class="archive-title h2">
						<span><?php _e( 'Posts Categorized:', 'bonestheme' ); ?></span> <?php single_cat_title(); ?>
						<?php //Display Page Number if Paginated
							 if( is_paged() ){
				              $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
				              echo '- Page ' . $currentPageNum;
				            }
						?>
					</h1>

				<?php } elseif (is_tag()) { ?>
					<h1 class="archive-title h2">
						<span><?php _e( 'Posts Tagged:', 'bonestheme' ); ?></span> <?php single_tag_title(); ?>
						<?php //Display Page Number if Paginated
							 if( is_paged() ){
				              $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
				              echo '- Page ' . $currentPageNum;
				            }
						?>
					</h1>

				<?php } elseif (is_author()) {
					global $post;
					$author_id = $post->post_author;
				?>
					<h1 class="archive-title h2">

						<span><?php _e( 'Posts By:', 'bonestheme' ); ?></span> <?php the_author_meta('display_name', $author_id); ?>
						<?php //Display Page Number if Paginated
							 if( is_paged() ){
				              $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
				              echo '- Page ' . $currentPageNum;
				            }
						?>

					</h1>
				<?php } elseif (is_day()) { ?>
					<h1 class="archive-title h2">
						<span><?php _e( 'Daily Archives:', 'bonestheme' ); ?></span> <?php the_time('l, F j, Y'); ?>
						<?php //Display Page Number if Paginated
							 if( is_paged() ){
				              $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
				              echo '- Page ' . $currentPageNum;
				            }
						?>
					</h1>

				<?php } elseif (is_month()) { ?>
						<h1 class="archive-title h2">
							<span><?php _e( 'Monthly Archives:', 'bonestheme' ); ?></span> <?php the_time('F Y'); ?>
							<?php //Display Page Number if Paginated
							 if( is_paged() ){
				              $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
				              echo '- Page ' . $currentPageNum;
				            }
						?>
						</h1>

				<?php } elseif (is_year()) { ?>
						<h1 class="archive-title h2">
							<span><?php _e( 'Yearly Archives:', 'bonestheme' ); ?></span> <?php the_time('Y'); ?>
							<?php //Display Page Number if Paginated
							 if( is_paged() ){
				              $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
				              echo '- Page ' . $currentPageNum;
				            }
						?>
						</h1>
				<?php } else { ?>
                    <h1 class="archive-title h2">
                        <?php the_archive_title(); ?>
                        <?php //Display Page Number if Paginated
                        if( is_paged() ){
                            $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
                            echo '- Page ' . $currentPageNum;
                        }
                        ?>
                    </h1>
                <?php } ?>

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">

					<header class="article-header">
						<h2 class="h2 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
						<span class="post-list-date"><?php echo 'Posted On: ' . get_the_date(); ?></span>&nbsp;|&nbsp;<span class="post-list-cat"><?php  echo 'Categorized as: ' . get_the_category_list(', ') ?></span>
					</header>

					<section class="entry-content cf">
						<?php
							if ( has_post_thumbnail() ) {
								the_post_thumbnail();
							}
						 ?>
						<?php my_excerpt(55); ?>
					</section>

				</article>

				</article>

				<hr>

				<?php endwhile; ?>

						<?php numeric_posts_nav(); ?>

				<?php else : ?>

						<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<p><?php _e( 'This is the error message in the archive.php template.', 'bonestheme' ); ?></p>
							</footer>
						</article>

				<?php endif; ?>

			</div>

		<?php get_sidebar(); ?>

	</div>




<?php get_footer(); ?>
