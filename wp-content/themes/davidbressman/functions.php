<?php
/******************************************************************
Square 4 Starter Theme
Built on Bones By Eddie Machado
******************************************************************/


/*--------------------------------------------
Brining in all of Bones, Start it Up!
---------------------------------------------*/

//Setting Folder Name
$themeFolderName = 'davidbressman';

//Bones Core
require_once( 'assets/bones.php' );

//Admin Modifications
require_once( 'assets/admin.php' );

function bones_ahoy() {

  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );

  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );

  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );

  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );

  // clean up comment styles in the head
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );

  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );

  // launching this stuff after theme setup
  bones_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'bones_register_sidebars' );

  // cleaning up random code around images
  add_filter( 'the_content', 'bones_filter_ptags_on_images' );

  // cleaning up excerpt
  add_filter( 'excerpt_more', 'bones_excerpt_more' );

} /* end bones ahoy */

// Get it Started! Miley style.
add_action( 'after_setup_theme', 'bones_ahoy' );


/*--------------------------------------------
Bring a Wrecking Ball to that Emoji
--------------------------------------------*/

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


/*--------------------------------------------
oEmbed Size Options
---------------------------------------------*/

if ( ! isset( $content_width ) ) {
	$content_width = 640;
}


/*--------------------------------------------
Thumbnail Sizes
---------------------------------------------*/


add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );
add_image_size( 'bones-thumb-page', 300, 150 );
add_image_size( 'faq-thumb', 300, 200, true );
add_image_size( 'faq-thumb-sidebar', 400, 250, true );

add_filter( 'image_size_names_choose', 'square4_nice_image_sizes' );

function square4_nice_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
        'bones-thumb-page' => __('300px by 150px'),

    ) );
}

/*--------------------------------------------
Declaring Sidebars/Widget Areas
---------------------------------------------*/

function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Default Sidebar', 'bonestheme' ),
		'description' => __( 'This is the default sidebar', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));


} //End bones_register_sidebars()


/*--------------------------------------------
Improved Comment Layout
---------------------------------------------*/

function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
  <div id="comment-<?php comment_ID(); ?>" <?php comment_class('cf'); ?>>
    <article  class="cf">
      <header class="comment-author vcard">
        <?php
        /*
          this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
          echo get_avatar($comment,$size='32',$default='<path_to_url>' );
        */
        ?>
        <?php // custom gravatar call ?>
        <?php
          // create variable
          $bgauthemail = get_comment_author_email();
        ?>
        <img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=40" class="load-gravatar avatar avatar-48 photo" height="40" width="40" src="<?php echo get_template_directory_uri(); ?>/assets/images/nothing.gif" />
        <?php // end custom gravatar call ?>
        <?php printf(__( '<cite class="fn">%1$s</cite> %2$s', 'bonestheme' ), get_comment_author_link(), edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ) ?>
        <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>

      </header>
      <?php if ($comment->comment_approved == '0') : ?>
        <div class="alert alert-info">
          <p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>
        </div>
      <?php endif; ?>
      <section class="comment_content cf">
        <?php comment_text() ?>
      </section>
      <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </article>
  <?php // </li> is added by WordPress automatically ?>
<?php
} //End bones_comments()




/*--------------------------------------------
Insert Attachment to Post
---------------------------------------------*/

function insert_attachment($file_handler,$post_id,$setthumb='false') {

  // check to make sure its a successful upload
  if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
  require_once(ABSPATH . "wp-admin" . '/includes/file.php');
  require_once(ABSPATH . "wp-admin" . '/includes/media.php');

  $attach_id = media_handle_upload( $file_handler, $post_id );

  if ($setthumb) update_post_meta($post_id,'_thumbnail_id',$attach_id);
  return $attach_id;
}



/*--------------------------------------------
Declaring Icons for CPTs
---------------------------------------------*/

  function add_menu_icons_styles(){ ?>
  <style>
      #adminmenu .menu-icon-bressman_reviews div.wp-menu-image:before { content: "\f313"; }
  </style>
  <?php }
  add_action( 'admin_head', 'add_menu_icons_styles' );


/*--------------------------------------------
Adding Parent Class to Menu Items
 - Used for mobile menu arrows
---------------------------------------------*/

add_filter( 'wp_nav_menu_objects', 'add_menu_parent_class' );
function add_menu_parent_class( $items ) {
  $parents = array();
  foreach ( $items as $item ) {
    if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
      $parents[] = $item->menu_item_parent;
    }
  }
  foreach ( $items as $item ) {
    if ( in_array( $item->ID, $parents ) ) {
      $item->classes[] = 'menu-parent-item';
    }
  }
  return $items;
}


/*--------------------------------------------
Bringing in CMB2
---------------------------------------------*/

require_once dirname( __FILE__ ) . '/assets/cmb2/init.php';

function update_cmb_meta_box_url( $url ) {
    $url = '/wp-content/themes/davidbressman/assets/cmb2';
    return $url;
}
add_filter( 'cmb2_meta_box_url', 'update_cmb_meta_box_url' );



/*--------------------------------------------
Preventing Null Search Queries
---------------------------------------------*/

add_filter( 'request', 'no_empty_search' );
function no_empty_search( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}


/*---------------------------------
Add Tag to Meta Description & Category
----------------------------------*/
function hennessey_description_tag($s){
 if( is_tag() ){
    $s .= single_tag_title(" Posts Tagged as: ", false);
 }
 if( is_category() ){
    $s .= single_cat_title(" Posts Categorized as: ", false);
 }
 return $s;
}
add_filter( 'wpseo_metadesc', 'hennessey_description_tag', 100, 1 );


/*--------------------------------------------
Adding Page Number to Description
---------------------------------------------*/

if ( ! function_exists( 't5_add_page_number' ) )
{
    function t5_add_page_number( $s )
    {
        global $page;
        $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
        ! empty ( $page ) && 1 < $page && $paged = $page;

        $paged > 1 && $s .= ' | ' . sprintf( __( 'Page: %s' ), $paged );

        return $s;
    }

    //add_filter( 'wp_title', 't5_add_page_number', 100, 1 );
    add_filter( 'wpseo_metadesc', 't5_add_page_number', 100, 1 );
}


/*--------------------------------------------
Adding Page Number to Title
---------------------------------------------*/

if ( ! function_exists( 'title_add_page_number' ) )
{
    function title_add_page_number( $s )
    {
        global $page;
        $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
        ! empty ( $page ) && 1 < $page && $paged = $page;

        $paged > 1 && $s .= ' | ' . sprintf( __( 'Page: %s' ), $paged );

        return $s;
    }

    add_filter( 'wpseo_title', 'title_add_page_number', 100, 1 );
}

/*--------------------------------------------
Custom Excerpt Length
---------------------------------------------*/

function my_excerpt($excerpt_length = 55, $id = false, $echo = true) {
    $text = '';
          if($id) {
                $the_post = & get_post( $my_id = $id );
                $text = ($the_post->post_excerpt) ? $the_post->post_excerpt : $the_post->post_content;
          } else {
                global $post;
                $text = ($post->post_excerpt) ? $post->post_excerpt : get_the_content('');
          }

                $text = strip_shortcodes( $text );
                $text = apply_filters('the_content', $text);
                $text = str_replace(']]>', ']]&gt;', $text);
                $text = strip_tags($text);
                $excerpt_more = ' ' . '... <a href="'.get_the_permalink().'">Read More &raquo;</a>';
                $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
                if ( count($words) > $excerpt_length ) {
                        array_pop($words);
                        $text = implode(' ', $words);
                        $text = $text . $excerpt_more;
                } else {
                        $text = implode(' ', $words);
                }
        if($echo)
        echo  apply_filters('the_content', $text);
        else
        return '<p>'.$text.'</p>';
}

function get_my_excerpt($excerpt_length = 55, $id = false, $echo = false) {
  return my_excerpt($excerpt_length, $id, $echo);
}


/*--------------------------------------------
Improved Page Navigation
---------------------------------------------*/

function numeric_posts_nav() {

  if( is_singular() )
    return;

  global $wp_query;

  /** Stop execution if there's only 1 page */
  if( $wp_query->max_num_pages <= 1 )
    return;

  $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
  $max   = intval( $wp_query->max_num_pages );

  /** Add current page to the array */
  if ( $paged >= 1 )
    $links[] = $paged;

  /** Add the pages around the current page to the array */
  if ( $paged >= 3 ) {
    $links[] = $paged - 1;
    $links[] = $paged - 2;
  }

  if ( ( $paged + 2 ) <= $max ) {
    $links[] = $paged + 2;
    $links[] = $paged + 1;
  }

  echo '<div class="pagination"><ul>' . "\n";

  /** Previous Post Link */
  if ( get_previous_posts_link() )
    printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

  /** Link to first page, plus ellipses if necessary */
  if ( ! in_array( 1, $links ) ) {
    $class = 1 == $paged ? ' class="active"' : '';

    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

    if ( ! in_array( 2, $links ) )
      echo '<li><span>...</span></li>';
  }

  /** Link to current page, plus 2 pages in either direction if necessary */
  sort( $links );
  foreach ( (array) $links as $link ) {
    $class = $paged == $link ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
  }

  /** Link to last page, plus ellipses if necessary */
  if ( ! in_array( $max, $links ) ) {
    if ( ! in_array( $max - 1, $links ) )
      echo '<li><span>...</span></li>' . "\n";

    $class = $paged == $max ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
  }

  /** Next Post Link */
  if ( get_next_posts_link() )
    printf( '<li>%s</li>' . "\n", get_next_posts_link() );

  echo '</ul></div>' . "\n";

}


/*--------------------------------------------
Grabbing Post Meta
---------------------------------------------*/
function getPostMeta($metaKey){
  if ( get_post_meta( $post->ID, '$metaKey', true ) ){
     return get_post_meta( $post->ID, '$metaKey', true );
  } else {
    return false;
  }
}


/*--------------------------------------------
Bringing in Post Types
---------------------------------------------*/

require_once('assets/post-types/reviews.php');
require_once('assets/post-types/faqs.php');

/*--------------------------------------------
Bringing in Shortcodes
---------------------------------------------*/

require_once('assets/shortcodes/reviews-homepage.php');
require_once('assets/shortcodes/reviews.php');
require_once('assets/shortcodes/sidebar-location-listing.php');
require_once('assets/shortcodes/sidebar-chat-live-button.php');

require_once('assets/shortcodes/locations.php');
/*--------------------------------------------
Get First Words of Review
---------------------------------------------*/

function get_words($sentence, $count = 10) {
  preg_match("/(?:\w+(?:\W+|$)){0,$count}/", $sentence, $matches);
  return $matches[0];
}



/*--------------------------------------------
Adding Business Schema
---------------------------------------------*/

function hennessey_add_business_schema(){
?>

  <script type='application/ld+json'>
  {
    "@context": "http://www.schema.org",
    "@type": "Attorney",
    "name": "Bressman Law",
    "url": "<?php echo site_url(); ?>",
    "sameAs": [
      "https://www.facebook.com/Bressman-Law-143111162614/",
      "https://twitter.com/dabressman",
      "https://www.linkedin.com/in/david-bressman-2a77258",
      "https://plus.google.com/100302340475839470715",
      "https://www.youtube.com/user/DavidBressman/videos"
    ],
    "logo": "<?php echo site_url(); ?>/wp-content/uploads/2013/08/BressmanLogo-420px.png",
    "image": "<?php echo site_url(); ?>/wp-content/uploads/2013/08/BressmanLogo-420px.png",
    "address": [
        {
          "@type": "PostalAddress",
          "streetAddress": "5186 Blazer Pkwy",
          "addressLocality": "Dublin",
          "addressRegion": "OH",
          "postalCode": "43017",
          "addressCountry": "United States"
        },
        {
          "@type": "PostalAddress",
          "streetAddress": "9435 Waterstone Blvd #130",
          "addressLocality": "Cincinnati",
          "addressRegion": "OH",
          "postalCode": "45249",
          "addressCountry": "United States"
        }
    ],
    "openingHoursSpecification" : {
      "@type" : "OpeningHoursSpecification",
      "dayOfWeek" : [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ],
      "opens" : "00:00:00",
      "closes" : "23:59:59"
    },
    "telephone": ["1-877-538-1116"],
    "priceRange": "$$"
  }

   </script>

<?php
}

add_action('wp_head', 'hennessey_add_business_schema');


/*---------------------------------
Adding Geo Tagging
----------------------------------*/
function hennessey_add_geo() {
?>

<meta name="zipcode" content="43016, 43017, 43002, 43004, 43016, 43017, 43026, 43035, 43054, 43065, 43068, 43081, 43082, 43085, 43109, 43110, 43119, 43123, 43125, 43137, 43147, 43201, 43202, 43203, 43204, 43205, 43206, 43207, 43209, 43210, 43211, 43212, 43213, 43214, 43215, 43217, 43219, 43220, 43221, 43222, 43223, 43224, 43227, 43228, 43229, 43230, 43231, 43232, 43235, 43240" />
<meta name="city" content="dublin, westerville, columbus, grove city, gahanna" />
<meta name="state" content="ohio" />
<meta name="country" content="usa, united states of america" />
<meta name="geo.region" content="US-OH" />
<meta name="geo.placename" content="Dublin" />
<meta name="geo.position" content="40.089229;-83.127907" />
<meta name="ICBM" content="40.089229, -83.127907" />

<?php
}

add_action('wp_head', 'hennessey_add_geo');

/*--------------------------------------------
Adding Google Analytics
---------------------------------------------*/


function hennessey_add_analytics() {
  ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WGN8DC9');</script>
<!-- End Google Tag Manager -->
  <?php
}

add_action('wp_head', 'hennessey_add_analytics');

/*--------------------------------------------
Adding Page Number to Title for Blogs
---------------------------------------------*/
function hennessey_output_page_number(){
  if( is_paged() ){
    $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
    return ' - Page ' . $currentPageNum;
  }
}

/*--------------------------------------------
Bringing in Related City Widget
---------------------------------------------*/

require_once('assets/modules/widget-location-list/location-module.php');

/*--------------------------------------------
Adding Lucky Orange
---------------------------------------------*/

function hennessey_add_lucky_orange() {
  ?>
  <script type='text/javascript'>
  window.__lo_site_id = 77236;

    (function() {
      var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
      wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
      })();
    </script>
  <?php
}

add_action('wp_footer', 'hennessey_add_lucky_orange');

/*--------------------------------------------
Adding Lucky Orange
---------------------------------------------*/

function hennessey_add_ctm(){
  ?>

  <script async src="//162956.tctm.co/t.js"></script>

  <?php
}

add_action('wp_footer', 'hennessey_add_ctm');

/*--------------------------------------------
Adding Facebook Tracking
---------------------------------------------*/

function hennessey_add_fb_tracking(){
  ?>
  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278171079428');
    fbq('init', '377269999735876');

    <?php if ( is_page(709) ) {
      echo "fbq('track', 'Lead');";
    } else {
        echo "fbq('track', 'PageView');";
    } ?>

  </script>
  <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278171079428&ev=PageView&noscript=1"
  /></noscript>
  <!-- End Facebook Pixel Code -->
  <?php
}

add_action('wp_footer', 'hennessey_add_fb_tracking');



// Related Pages Module
require_once('assets/modules/rs-related-pages-module.php');


/*--------------------------------------------
 __       _                  _          _
/ _\_ __ (_)_ __  _ __   ___| |_ ___   / \
\ \| '_ \| | '_ \| '_ \ / _ \ __/ __| /  /
_\ \ | | | | |_) | |_) |  __/ |_\__ \/\_/
\__/_| |_|_| .__/| .__/ \___|\__|___/\/
           |_|   |_|
---------------------------------------------*/

/*--------------------------------------------

Simple Loop for Custom Post Type


<?php
    $args = array(
        'posts_per_page' => 1,
        'post_type' => 'presidents_letter',
        'order' => 'DSC',
    );

    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
?>
<!-- MAIN CONTENT HERE -->
<?php endwhile; else : ?>
<!-- IF NOTHING FOUND CONTENT HERE -->
<?php endif; ?>
<?php wp_reset_query(); ?>

---------------------------------------------*/

/*--------------------------------------------

Alternate Page Limits For Things


function custom_query_additions( $query ) {

  // Check if on frontend and main query is modified
     if ( is_post_type_archive( 'presidents_letter' ) ) {
            // Display 50 posts for a custom post type called 'movie'
            $query->set( 'posts_per_page', 50 );
            return;
      }
  }
 add_action( 'pre_get_posts', 'custom_query_additions' );

---------------------------------------------*/


/* End of File */ ?>