<?php
/*
 Template Name: FAQ Listings
*/
?>

<?php get_header(); ?>

	<div id="inner-content" class="wrapper">

		<div id="main-full" class="content-container" role="main">

			<div class="breadcrumbs-wrapper">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?>>

				<header class="article-header">
					<h1 class="page-title"><?php the_title(); ?></h1>
				</header>

				<section class="entry-content cf" itemprop="articleBody">
					<?php
						the_content();
					?>
				</section>

				<?php comments_template(); ?>

			</article>

			<?php endwhile; else : ?>

					<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
						<footer class="article-footer">
								<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
						</footer>
					</article>

			<?php endif; ?>

			<?php //BEGIN FAQ DISPLAY ?>
	        <div class="faq-filter-portfolio-wrapper">

	            <div id="filters" class="faq-filter-category-list filters-button-group">
	                <?php //Output all Categories
	                echo '<button class="isotope-link is-checked" data-filter="*">All</button>';
	                 $terms = get_terms( 'bressman_faq');
	                 if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){

	                     foreach ( $terms as $term ) {
	                       echo '<button class="isotope-link" data-filter=".' . $term->slug . '"><span>' . $term->name . '</span></button>';

	                     }

	                 }
	                ?>
	            </div>

	            <div class="faq-filter-list">
	                <?php
	                		$faqTerms = get_terms( 'bressman_faq' );
						// convert array of term objects to array of term IDs
						$faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

	                    $args = array(
	                      'posts_per_page' => -1,
	                      'post_type' => 'page',
	                      'tax_query' => array(
								array(
									'taxonomy' => 'bressman_faq',
									'field' => 'term_id',
									'terms' => $faqTermIDs
								),
							),
	                      'order' => 'DSC',
	                    );

	                    $the_query = new WP_Query( $args );
	                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
	                  ?>
	                    <?php //Getting Category for Filtering
	                        $postTerms =  wp_get_object_terms($post->ID, 'bressman_faq');
	                        $categoryFilterSlug = '';
	                        $categoryPrettyName = '';
	                        if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
	                             foreach ( $postTerms as $term ) {
	                               $categoryFilterSlug .= ' ' . $term->slug;
	                               $categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
	                             }
	                         }
	                     ?>

	                    <div class="faq-filter-listing  grid-item <?php echo $categoryFilterSlug; ?> ">

		                    <a href="<?php the_permalink(); ?>" class="faq-filter-image-link">
		                    <?php
		                        if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		                            the_post_thumbnail('faq-thumb');
		                        } else{
		                        	echo '<img alt="faq thumb" src="' . get_template_directory_uri() . '/assets/images/default-faq-thumb.jpg" />';
		                        }
		                    ?>
							<span class="faq-date"><?php echo get_the_date('m-j-Y');  ?></span>
		                    </a>
		                   <div class="faq-meta">
		                    	<span class="faq-category"><?php echo $categoryPrettyName; ?></span>
		                    </div>
		                    <a href="<?php the_permalink(); ?>" class="faq-filter-title-link"><?php the_title(); ?></a>

							<a href="<?php the_permalink(); ?>" class="continue-reading-button">Continue Reading &raquo;</a>

	                    </div>
	                  <?php endwhile; else : ?>
	                    <!-- IF NOTHING FOUND CONTENT HERE -->
	                  <?php endif; ?>
	                  <?php wp_reset_query(); ?>
	            </div>

	        </div>
	    <?php //END faq DISPLAY ?>

		</div>

	</div>

<?php get_footer(); ?>
