<?php

    // Adding Taxonomy for FAQs
    register_taxonomy( 'bressman_faq',
        array('page'),
        array('hierarchical' => true,     /* if this is true, it acts like categories */
            'labels' => array(
                'name' => __( 'FAQ Category', 'bonestheme' ), /* name of the custom taxonomy */
                'singular_name' => __( 'Category', 'bonestheme' ), /* single taxonomy name */
                'search_items' =>  __( 'Search Categories', 'bonestheme' ), /* search title for taxomony */
                'all_items' => __( 'All Categories', 'bonestheme' ), /* all title for taxonomies */
                'parent_item' => __( 'Parent', 'bonestheme' ), /* parent title for taxonomy */
                'parent_item_colon' => __( 'Parent:', 'bonestheme' ), /* parent taxonomy title */
                'edit_item' => __( 'Edit Category', 'bonestheme' ), /* edit custom taxonomy title */
                'update_item' => __( 'Update Category', 'bonestheme' ), /* update title for taxonomy */
                'add_new_item' => __( 'Add New Category', 'bonestheme' ), /* add new title for taxonomy */
                'new_item_name' => __( 'New Category', 'bonestheme' ) /* name title for taxonomy */
            ),
            'show_admin_column' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'faq_cat' ),
            'show_in_rest' => true
        )
    );

?>