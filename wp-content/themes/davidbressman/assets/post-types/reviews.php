<?php
/*
Review Post Type
*/


// let's create the function for the custom type
function reviews_post_type() {
    // creating (registering) the custom type
    register_post_type( 'bressman_reviews', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
        // let's now add all the options for this post type
        array( 'labels' => array(
            'name' => __( 'Reviews', 'bonestheme' ), /* This is the Title of the Group */
            'singular_name' => __( 'Review', 'bonestheme' ), /* This is the individual type */
            'all_items' => __( 'All Reviews', 'bonestheme' ), /* the all items menu item */
            'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
            'add_new_item' => __( 'Add New Review', 'bonestheme' ), /* Add New Display Title */
            'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
            'edit_item' => __( 'Edit Review', 'bonestheme' ), /* Edit Display Title */
            'new_item' => __( 'New Review', 'bonestheme' ), /* New Display Title */
            'view_item' => __( 'View Review', 'bonestheme' ), /* View Display Title */
            'search_items' => __( 'Search Reviews', 'bonestheme' ), /* Search Custom Type Title */
            'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */
            'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
            ), /* end of arrays */
            'description' => __( 'These are reviews that appear on the reviews  page', 'bonestheme' ), /* Custom Type Description */
            'public' => false,
            'publicly_queryable' => false,
            'exclude_from_search' => true,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon' => '',  /* get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png'*/
            'rewrite'   => array( 'slug' => 'bressman_reviews', 'with_front' => false ), /* you can specify its url slug */
            'has_archive' => false, /* you can rename the slug here */
            'capability_type' => 'post',
            'hierarchical' => false,
            /* the next one is important, it tells what's enabled in the post editor */
            'supports' => array( 'title', 'revisions', 'thumbnail')
        ) /* end of options */
    ); /* end of register post type */

}

    // adding the function to the Wordpress init
    add_action( 'init', 'reviews_post_type');



add_action( 'cmb2_init', 'bressman_review_metaboxes' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function bressman_review_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_bressman';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $cmb_review = new_cmb2_box( array(
        'id'            => $prefix . 'metabox',
        'title'         => __( 'Review Information', 'cmb2' ),
        'object_types'  => array( 'bressman_reviews', ), // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );

    $cmb_review->add_field( array(
        'name' => 'Name',
        'desc' => 'The reviewer name ',
        'id'   => $prefix . '_review_name',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    $cmb_review->add_field( array(
        'name' => 'Rating',
        'desc' => 'A numerical rating 1 to 5',
        'id'   => $prefix . '_review_rating',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    $cmb_review->add_field( array(
        'name' => 'Feedback',
        'desc' => 'The content of the review',
        'id'   => $prefix . '_review_feedback',
        'type' => 'textarea',
    ) );


}
