<?php
/*---------------------------------
Sidebar Location Boxes
[bressman-sidebar-locations]
----------------------------------*/
function bressmanSidebarLocations($atts = null) {
    global $post;
    extract(shortcode_atts(array(
        'count' => '-1'
    ), $atts));
    ob_start();
    //BEGIN OUTPUT
?>


<div class="sidebar-location-listing">
  <a class="sidebar-location-listing__link" href="https://www.bressmanlaw.com/dublin-injury/">Dublin Location</a>
  <address class="sidebar-location-listing__address">
    5186 Blazer Pkwy<br>
    Dublin, OH 43017
  </address>
   <span class="sidebar-location-listing__phone">
    Phone: <a href="tel:+1-614-538-1116">614-538-1116</a>
   </span>
</div>

<div class="sidebar-location-listing">
  <a class="sidebar-location-listing__link" href="https://www.bressmanlaw.com/cincinnati-injury/">Cincinnati Location</a>
  <address class="sidebar-location-listing__address">
    9435 Waterstone Blvd #130<br>
    Cincinnati, OH 45249
  </address>
  <span class="sidebar-location-listing__phone">
    Phone: <a href="tel:+1-513-434-1818">513-434-1818</a>
  </span>
</div>


<div class="sidebar-location-listing">
  <a class="sidebar-location-listing__link no-hover">Columbus Location</a>
  <address class="sidebar-location-listing__address">
     64 Parsons Avenue<br>
     Columbus, Ohio 43215
  </address>
  <span class="sidebar-location-listing__phone">
    Phone: <a href="tel:+1-614-379-6669">614-379-6669</a>
  </span>
</div>

<style>.no-hover:hover { color: #e46a10 ; text-decoration: none; cursor: default; }</style>

<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('bressman-sidebar-locations', 'bressmanSidebarLocations');
?>
