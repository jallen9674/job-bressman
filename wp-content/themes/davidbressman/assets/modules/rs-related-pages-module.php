<?php

global $rs_faq_term;
global $rs_area_term;

$rs_area_term = 'hc_location';
$rs_faq_term = 'bressman_faq';

function removeCommonWords($input){

  $commonWords = array('a','an','and','I','it','is','do','does','for','from','go','how','the','etc','a','able','about','above','abroad','according','accordingly','across','actually','adj','after','afterwards','again','against','ago','ahead','ain\'t','all','allow','allows','almost','alone','along','alongside','already','also','although','always','am','amid','amidst','among','amongst','an','and','another','any','anybody','anyhow','anyone','anything','anyway','anyways','anywhere','apart','appear','appreciate','appropriate','are','aren\'t','around','as','a\'s','aside','ask','asking','associated','at','available','away','awfully','b','back','backward','backwards','be','became','because','become','becomes','becoming','been','before','beforehand','begin','behind','being','believe','below','beside','besides','best','better','between','beyond','both','brief','but','by','c','came','can','cannot','cant','can\'t','caption','cause','causes','certain','certainly','changes','clearly','c\'mon','co','co.','com','come','comes','concerning','consequently','consider','considering','contain','containing','contains','corresponding','could','couldn\'t','course','c\'s','currently','d','dare','daren\'t','definitely','described','despite','did','didn\'t','different','directly','do','does','doesn\'t','doing','done','don\'t','down','downwards','during','e','each','edu','eg','eight','eighty','either','else','elsewhere','end','ending','enough','entirely','especially','et','etc','even','ever','evermore','every','everybody','everyone','everything','everywhere','ex','exactly','example','except','f','fairly','far','farther','few','fewer','fifth','first','five','followed','following','follows','for','forever','former','formerly','forth','forward','found','four','from','further','furthermore','g','get','gets','getting','given','gives','go','goes','going','gone','got','gotten','greetings','h','had','hadn\'t','half','happens','hardly','has','hasn\'t','have','haven\'t','having','he','he\'d','he\'ll','hello','help','hence','her','here','hereafter','hereby','herein','here\'s','hereupon','hers','herself','he\'s','hi','him','himself','his','hither','hopefully','how','howbeit','however','hundred','i','i\'d','ie','if','ignored','i\'ll','i\'m','immediate','in','inasmuch','inc','inc.','indeed','indicate','indicated','indicates','inner','inside','insofar','instead','into','inward','is','isn\'t','it','it\'d','it\'ll','its','it\'s','itself','i\'ve','j','just','k','keep','keeps','kept','know','known','knows','l','last','lately','later','latter','latterly','least','less','lest','let','let\'s','like','liked','likely','likewise','little','look','looking','looks','low','lower','ltd','m','made','mainly','make','makes','many','may','maybe','mayn\'t','me','mean','meantime','meanwhile','merely','might','mightn\'t','mine','minus','miss','more','moreover','most','mostly','mr','mrs','much','must','mustn\'t','my','myself','n','name','namely','nd','near','nearly','necessary','need','needn\'t','needs','neither','never','neverf','neverless','nevertheless','new','next','nine','ninety','no','nobody','non','none','nonetheless','noone','no-one','nor','normally','not','nothing','notwithstanding','novel','now','nowhere','o','obviously','of','off','often','oh','ok','okay','old','on','once','one','ones','one\'s','only','onto','opposite','or','other','others','otherwise','ought','oughtn\'t','our','ours','ourselves','out','outside','over','overall','own','p','particular','particularly','past','per','perhaps','placed','please','plus','possible','presumably','probably','provided','provides','q','que','quite','qv','r','rather','rd','re','really','reasonably','recent','recently','regarding','regardless','regards','relatively','respectively','right','round','s','said','same','saw','say','saying','says','second','secondly','see','seeing','seem','seemed','seeming','seems','seen','self','selves','sensible','sent','serious','seriously','seven','several','shall','shan\'t','she','she\'d','she\'ll','she\'s','should','shouldn\'t','since','six','so','some','somebody','someday','somehow','someone','something','sometime','sometimes','somewhat','somewhere','soon','sorry','specified','specify','specifying','still','sub','such','sup','sure','t','take','taken','taking','tell','tends','th','than','thank','thanks','thanx','that','that\'ll','thats','that\'s','that\'ve','the','their','theirs','them','themselves','then','thence','there','thereafter','thereby','there\'d','therefore','therein','there\'ll','there\'re','theres','there\'s','thereupon','there\'ve','these','they','they\'d','they\'ll','they\'re','they\'ve','thing','things','think','third','thirty','this','thorough','thoroughly','those','though','three','through','throughout','thru','thus','till','to','together','too','took','toward','towards','tried','tries','truly','try','trying','t\'s','twice','two','u','un','under','underneath','undoing','unfortunately','unless','unlike','unlikely','until','unto','up','upon','upwards','us','use','used','useful','uses','using','usually','v','value','various','versus','very','via','viz','vs','w','want','wants','was','wasn\'t','way','we','we\'d','welcome','well','we\'ll','went','were','we\'re','weren\'t','we\'ve','what','whatever','what\'ll','what\'s','what\'ve','when','whence','whenever','where','whereafter','whereas','whereby','wherein','where\'s','whereupon','wherever','whether','which','whichever','while','whilst','whither','who','who\'d','whoever','whole','who\'ll','whom','whomever','who\'s','whose','why','will','willing','wish','with','within','without','wonder','won\'t','would','wouldn\'t','x','y','yes','yet','you','you\'d','you\'ll','your','you\'re','yours','yourself','yourselves','you\'ve','z','zero', 'will', 'long');

  return preg_replace('/\b('.implode('|',$commonWords).')\b/','',$input);
}

function getRelatedIDs($post, $type = 'both') {
  global $post;
  global $wpdb;

      // normalize title and convert it to array
      $title_words = explode(' ',
        preg_replace('/\?/', '',
          removeCommonWords(
            strtolower($post->post_title))));

      $quote_ids = [];
      foreach ($title_words as $tk => $word) {
        if($type == 'both') {
          $search_query = 'SELECT ID FROM wp_posts WHERE (post_type = "page" OR post_type = "post") AND post_title LIKE %s';
        } else {
          $search_query = 'SELECT ID FROM wp_posts WHERE post_type = "'.$type.'" AND post_title LIKE %s';
        }

        $like = '% '.$word.' %';
        $results = $wpdb->get_results($wpdb->prepare($search_query, $like), ARRAY_N);
        foreach($results as $key => $array){
          $quote_ids[$word][] = $array[0];
        }
      }

      uksort($quote_ids, function($a, $b) { return count($b) - count($a); });

      $three_more = array_filter($quote_ids, function($item){
        return count($item) >= 3;
      });

      $out = count($three_more) ? array_pop($three_more) : array_shift($quote_ids);

      $out = array_diff($out, [$post->ID]);

      return $out;
}



// ADD CUSTOM FIELDS IF NOT EXIST


if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
  'key' => 'group_5ba19af7b124d',
  'title' => 'Related FAQs',
  'fields' => array(
    array(
      'key' => 'field_5b0d876a0c8f9',
      'label' => 'Search Word',
      'name' => 'search_word',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'formatting' => 'none',
      'maxlength' => '',
    ),
    array(
      'key' => 'field_5b0d89dd29409',
      'label' => 'Related Page 1',
      'name' => 'related_page_1',
      'type' => 'post_object',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'post_type' => '',
      'taxonomy' => '',
      'allow_null' => 1,
      'multiple' => 0,
      'return_format' => 'object',
      'ui' => 1,
    ),
    array(
      'key' => 'field_5ba275e581a79',
      'label' => 'Related Page 2',
      'name' => 'related_page_2',
      'type' => 'post_object',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'post_type' => '',
      'taxonomy' => '',
      'allow_null' => 1,
      'multiple' => 0,
      'return_format' => 'object',
      'ui' => 1,
    ),
    array(
      'key' => 'field_5ba275e881a7a',
      'label' => 'Related Page 3',
      'name' => 'related_page_3',
      'type' => 'post_object',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'post_type' => '',
      'taxonomy' => '',
      'allow_null' => 1,
      'multiple' => 0,
      'return_format' => 'object',
      'ui' => 1,
    ),
  ),
  'location' => array(
    array(
      array(
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'page',
      ),
    ),
    array(
      array(
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'post',
      ),
    )
  ),
  'menu_order' => 0,
  'position' => 'side',
  'style' => 'seamless',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => 1,
  'description' => '',
));

endif;


// END


// SIDEBAR SHORTCODE

add_filter( 'posts_where', 'wpse18703_rs_posts_where', 10, 2 );
function wpse18703_rs_posts_where( $where, &$wp_query )
{
    global $wpdb;
    if ( $wpse18703_title = $wp_query->get( 'rs_title_search' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( $wpdb->esc_like( $wpse18703_title ) ) . '%\'';
    }
    return $where;
}

function randomFaqRsSidebar($atts = null) {

    extract(shortcode_atts(array(
      'amount' => '',
      'rs_post_id' => ''
   ), $atts));

    $queryAmount = $amount;

    $global_post_id = isset($rs_post_id) ? $rs_post_id : $post->ID;


    global $post;
    global $rs_faq_term;

    $rs_search_word = function_exists('get_field') ? get_field('field_5b0d876a0c8f9', $global_post_id) : false;
    $rs_related_page_1 = function_exists('get_field') ? get_field('field_5b0d89dd29409', $global_post_id) : false;
    $rs_related_page_2 = function_exists('get_field') ? get_field('field_5ba275e581a79', $global_post_id) : false;
    $rs_related_page_3 = function_exists('get_field') ? get_field('field_5ba275e881a7a', $global_post_id) : false;

    if($rs_related_page_1){
      $rs_related_page_1 = $rs_related_page_1->ID;
    }
    if($rs_related_page_2){
      $rs_related_page_2 = $rs_related_page_2->ID;
    }
    if($rs_related_page_3){
      $rs_related_page_3 = $rs_related_page_3->ID;
    }

    $heading = $rs_search_word || ($rs_related_page_1 || $rs_related_page_2 || $rs_related_page_3) ? 'Related Pages' : 'Frequently Asked Questions';

    ob_start();
    //BEGIN OUTPUT

?>

<?php //if($rs_search_word || ($rs_related_page_1 || $rs_related_page_2 || $rs_related_page_3)) { ?>

    <div class="random-faq-loop  faq-filter-portfolio-wrapper">
        <?php $faqTerms = get_terms( 'bressman_faq' );
        // convert array of term objects to array of term IDs
        $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

        //Alternate Args for Blog Archive Pages to Prevent Weird Results
        if (is_archive() || is_home()){
           $args = array(
            'posts_per_page' => $queryAmount,
            'post_type' => array('page'),
            'tax_query' => array(
                  array(
                      'taxonomy' => 'bressman_faq',
                      'field' => 'term_id',
                      'terms' => 'general'
                  ),
              ),
            'order' => 'DSC',
            'orderby' => 'rand',
           // 'post__in' => count(getRelatedIDs($post, 'page')) >= 3 ? getRelatedIDs($post, 'page') : array()
          );
        } else if ($faqTermIDs) {
          $args = array(
            'posts_per_page' => $queryAmount,
            'post_type' => array('page'),
            'tax_query' => array(
                  array(
                      'taxonomy' => 'bressman_faq',
                      'field' => 'term_id',
                      'terms' => $faqTermIDs
                  ),
              ),
            'order' => 'DSC',
            'orderby' => 'rand',
            'post__in' => count(getRelatedIDs($post, 'page')) >= 3 ? getRelatedIDs($post, 'page') : array()
          );
        }

        if(count(get_posts($args) < 3)) {
          unset($args['post__in']);
        }



        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
      ?>
        <?php //Getting Category for Filtering
            $postTerms =  wp_get_object_terms($post->ID, $rs_faq_term);
            $categoryFilterSlug = '';
            $categoryPrettyName = '';
            if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
                 foreach ( $postTerms as $term ) {
                   $categoryFilterSlug .= ' ' . $term->slug;
                   $categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
                 }
             }
         ?>

        <a href="<?php the_permalink(); ?>" class="faq-page-listing">

            <div class="faq-filter-image-link">
            <?php
                if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                    the_post_thumbnail('large');
                } else{
                    echo '<img alt="faq thumb" src="' . get_template_directory_uri() . '/assets/images/default-faq-thumb.jpg" />';
                }
            ?>
            </div>
           <div class="faq-meta">
                <span class="faq-category"><?php echo $categoryPrettyName; ?></span>
            </div>
            <div class="faq-filter-title-link"><?php the_title(); ?></div>

            <div class="continue-reading-button">Continue Reading &raquo;</div>

        </a>
          <?php endwhile; else : ?>
            Nothing to display.
          <?php endif; ?>
          <?php wp_reset_query(); ?>

    </div> <!-- end .random-faq-loop -->

<?php


// } else {
//   echo do_shortcode( '[random-faq-loop-image amount="3"]' );
// }


    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}


add_shortcode('random-faq-rs-sidebar', 'randomFaqRsSidebar');


// END SIDEBAR SHORTCODE //



// CONTENT HOOK


add_filter('the_content', 'rs_related_page_content');
function rs_related_page_content( $content ){
  global $post;
  global $wpdb;
  global $rs_faq_term;
  global $rs_area_term;

  $rs_ignore_pages = array();

  $terms = wp_get_post_terms( $post->ID, $rs_faq_term );

  if(!$terms) {
    $rs_faq_temp = $rs_faq_term;
    $rs_faq_term = $rs_area_term;
    $terms = wp_get_post_terms($post->ID, $rs_area_term);
  }

  if(!is_single() && !$terms) return $content;

  $rs_search_word = function_exists('get_field') ? get_field('field_5b0d876a0c8f9', $post->ID) : false;
  $rs_related_page_1 = function_exists('get_field') ? get_field('field_5b0d89dd29409', $post->ID) : false;
  $rs_related_page_2 = function_exists('get_field') ? get_field('field_5ba275e581a79', $post->ID) : false;
  $rs_related_page_3 = function_exists('get_field') ? get_field('field_5ba275e881a7a', $post->ID) : false;

  $exact_posts = array();

  if($rs_related_page_1){
    $exact_posts[] = $rs_related_page_1->ID;
  }
  if($rs_related_page_2){
    $exact_posts[] = $rs_related_page_2->ID;
  }
  if($rs_related_page_3){
    $exact_posts[] = $rs_related_page_3->ID;
  }

  //if($rs_search_word || $exact_posts) {
   global $post;
    $args = array(
      'posts_per_page' => 3,
      'post_type' => array('page', 'post'),
      'order' => 'DSC',
      'orderby' => 'rand',
      'post__not_in' => array($post->ID)
    );
    if($exact_posts) {

      $args['post__in'] = $exact_posts;

    } else if($rs_search_word) {

      $search_query = 'SELECT ID FROM wp_posts WHERE (post_type = "page" OR post_type = "post") AND post_title LIKE %s';
      $like = '%'.$rs_search_word.'%';
      $results = $wpdb->get_results($wpdb->prepare($search_query, $like), ARRAY_N);
      foreach($results as $key => $array){
           $quote_ids[] = $array[0];
      }

      $args['post__in'] = $quote_ids;
    } else {

      $args['post__in'] = getRelatedIDs($post);
    }

    if($terms) {
      $faqTerms = get_terms( $rs_faq_term );
      $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );
      $args['tax_query'] = array(
        array(
          'taxonomy' => $rs_faq_term,
          'field' => 'term_id',
          'terms' => $faqTermIDs
        )
      );
      $args['post__in'] = getRelatedIDs($post, 'page');
    }

    preg_match_all('/<h2>/', $content, $matches, PREG_OFFSET_CAPTURE);

    if(!$matches[0] && is_single()) {
      preg_match_all('/<p>.{300,}?<\/p>/', $content, $matches, PREG_OFFSET_CAPTURE);
    }

    $exact_posts_query = get_posts($args);

    if(!$exact_posts_query) {
      $args['post__in'] = array();
      $exact_posts_query = get_posts($args);
    }

    if($matches[0]) {
        foreach(array_reverse($matches[0]) as $k => $match) {
            if($exact_posts_query[$k] && $match[1]) {
                $content = substr_replace($content, '<p class="rs-related-link-block">Related: <a href="'.get_permalink($exact_posts_query[$k]->ID).'">'.$exact_posts_query[$k]->post_title.'</a></p>', $match[1], 0);
            }
        }
    }

    $content .= '<style>p.rs-related-link-block {background: #ddd;padding: 10px 20px;border-radius: 5px;}</style>';
  //}

  if(isset($rs_faq_temp)) $rs_faq_term = $rs_faq_temp;

  return $content;
}


// END CONTENT HOOK //

?>