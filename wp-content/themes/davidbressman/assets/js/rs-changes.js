// lazy loading iframes
function rsInitIframes() {
var vidDefer = document.getElementsByTagName('iframe');
for (var i=0; i<vidDefer.length; i++) {
if(vidDefer[i].getAttribute('data-src')) {
vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
} } 
}
window.onload = rsInitIframes;

(function($){



	Array.prototype.clean = function(deleteValue) {
	  for (var i = 0; i < this.length; i++) {
	    if (this[i] == deleteValue) {
	      this.splice(i, 1);
	      i--;
	    }
	  }
	  return this;
	};

	function removeDuplicates(e,r){for(var n={},o=0,t=e.length;t>o;o++)n[e[o][r]]||(n[e[o][r]]=e[o]);var u=[];for(var c in n)u.push(n[c]);return u}


	$(document).ready(function(){
		if($('.location-widget-links.location-listing a').length) {

			var links = [];
			var core = [
				"Bicycle Accidents",
				"Birth Injuries",
				"Bus Accidents",
				"Car Accidents",
				"Construction Accidents",
				"Dog Bites",
				"Drunk Driving Accidents",
				"Medical Malpractice",
				"Motorcycle Accidents",
				"Nursing Home Abuse",
				"Pedestrian Accidents",
				"Premises Liability",
				"Slip and Fall Accidents",
				"Social Security Disability",
				"Swimming Pool Accidents",
				"Taxicab Accidents",
				"Traumatic Brain Injuries",
				"Truck Accidents",
				"Uninsured Motorist Accidents",
				"Unsafe Premises and Property Negligence",
				"Workers’ Compensation",
				"Wrongful Death",
			];

			$('.location-widget-links.location-listing a').each(function(){
				var el = this
				// core.forEach(function(item){
				// 	if($(el).text().trim() === item) {
						links.push({url: $(el).attr('href'), title: $(el).text().trim()})
				// 	}
				// })
			})

			var html = '';
			removeDuplicates(links, 'title').forEach(function(item, i){
				html += '<li><a style="letter-spacing: 0;" href="'+item.url+'">'+item.title+'</a></li>'
			})

			$('.menu-item-28 > a + .sub-menu').html(html)
			// $('.menu-item-28 > a + .sub-menu').width('auto');
			$('.menu-item-28 > a').attr('href', '#');
		}

	});

})(jQuery)