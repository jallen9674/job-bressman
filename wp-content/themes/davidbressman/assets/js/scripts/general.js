/*--------------------------------------------
Square4 General JavaScript
---------------------------------------------*/


/*--------------------------------------------
Bringing Everything for Koala Concat + Minify
-- May change back to gulp we will see.
---------------------------------------------*/
// @prepros-prepend "../vendor/jquery.mCustomScrollbar.js"
// @prepros-prepend "../vendor/jquery.touchMenuHover.js"
// @prepros-prepend "../vendor/mousewheelStopPropagation.js"
// @prepros-prepend "../vendor/parsley.js"
// @prepros-prepend "../vendor/slidebars.js"
// @prepros-prepend "../vendor/simpleparallax.js"
// @prepros-prepend "../vendor/flexslider.js"
// @prepros-prepend "../vendor/lightbox.js"
// @prepros-prepend "../vendor/isotope.js"


/*--------------------------------------------
 ___ _            _     ___ _     _   _
 / __| |_ __ _ _ _| |_  |_ _| |_  | | | |_ __
 \__ \  _/ _` | '_|  _|  | ||  _| | |_| | '_ \
 |___/\__\__,_|_|  \__| |___|\__|  \___/| .__/
                                        |_|
---------------------------------------------*/


jQuery(document).ready(function($) {

    /*--------------------------------------------
    Simple Throttle Function Awesome
    - https://medium.com/@_jh3y/throttling-and-debouncing-in-javascript-b01cad5c8edf
    ---------------------------------------------*/

    var throttle = function(func, limit) {
        var inThrottle = undefined;
        return function() {
            var args = arguments,
                context = this;
            if (!inThrottle) {
                func.apply(context, args);
                inThrottle = true;
                return setTimeout(function() {
                    return inThrottle = false;
                }, limit);
            }
        };
    };

    /*--------------------------------------------
    On Resize, Calculate Height of Desktop Header and Set Padding on Container As Appropriate
    ---------------------------------------------*/

    if (!$('body').hasClass('page-template-page-homepage-php')) {

        //Update on Page Load
        if ($(window).width() > 767) {
            var containerTopPadding = ($('.desktop-header').outerHeight()) + 30;
            $('#container').css('padding-top', containerTopPadding);
        }

        //Update on Resize
        $(window).on('resize', throttle(function() {

            if ($(window).width() > 767) {
                var containerTopPadding = ($('.desktop-header').outerHeight()) + 30;
                $('#container').css('padding-top', containerTopPadding);
            }

        }, 100));

    }


    /*--------------------------------------------
    Initiation of Lightbox for Infographic
    ---------------------------------------------*/
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true,
        'fitImagesInViewport': false
    })

    /*--------------------------------------------
    Initiation of Homepage Testimonial Slider
    ---------------------------------------------*/

    $('.homepage-testimonial-slider').flexslider({
        animation: "fade",
        controlNav: false,
        directionNav: false
    });


    /*--------------------------------------------
    Second Level Menu Items Applying Styles to Parent
    ---------------------------------------------*/

    $(".nav ul li.current-menu-item").parent().parent().children("a").addClass("currentpage-styles");


    /*--------------------------------------------
    Two Column Auto Split List
    ---------------------------------------------*/

    $(function() {
        $("ul.two-column-list").each(function() {
            var list = $(this);
            var size_a = $(this).children($("li")).size();
            var current_size_a = 0;
            if (size_a % 2 === 0) {
                size_a = (size_a / 2);
            } else {
                size_a = (size_a / 2) + 1;
            }
            list.children().each(function() {
                //console.log(current_size_a + ": " + $(this).text());

                if (++current_size_a > size_a) {
                    var new_list = $("<ul class='two-column-list fancy-list last-col'></ul>").insertAfter(list);
                    list = new_list;
                    current_size_a = 1;
                }
                list.append(this);
            });
        });
    });

    /*--------------------------------------------
    Fade in Header on Scroll
    ---------------------------------------------*/

    var scrollHeader = $(window).scrollTop();

    if (scrollHeader >= 250) {
        $(".desktop-scrolled-header").addClass("scrolled");
    } else {
        $(".desktop-scrolled-header").removeClass("scrolled");
    }

    $(window).scroll(function() {
        var scrollHeader = $(window).scrollTop();

        if (scrollHeader >= 250) {
            $(".desktop-scrolled-header").addClass("scrolled");
        } else {
            $(".desktop-scrolled-header").removeClass("scrolled");
        }
    });


    /*--------------------------------------------
    On Homepage Fade Header to Black
    ---------------------------------------------*/

    var scrollHomepageHeader = $(window).scrollTop();

    if (scrollHomepageHeader >= 2) {
        $(".desktop-header").addClass("scrolled");
    } else {
        $(".desktop-header").removeClass("scrolled");
    }

    $(window).scroll(function() {
        var scrollHomepageHeader = $(window).scrollTop();

        if (scrollHomepageHeader >= 2) {
            $(".desktop-header").addClass("scrolled");
        } else {
            $(".desktop-header").removeClass("scrolled");
        }
    });


    /*--------------------------------------------
    Display Mobile Click to Call on Scroll
    ---------------------------------------------*/

    $(window).on('scroll', function() {
        if ($(window).scrollTop() >= 300) {
            $('.footer-phone-circle').addClass('visible');
        } else {
            $('.footer-phone-circle').removeClass('visible');
        }
    });


    //Close Button
    $('.mobile-contact-close').on("click", function() {
        $('.mobile-contact-box').addClass('closed');
        $('.footer-phone-circle').addClass('visible');
    });

    /*--------------------------------------------
    Adding Drop Down Arrows to Menu Items
    ---------------------------------------------*/

    $('.nav.top-nav .menu-parent-item a').append('<span class="desktop-downarrow"></span>');


    /*--------------------------------------------
    Adding Nice Scroll Bar to Slideout Nav
    ---------------------------------------------*/


    $(".sb-slidebar").mCustomScrollbar({
        scrollButtons: {
            enable: false
        },
        scrollbarPosition: 'inside',
        scrollInertia: 0,
        updateOnContentResize: true
    });


    /*--------------------------------------------
    Mobile CSS Dropdown Fix
    ---------------------------------------------*/

    $('.menu-parent-item').touchMenuHover();


    /*--------------------------------------------
    Slider Navigation
     -  http://plugins.adchsm.me/slidebars/usage.php
    ---------------------------------------------*/

    $.slidebars({
        siteClose: true,
        disableOver: 767
    });


    /*--------------------------------------------
    Stop Mouse Scroll Propagation on Slider Nav
     - https://github.com/basselin/jquery-mousewheel-stop-propagation
    ---------------------------------------------*/

    $('.sb-left').mousewheelStopPropagation();


    /*--------------------------------------------
    Scroll to Top Button
    ---------------------------------------------*/

    $(window).scroll(function() {
        if ($(this).scrollTop() >= 50) { // If page is scrolled more than 50px
            $('#return-to-top').addClass('top-visible'); // Fade in the arrow
        } else {
            $('#return-to-top').removeClass('top-visible'); // Else fade out the arrow
        }
    });
    $('#return-to-top').click(function() { // When arrow is clicked
        $('body,html').animate({
            scrollTop: 0 // Scroll to top of body
        }, 500);
    });


    /*--------------------------------------------
    Do Something on Class Change
    ---------------------------------------------*/

    // Create a closure
    (function() {
        // Your base, I'm in it!
        var originalAddClassMethod = jQuery.fn.addClass;

        jQuery.fn.addClass = function() {
            // Execute the original method.
            var result = originalAddClassMethod.apply(this, arguments);

            // trigger a custom event
            jQuery(this).trigger('cssClassChanged');

            // return the original result
            return result;
        }
    })();

    // document ready function
    $(function() {
        $(".your-name input, .your-email input, .your-message input, .your-phone input").bind('cssClassChanged', function() {
            if ($(this).hasClass('valid')) {
                $(this).parent().addClass('valid');
                $(this).parent().removeClass('error');
            }
            if ($(this).hasClass('error')) {
                $(this).parent().addClass('error');
                $(this).parent().removeClass('valid');
            }

        });
    });


    /*--------------------------------------------
    Smooth Scroll To Same Page Anchor
    -https://css-tricks.com/snippets/jquery/smooth-scrolling/
    ---------------------------------------------*/


    // Select all links with hashes
    $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top - 250
                    }, 1000, function() {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });




}); /* end of as page load scripts */



/*-----------------------------------------------
Initializing things that rely on full CSS load
to render correctly (Isotope etc.)
-----------------------------------------------*/

jQuery(window).bind('load', function($) {

    /*****************************************
    Delaying Lucky Orange Loading
    *****************************************/

    function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.innerHTML = " window.__lo_site_id = 77236;(function() { var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true; wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);})();";
        document.body.appendChild(element);
    }

    if (window.addEventListener) window.addEventListener("load", downloadJSAtOnload, false);
    else if (window.attachEvent) window.attachEvent("onload", downloadJSAtOnload);
    else window.onload = downloadJSAtOnload;

}); //End Window Load Scripts


jQuery(window).bind('load', function($){

/*-----------------------------------------------
Isotope Video FAQ Filtering
-----------------------------------------------*/

  var $container = jQuery('.faq-filter-list').isotope({
    itemSelector: '.grid-item',
    layoutMode: 'fitRows',
    getSortData: {
      category: '[data-category]'
    },
    hiddenClass: 'isotope-hidden',
  });

// filter functions
  var filterFns = {

  };

  var itemReveal = Isotope.Item.prototype.reveal;
Isotope.Item.prototype.reveal = function() {
  itemReveal.apply( this, arguments );
  jQuery( this.element ).removeClass('isotope-hidden');
};

var itemHide = Isotope.Item.prototype.hide;
Isotope.Item.prototype.hide = function() {
  itemHide.apply( this, arguments );
  jQuery( this.element ).addClass('isotope-hidden');
};

  // bind filter button click
  jQuery('#filters').on( 'click', 'button', function() {

    var filterValue = jQuery( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $container.isotope({ filter: filterValue });



  });

  // change is-checked class on buttons
  jQuery('.filters-button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = jQuery( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      jQuery( this ).addClass('is-checked');
    });
  });


  $container.on( 'arrangeComplete',  function( event, filteredItems ) {
    //console.log( 'Isotope arrange completed on ' + filteredItems.length + ' items' );
    //equalheight('.faq-filter-listing');
  });

  $container.on( 'layoutComplete',  function( event, filteredItems ) {
    console.log( 'Layout Complete Isotope arrange completed on ' + filteredItems.length + ' items' );
    //jQuery('.faq-filter-listing').addClass('sorted');

     if(jQuery('.isotope-link:nth-of-type(1)').hasClass('is-checked')){
      jQuery('.faq-filter-listing').removeClass('sorted');
     }

     equalheight('.faq-filter-listing:not(.isotope-hidden)');

  });


  //Equal Height Columns
  equalheight('.faq-filter-listing:not(.isotope-hidden)');


/*-----------------------------------------------
Masonry Review Grid Layout
-----------------------------------------------*/

jQuery('.testimonial-grid').masonry({
  // options
  itemSelector: '.single-review',
  columnWidth: 320
});

});



jQuery(window).resize(function(){
  //equalheight('.faq-filter-listing');
});


equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;

 jQuery(container).each(function() {

   $el = jQuery(this);
   jQuery($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}


/*-----------------------------------------------
Deferred Loading External Javascript for PageSpeed
 - Currently Disabled
-----------------------------------------------*/

/*
 function downloadJSAtOnload() {
   var element1 = document.createElement("script");
   element1.src = "https://thelivechatsoftware.com/Dashboard/cwgen/scripts/library.js";
   document.body.appendChild(element1);
 }

 // Check for browser support of event handling capability
 if (window.addEventListener)
 window.addEventListener("load", downloadJSAtOnload, false);
 else if (window.attachEvent)
 window.attachEvent("onload", downloadJSAtOnload);
 else window.onload = downloadJSAtOnload;
*/