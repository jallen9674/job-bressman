<div id="sidebar" class="sidebar content-container">

	<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar1' ); ?>

	<?php else : ?>

		<?php
			/*
			 * This content shows up if there are no widgets defined in the backend.
			*/
		?>

		<div class="no-widgets">
			<p>These are not the widgets you are looking for...</p>
		</div>

	<?php endif; ?>

</div>
