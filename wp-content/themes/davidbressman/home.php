<?php get_header(); ?>

	<div id="inner-content" class="wrapper">
			<div id="main" class="content-container">
				<header>
					<h1 class="page-title">Blog
						<?php //Display Page Number if Paginated
						 if( is_paged() ){
			              $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
			              echo '- Page ' . $currentPageNum;
			            }
						?>
					</h1>
				</header>

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> >

					<header class="article-header">
						<h2 class="h2 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
						<span class="post-list-date"><?php echo 'Posted On: ' . get_the_date(); ?></span>&nbsp;|&nbsp;<span class="post-list-cat"><?php  echo 'Categorized as: ' . get_the_category_list(', ') ?></span>
					</header>

					<section class="entry-content cf">
						<?php
							if ( has_post_thumbnail() ) {
								the_post_thumbnail();
							}
						 ?>
						<?php my_excerpt(55); ?>
					</section>

				</article>

				<hr>

				<?php endwhile; ?>

						<?php numeric_posts_nav(); ?>

				<?php else : ?>

						<article id="post-not-found" class="hentry cf">
								<header class="article-header">
									<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
								<section class="entry-content">
									<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<!-- This is the index.php template -->
							</footer>
						</article>

				<?php endif; ?>

			</div>

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>
