

		</div> <?php // end #container ?>

		<footer class="footer page-footer" id="page-footer" >

			<div id="inner-footer" class="wrapper page-footer__inner">

				<h2 class="page-footer__section-title">
					<span class="page-footer__section-title-inner">
						Contact Us For a Free Case Evaluation
					</span>
				</h2>

				<div class="row">
					<div class="one-half">
						<div class="row">
							<div class="one-half">
								<div class="page-footer__contact-form-wrapper">
									<?php echo do_shortcode('[contact-form-7 id="2412" title="Homepage Contact Form" html_class="footer-contact-form standard-form"]'); ?>
								</div>
							</div>
							<div class="one-half">

								<div class="page-footer__address-box">
									<span class="page-footer__address-name"><a href="https://www.bressmanlaw.com/dublin-injury/" title="Dublin Personal Injury Lawyer" style="color:rgb(228, 106, 16);">Dublin Location</a></span>
									<address class="page-footer_address-location">
										5186 Blazer Pkwy<br>
										Dublin, OH 43017<br>
										Phone: <a href="tel:+1-614-538-1116">614-538-1116</a>
									</address>
									<div class="iframe-wrapper page-footer__map-embed iframe-wrapper-16x9">
										<iframe data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3052.3530689384297!2d-83.13033858385934!3d40.08984038337776!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88389323f6b0c569%3A0xd62231ee529bd85a!2s5186+Blazer+Pkwy%2C+Dublin%2C+OH+43017!5e0!3m2!1sen!2sus!4v1508171676685" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="one-half">

							<div class="row">
								<div class="one-half">
									<div class="page-footer__address-box">
										<span class="page-footer__address-name"><a href="https://www.bressmanlaw.com/cincinnati-injury/" title="Personal Injury Lawyers in Cincinnati" style="color:rgb(228, 106, 16);">Cincinnati Location</a></span>
										<address class="page-footer_address-location">
											9435 Waterstone Blvd #130<br>
											Cincinnati, OH 45249<br>
											Phone: <a href="tel:+1-513-434-1818">513-434-1818</a>
										</address>
										<div class="iframe-wrapper page-footer__map-embed iframe-wrapper-16x9">
											<iframe data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3087.492945822731!2d-84.30469358387072!3d39.29974833073046!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88405651eeb35c8f%3A0x5acf0a092a7ceb54!2s9435+Waterstone+Blvd+%23130%2C+Cincinnati%2C+OH+45249!5e0!3m2!1sen!2sus!4v1508171713981" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
										</div>
								</div>
							</div>
								<div class="one-half">
									<div class="page-footer__address-box">
										<span class="page-footer__address-name"><a href="https://www.bressmanlaw.com/columbus-injury/" title="Personal Injury Lawyers in Columbus" style="color:rgb(228, 106, 16);">Columbus Location</a></span>
										<address class="page-footer_address-location">
											64 Parsons Avenue<br>
											Columbus, Ohio 43215<br>
											Phone: <a href="tel:+1-513-434-1818">513-434-1818</a>
										</address>
										<div class="iframe-wrapper page-footer__map-embed iframe-wrapper-16x9">
											<iframe data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3058.035563419153!2d-82.98356188461781!3d39.96295757942056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x883888c545a0b76d%3A0xe0e64ce700ddf705!2s64+Parsons+Ave%2C+Columbus%2C+OH+43215!5e0!3m2!1sen!2sus!4v1559852788417!5m2!1sen!2sus" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
										</div>
								</div>
							</div>
						</div>

					</div>


				</div>

			</div>

			<div class="page-footer__disclaimer-section">
				<div class="wrapper">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/bressman-law-logo.png" alt="David Bressman Law" class="page-footer__footer-logo">

					<?php //Social Icons ?>
					<div class="page-footer__social-icons">
						<span class="page-footer__social-intro-text">Follow Us On:</span>
							<a  target="_blank" href="https://www.facebook.com/bressmanlawfirm/" class="page-footer__social-icon"><i class="fa fa-facebook"></i></a>
							<a  target="_blank" href="https://twitter.com/dabressman" class="page-footer__social-icon"><i class="fa fa-twitter"></i></a>
							<a  target="_blank" href="https://www.linkedin.com/pub/david-bressman/8/725/2a7" class="page-footer__social-icon"><i class="fa fa-linkedin"></i></a>
							<a  target="_blank" href="https://www.youtube.com/user/DavidBressman/videos" class="mobile-header__social-icon"><i class="fa fa-play"></i></a>
					</div>

					<p class="page-footer__disclaimer-text">
						<a rel="nofollow" href="https://m.me/143111162614/" target="_blank">Message Us on Facebook</a> | <a rel="nofollow" href="https://plus.google.com/+Bressmanlaw" target="_blank" rel="nofollow">Visit Us on Google Plus</a> |  <a href="<?php echo site_url(); ?>/disclaimer/">Disclaimer</a> | <a href="<?php echo site_url(); ?>/site-map/">Site Map</a>
					</p>
				</div>
			</div>

		</footer>


	</div><?php //  END .sticky-footer-container ?>


	<?php //BEGIN Mobile Phone Popup ?>

	<a onclick="ga('send', 'event', 'Mobile Phone Link', 'Mobile Phone Click', 'Mobile Phone Click');" href="tel:+1-877-538-1116" class="footer-phone-circle mobile-phone-analytics-track"><i class="fa fa-phone"></i></a>

	<?php //END Mobile Phone Popup ?>


 	<div class="sb-slidebar sb-right">
		<span class="slidebar-header h2">
		Menu
			<span class="mobile-nav-box-close sb-close">
			</span>
		</span>

		<nav role="navigation">
			<?php wp_nav_menu(array(
			'container' => false,                           // remove nav container
			'container_class' => 'menu cf',                 // class of container (should you choose to use it)
			'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
			'menu_class' => 'nav slideout-nav cf',               // adding custom nav class
			'theme_location' => 'main-nav',                 // where it's located in the theme
			'before' => '',                                 // before the menu
			'after' => '',                                  // after the menu
			'link_before' => '',                            // before each link
			'link_after' => '',                             // after each link
			'depth' => 0,                                   // limit the depth of the nav
			'fallback_cb' => ''                             // fallback function (if there is one)
			)); ?>
		</nav>
	</div>

	<span id="return-to-top"><i class="fa fa-angle-up"></i></span>

	<?php // all js scripts are loaded in library/bones.php ?>
	<?php wp_footer(); ?>
	<?php //Tower Git Test ?>

	<?php //Add Apex Chat ?>
	<script src="//www.apex.live/scripts/invitation.ashx?company=bressmanlaw" async defer></script>
	</body>

</html>
