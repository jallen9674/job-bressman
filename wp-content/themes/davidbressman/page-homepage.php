<?php
/*
 Template Name: Homepage Template

Example Button

<a href="#" class="button-orange button">
		<div class="button-orange__gradient-helper">
		</div>
		<span class="text-outer">
			<span class="text-inner">View More Testimonials &raquo;</span>
		</span>
	</a>

*/
?>

<?php get_header(); ?>


		<?php
		/*-----------------------------------------
		Homepage Infro Section (Section 1)
		------------------------------------------*/
		?>

		<div class="homepage-section-1 homepage-section">
			<div class="homepage-section-1__inner wrapper">

				<div class="homepage-section-1__tablet-quote-wrapper">

					<div class="homepage-testimonial-slider flexslider">
						<ul class="slides">
							<li>
								<span class="homepage-section-1__quote-tablet homepage-section-1__quote">
									"We were put in a terrible situation due to the incompetence of another attorney. David was the only one who would help us. He fixed the problems, turned things around and got us a settlement better than expected."
								</span>
								<span class="homepage-section-1__quote-author-tablet homepage-section-1__quote-author">
									Michael P.
								</span>
							</li>
							<li>
								<span class="homepage-section-1__quote-tablet homepage-section-1__quote">
									David and his staff at Bressman Law are absolutely the best ! David was upfront and honest with us from the very beginning. We followed his advice and received a very nice settlement that we were satisfied with. We would recommend Bressman Law to anyone that needs a personal injury lawyer.
								</span>
								<span class="homepage-section-1__quote-author-tablet homepage-section-1__quote-author">
									Steve H.
								</span>
							</li>
							<li>
								<span class="homepage-section-1__quote-tablet homepage-section-1__quote">
									Bressman Law is the best! One of the things I love about their office was that there was always a kind voice on the other end of the telephone. They also returned with an answer to every question and concern, professionally and promptly
								</span>
								<span class="homepage-section-1__quote-author-tablet homepage-section-1__quote-author">
									Dave C.
								</span>
							</li>
						</ul>
					</div>

				</div>

				<div class="row">

				<div class="homepage-section-1__far-left">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/tom-martello-portrait.png" alt="David Bressman" class="homepage-section-1__tom-portrait">
					<span class="homepage-section-1__tm-signature">Tom Martello, Esq.</span>
				</div>

					<div class="homepage-section-1__left">

						<div class="homepage-testimonial-slider flexslider">
							<ul class="slides">
								<li>
									<span class="homepage-section-1__quote">
										"We were put in a terrible situation due to the incompetence of another attorney. David was the only one who would help us. He fixed the problems, turned things around and got us a settlement better than expected."
									</span>
									<span class="homepage-section-1__quote-author">
										Jerry F. - 2017
									</span>
								</li>
								<li>
									<span class="homepage-section-1__quote">
										“Fortunately a friend recommended David and I couldn't have been happier. He took care of everything and explained the process in an easy to understand way. I'm happy to have this case closed and finally behind me.”
									</span>
									<span class="homepage-section-1__quote-author">
										Aaron A. – 2017
									</span>
								</li>
								<li>
									<span class="homepage-section-1__quote">
										“The staff at Bressman Law was wonderful about keeping me updated on my case and communicating with me every step of the way... Because of Bressman Law my whole financial situation changed for the better – thank you and God bless!”
									</span>
									<span class="homepage-section-1__quote-author">
										Brenda S. – 2016
									</span>
								</li>
								<li>
									<span class="homepage-section-1__quote">
										“They took my case at the last minute and I mean last minute when no one else would or could. They got me a nice settlement when I was told by another firm it wasn’t worth it.”
									</span>
									<span class="homepage-section-1__quote-author">
										Tashiea C. – 2015
									</span>
								</li>
							</ul>
						</div>

						<div class="homepage-section-1__button-wrapper">

							<a href="<?php echo site_url(); ?>/testimonials/" class="button-orange button homepage-section-1__testimonials-button">
								<div class="button-orange__gradient-helper">
								</div>
								<span class="text-outer">
									<span class="text-inner">View More Testimonials &raquo;</span>
								</span>
							</a>
						</div>

						<div class="homepage-section-1__logos">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/logo-top100.png" alt="Top 100 Logo" class="homepage-section-1__logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/logo-aaj.png" alt="AAJ Logo" class="homepage-section-1__logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/logo-bbb.png" alt="BBB Logo" class="homepage-section-1__logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/logo-clf-seal.png" alt="CLF Seal" class="homepage-section-1__logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/logo-aplita-america.png" alt="APLITAmerica Logo" class="homepage-section-1__logo">
						</div>

					</div>

					<div class="homepage-section-1__right">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/david-bressman-portrait.png" alt="David Bressman" class="homepage-section-1__db-portrait">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/david-and-tom.png" alt="Tom and David" class="homepage-section-1__db-portrait combined">
						<?php /*<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/david-bressman-signature.png" alt="David Bressman Signature"  class="homepage-section-1__db-signature"> */ ?>

						<span class="homepage-section-1__db-signature">David Bressman, Esq.</span>
					</div>

				</div>

			</div>
		</div>

		<?php
		/*-----------------------------------------
		Homepage Section 2
		------------------------------------------*/
		?>

		<div class="homepage-section-2 homepage-section">

			<div class="homepage-section-2__prev-arrow-outer">
				<div class="homepage-section-2__prev-arrow-inner"></div>
				<div class="homepage-section-2__prev-arrow-center">
					<span class="homepage-section-2__prev-arrow-left-triangle"></span>
					<span class="homepage-section-2__prev-arrow-right-triangle"></span>
					<span class="homepage-section-2__prev-arrow-icon"><i class="fa fa-angle-down"></i></span>
				</div>
			</div>


			<div class="hommepage-section-2__inner wrapper">

				<h2 class="homepage-section-title">
					<span class="homepage-section-title__inner">
						Serving Ohio for Over 25 Years
					</span>
				</h2>
				<?php //Intro Text ?>
				<div class="row homepage-section-2__intro">
					<span class="homepage-section-2__subtitle" style="text-align: center;">
						A Columbus Personal Injury Firm Built on 3 Things
						</span>
						<div class="homepage-section-2__content">
							<p style="text-align: center;">Experience. Communication. Transparency.</p>
							<p>We work with the insurance company to settle your case, but if we can’t reach a fair settlement, we use our 25 years of courtroom experience to <strong>get you the money you need</strong>. Our firm’s unique communication system guarantees <strong>you won’t play phone tag when you call us</strong>. And our policy of transparency means we share all documents and keep you updated on your case’s status, so <strong>you aren’t left in the dark</strong>.</p>
							<p><strong>Watch the videos below to learn more about what makes a good personal injury attorney.</strong></p>

						</div>
				</div>
				<?php //Row 1 ?>
				<div class="row homepage-section-2__row-1">
					<div class="one-half">
						<?php //Note this video is uploaded to the WPAdmin ?>
						<?php echo do_shortcode('[arve mp4="https://www.bressmanlaw.com/wp-content/uploads/2018/12/BressmanLaw_Intro-web.mp4" thumbnail="2725"]');?>
					</div>
					<div class="one-half">
						<div class="homepage-section-2__video">
							<div class="iframe-wrapper iframe-wrapper-16x9">
								<iframe width="500" height="350" frameborder="0" data-src="https://www.youtube.com/embed/Q1jGChnLQ68?rel=0"></iframe>
							</div>
						</div>
					</div>
				</div>

				<?php //Row 2 ?>
				<div class="row homepage-section-2__row-2">
					<div class="one-half">
						<img class="homepage-section-2__guarantee-image" src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/no-fee-guarantee.png" alt="No Fee Guarantee">
					</div>
					<div class="one-half">
						<div class="homepage-section-2__contact-form">
							<span class="homepage-section-2__contact-form-evaluation-text">
								FREE CASE EVALUATION
							</span>
							<span class="homepage-section-2__contact-form-title">
								Call <a href="tel:+1-877-538-1116">1-877-538-1116</a>
							</span>
							<span class="homepage-section-2__contact-form-subtitle">
								- or fill out the form below -
							</span>

							<?php echo do_shortcode('[contact-form-7 id="2412" title="Homepage Contact Form" html_class="homepage-contact-form standard-form"]'); ?>
						</div>
					</div>
				</div>

				<?php //Row 3 (Scholarship Information - Temporary) ?>
				<div class="row homepage-section-2__row-3">

					<h2 class="homepage-section-2__scholarship-title">
						Announcing the Bressman Law Traumatic Brain Injury Scholarship
					</h2>

					<h3 class="homepage-section-2__scholarship-subtitle">
						Applications Can Be Submitted from December 1st, 2017 through January 31st, 2018.
					</h3>

					<div class="homepage-section-2__scholarship-button-wrapper">
						<a href="<?php echo site_url(); ?>/scholarship/" class="button-orange button homepage-section-2__scholarship-button">
							<div class="button-orange__gradient-helper">
							</div>
							<span class="text-outer">
								<span class="text-inner">View Scholarship Details &raquo;</span>
							</span>
						</a>
					</div>

				</div>

			</div> <?php // End inner ?>

			<div class="homepage-section-2__next-arrow-outer">
				<div class="homepage-section-2__next-arrow-inner"></div>
				<div class="homepage-section-2__next-arrow-center">
					<span class="homepage-section-2__next-arrow-left-triangle"></span>
					<span class="homepage-section-2__next-arrow-right-triangle"></span>
					<span class="homepage-section-2__next-arrow-icon"><i class="fa fa-angle-down"></i></span>
				</div>
			</div>

		</div> <?php //End Homepage Section 2 ?>


		<?php
		/*-----------------------------------------
		Homepage Section 3
		------------------------------------------*/
		?>

		<div class="homepage-section-3 homepage-section parallax-window" data-parallax="scroll" data-z-index="1" data-image-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/practice-areas-bg.jpg" id="homepage-section-3">
			<div class="homepage-section-3__inner wrapper">

				<h2 class="homepage-section-title">
					<span class="homepage-section-title__inner">
						Have Legal Questions? We Have Answers
					</span>
				</h2>

				<div class="homepage-section-3__callout">
					If you or someone you love has sustained an injury in any of these types of accidents caused by negligence, contact the personal injury team at Bressman Law to find out how we can help you get compensation.
				</div>

				<div class="homepage-section-3__practice-area-grid">

					<?php //Car Accidents ?>
					<a href="<?php echo site_url(); ?>/columbus-injury/car-accident-lawyer/" class="homepage-practice-area">
						<span class="homepage-practice-area__title">Car Accidents</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/car.png" alt="Car Accidents" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							Your car accident case has to prove fault to hold the negligent driver liable. We can help you get the money you deserve.
						</p>
						<?php /*<span class="button-orange button">
							<div class="button-orange__gradient-helper">
							</div>
							<span class="text-outer">
								<span class="text-inner">Learn More &raquo;</span>
							</span>
						</span> */ ?>
					</a> <?php //End Car Accidents ?>

					<?php //Truck Accidents ?>
					<a href="<?php echo site_url(); ?>/columbus-injury/truck-accident-lawyer/" class="homepage-practice-area">
						<span class="homepage-practice-area__title">Truck Accidents</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/truck.png" alt="Truck Accidents" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							The truck carrier is liable if a trucker caused your accident. We help you navigate complicated truck accident cases.
						</p>
						<?php /*<span class="button-orange button">
							<div class="button-orange__gradient-helper">
							</div>
							<span class="text-outer">
								<span class="text-inner">Learn More &raquo;</span>
							</span>
						</span> */ ?>
					</a> <?php //End Truck Accidents ?>

					<?php //Bus Accidents ?>
					<a href="<?php echo site_url(); ?>/columbus-injury/bus-accident-lawyer/" class="homepage-practice-area">
						<span class="homepage-practice-area__title">Bus Accidents</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/bus.png" alt="Bus Accidents" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							Passengers, pedestrians, and other drivers can hold bus companies liable for accidents. We make sure you get what you deserve.
						</p>
						<?php /*<span class="button-orange button">
							<div class="button-orange__gradient-helper">
							</div>
							<span class="text-outer">
								<span class="text-inner">Learn More &raquo;</span>
							</span>
						</span> */ ?>
					</a> <?php //End Bus Accidents ?>

					<?php //Motorcycle Accidents ?>
					<a href="<?php echo site_url(); ?>/columbus-injury/motorcycle-accident-lawyer/" class="homepage-practice-area">
						<span class="homepage-practice-area__title">Motorcycle Accidents</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/motorbike.png" alt="Motorcycle Accidents" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							Motorcycle cases come with complexities and biases that other accidents do not have. We help you get the money you need.
						</p>
						<?php /*<span class="button-orange button">
							<div class="button-orange__gradient-helper">
							</div>
							<span class="text-outer">
								<span class="text-inner">Learn More &raquo;</span>
							</span>
						</span> */ ?>
					</a> <?php //End Motorcycle Accidents ?>

					<?php //Personal Injury ?>
					<a href="<?php echo site_url(); ?>/" class="homepage-practice-area">
						<span class="homepage-practice-area__title">Personal Injury</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/hospital.png" alt="Personal Injury" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							If negligence caused your accident, you can hold the at-fault party liable for bills, losses, and other damages. We can help.
						</p>
						<?php /*<span class="button-orange button">
							<div class="button-orange__gradient-helper">
							</div>
							<span class="text-outer">
								<span class="text-inner">Learn More &raquo;</span>
							</span>
						</span> */ ?>
					</a> <?php //End Personal Injury ?>

					<?php //Product Liability Lawyer ?>
					<a href="<?php echo site_url(); ?>/columbus-injury/product-liability-lawyer/" class="homepage-practice-area">
						<span class="homepage-practice-area__title">Product Liability</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/airbag.png" alt="Product Liability Lawyer" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							If a defective product injured you, the manufacturer could be liable. We help you get money for your losses.
						</p>
						<?php /*<span class="button-orange button">
							<div class="button-orange__gradient-helper">
							</div>
							<span class="text-outer">
								<span class="text-inner">Learn More &raquo;</span>
							</span>
						</span> */ ?>
					</a> <?php //End Product Liability Lawyer ?>

					<?php //Traumatic Brain Injury ?>
					<a href="<?php echo site_url(); ?>/columbus-injury/traumatic-brain-injury-tbi-lawyer/" class="homepage-practice-area">
						<span class="homepage-practice-area__title">Traumatic Brain Injury</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/brain-mri.png" alt="Traumatic Brain Injury" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							A TBI has long-lasting effects. We help you get money to cover your current and future bills, losses, and needs.
						</p>
						<?php /*<span class="button-orange button">
							<div class="button-orange__gradient-helper">
							</div>
							<span class="text-outer">
								<span class="text-inner">Learn More &raquo;</span>
							</span>
						</span> */ ?>
					</a> <?php //End Traumatic Brain Injury ?>

					<?php //Wrongful Death ?>
					<a href="<?php echo site_url(); ?>/columbus-injury/wrongful-death-lawyer/" class="homepage-practice-area">
						<span class="homepage-practice-area__title">Wrongful Death</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/practice-areas/death.png" alt="Wrongful Death" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							If negligence caused your loved one’s death, we hold the liable party responsible for your family’s losses.
						</p>
						<?php /*<span class="button-orange button">
							<div class="button-orange__gradient-helper">
							</div>
							<span class="text-outer">
								<span class="text-inner">Learn More &raquo;</span>
							</span>
						</span> */ ?>
					</a> <?php //End Wrongful Death ?>

				</div>

			</div>
		</div> <?php //End Homepage Section 3 ?>


		<?php
		/*-----------------------------------------
		Homepage Section 4
		------------------------------------------*/
		?>

		<div class="homepage-section-4 homepage-section">

			<div class="homepage-section-4__prev-arrow-outer">
				<div class="homepage-section-4__prev-arrow-inner"></div>
				<div class="homepage-section-4__prev-arrow-center">
					<span class="homepage-section-4__prev-arrow-left-triangle"></span>
					<span class="homepage-section-4__prev-arrow-right-triangle"></span>
					<span class="homepage-section-4__prev-arrow-icon"><i class="fa fa-angle-down"></i></span>
				</div>
			</div>

			<div class="homepage-section-4__inner wrapper">

				<h2 class="homepage-section-title">
					<span class="homepage-section-title__inner">
						Available 24/7 - Will Travel To You
					</span>
				</h2>

				<span class="homepage-section-4__callout">
					Proudly Serving Ohio For Over 25 Years
				</span>

				<div class="homepage-city-list">

					<?php //Columbus Location ?>
					<a href="<?php echo site_url(); ?>/" class="homepage-city">
						<span class="homepage-city__title">Columbus</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/locations/columbus.png" alt="Columbus Location" class="homepage-city__image">
						<p class="homepage-city__description">
							We serve the people of Columbus and the surrounding metro area within and outside the I-270 Outerbelt.
						</p>
					</a>

					<?php //Dublin Location ?>
					<a href="<?php echo site_url(); ?>/dublin-injury/" class="homepage-city">
						<span class="homepage-city__title">Dublin</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/locations/dublin.png" alt="Dublin Location" class="homepage-city__image">
						<p class="homepage-city__description">
							Our main office is in Dublin west of the Scioto River and easily accessible from I-270 and SR-33.
						</p>
					</a>

					<?php //Hilliard Location ?>
					<a href="<?php echo site_url(); ?>/hilliard-injury/" class="homepage-city">
						<span class="homepage-city__title">Hilliard</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/locations/hilliard.png" alt="Hilliard Location" class="homepage-city__image">
						<p class="homepage-city__description">
							Injured in Hilliard? Come to our main office location, accessible via the I-270 Outerbelt or we can come to you.
						</p>
					</a>

					<?php //Upper Arlington Location ?>
					<a href="<?php echo site_url(); ?>/upper-arlington-injury/car-accident-lawyer/" class="homepage-city">
						<span class="homepage-city__title">Upper Arlington</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/locations/upper-arlington.png" alt="Upper Arlington Location" class="homepage-city__image">
						<p class="homepage-city__description">
							We invite those in Upper Arlington to visit our main office, accessible via SR-33 or the I-270 Outerbelt, or we can come to you.
						</p>
					</a>

					<?php //Gahanna Location ?>
					<a href="<?php echo site_url(); ?>/gahanna-injury/" class="homepage-city">
						<span class="homepage-city__title">Gahanna</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/locations/gahanna.png" alt="Gahanna Location" class="homepage-city__image">
						<p class="homepage-city__description">
							Residents of Gahanna can access our main office via the I-270 Outerbelt, or we can come to you.
						</p>
					</a>

				</div> <?php //End Homepage City List ?>

				<p class="homepage-section-4__city-service-link-wrapper">
					<a href="<?php echo site_url(); ?>/areas-we-serve/" class="homepage-section-4__city-service-link">See Full List of Cities We Service &raquo;</a>
				</p>

			</div> <?php //End .inner ?>

			<div class="homepage-section-4__next-arrow-outer">
				<div class="homepage-section-4__next-arrow-inner"></div>
				<div class="homepage-section-4__next-arrow-center">
					<span class="homepage-section-4__next-arrow-left-triangle"></span>
					<span class="homepage-section-4__next-arrow-right-triangle"></span>
					<span class="homepage-section-4__next-arrow-icon"><i class="fa fa-angle-down"></i></span>
				</div>
			</div>

		</div> <?php //End Homepage Section 4 ?>


		<?php
		/*-----------------------------------------
		Homepage Section 5
		------------------------------------------*/
		?>

		<div class="homepage-section-5 homepage-section" data-parallax="scroll" data-z-index="1" data-bleed="60" data-image-src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage/homepage-verdict-bg.jpg" id="homepage-section-5">
			<div class="homepage-section-5__inner wrapper">

				<h2 class="homepage-section-title">
					<span class="homepage-section-title__inner">
						We've Helped Thousands of People Just Like You
					</span>
				</h2>

				<span class="homepage-section-5__callout">
					Case Results
				</span>

				<div class="homepage-case-results-list">

					<div class="homepage-case-result">
						<span class="homepage-case-result__verdict">
							Verdict of $1,450,000
						</span>
						<span class="homepage-case-result__description">
							Insurance company offered nothing. We won a $1.45 million verdict.
						</span>
					</div>

					<div class="homepage-case-result">
						<span class="homepage-case-result__verdict">
							Verdict of $415,576
						</span>
						<span class="homepage-case-result__description">
							Insurance company offered $0. We won $415,576 in court.
						</span>
					</div>

					<div class="homepage-case-result">
						<span class="homepage-case-result__verdict">
							Verdict of $180,000
						</span>
						<span class="homepage-case-result__description">
							Insurance company offered $0. We won $180,000 at trial.
						</span>
					</div>

					<div class="homepage-case-result">
						<span class="homepage-case-result__verdict">
							Verdict of $100,000
						</span>
						<span class="homepage-case-result__description">
							Insurance company offered $5,000. We won $100,000 in court.
						</span>
					</div>

				</div> <?php //End Case Results List ?>

 				<div class="homepage-section-5__button-wrapper">
					<a href="<?php echo site_url(); ?>/verdicts/" class="button-orange button">
						<div class="button-orange__gradient-helper">
						</div>
						<span class="text-outer">
							<span class="text-inner">See More Case Results &raquo;</span>
						</span>
					</a>
				</div>

			</div>
		</div> <?php //End Homepage Section 5 ?>


		<?php
		/*-----------------------------------------
		Homepage Section 6
		------------------------------------------*/
		?>

		<div class="homepage-section-6 homepage-section">

			<div class="homepage-section-6__prev-arrow-outer">
				<div class="homepage-section-6__prev-arrow-inner"></div>
				<div class="homepage-section-6__prev-arrow-center">
					<span class="homepage-section-6__prev-arrow-left-triangle"></span>
					<span class="homepage-section-6__prev-arrow-right-triangle"></span>
					<span class="homepage-section-6__prev-arrow-icon"><i class="fa fa-angle-down"></i></span>
				</div>
			</div>

			<div class="homepage-section-6__inner wrapper">

				<h2 class="homepage-section-title">
					<span class="homepage-section-title__inner">
						Testimonials
					</span>
				</h2>

				<?php //Display Testimonials ?>
				<div class="homepage-section-6__review-wrapper">
					<?php echo do_shortcode('[bressman-homepage-reviews count="4"]'); ?>
				</div>


 				<div class="homepage-section-6__button-wrapper">
					<a href="<?php echo site_url(); ?>/testimonials/" class="button-orange button">
						<div class="button-orange__gradient-helper">
						</div>
						<span class="text-outer">
							<span class="text-inner">See More Testimonials &raquo;</span>
						</span>
					</a>
				</div>

			</div> <?php //End __inner ?>

			<div class="homepage-section-6__next-arrow-outer">
				<div class="homepage-section-6__next-arrow-inner"></div>
				<div class="homepage-section-6__next-arrow-center">
					<span class="homepage-section-6__next-arrow-left-triangle"></span>
					<span class="homepage-section-6__next-arrow-right-triangle"></span>
					<span class="homepage-section-6__next-arrow-icon"><i class="fa fa-angle-down"></i></span>
				</div>
			</div>

		</div> <?php //End Homepage Section 6 ?>







<?php get_footer(); ?>
