<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">

	<?php // Google Chrome Frame for IE ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php wp_title(''); ?></title>

	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>


	<?php // wordpress head functions ?>
	<?php wp_head(); ?>
	<?php // end of wordpress head ?>

	<?php // drop Google Analytics Here ?>
	<?php // end analytics ?>

</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WGN8DC9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="header sb-slide mobile-header">
	<div id="inner-header-mobile" class="wrap cf">

		<div class="mobile-header__logo mobile-header__left">
			<a href="<?php echo site_url(); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/bressman-law-logo.png" alt="Bressman Law Logo">
			</a>
		</div>

		<?php //Contact Text ?>

		<div class="mobile-header__contact-text">
			<span class="mobile-header__contact-intro-text">
				Call Us Today:
			</span>
			<a href="#" class="mobile-header__contact-phone-number">
				877-538-1116
			</a>
		</div>
		<?php
		    if (function_exists('zeno_font_resizer_place')) {
		        $font_resizer = zeno_font_resizer_place( false  );
		        echo '<div class="font-resizer">'     ;
		        echo '<span class="font-resizer__heading">Resize Font</span>';
		        $font_resizer = str_replace('Increase font size.', 'Increase', $font_resizer);
		        $font_resizer = str_replace('Decrease font size.', 'Decrease', $font_resizer);
		        $font_resizer = str_replace('Reset font size.', 'Reset', $font_resizer);
		        echo $font_resizer;
		        echo '</div>';
		    }
		?>

		<div class="mobile-nav-menu-box">
			MENU	<span class="sb-toggle-right mobile-nav-arrow mobile-nav-box-open"></span>
		</div>
	</div>
</div>


<?php /* Disabled - Regular Header is Fixed ?>
<div class="desktop-scrolled-header">
	<div class="top-nav-wrapper">
		<nav role="navigation" class="wrapper">
			<?php wp_nav_menu(array(
			'container' => false,                           // remove nav container
			'container_class' => 'menu cf',                 // class of container (should you choose to use it)
			'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
			'menu_class' => 'nav top-nav cf',               // adding custom nav class
			'theme_location' => 'main-nav',                 // where it's located in the theme
			'before' => '',                                 // before the menu
			'after' => '',                                  // after the menu
			'link_before' => '',                            // before each link
			'link_after' => '',                             // after each link
			'depth' => 0,                                   // limit the depth of the nav
			'fallback_cb' => ''                             // fallback function (if there is one)
			)); ?>

			<div class="phone-number">
				<a href="tel:+1-555-555-5555">555-555-5555</a>
			</div>
		</nav>
	</div>
</div> */ ?>


	<header class="header desktop-header">
		<div id="inner-header" class="wrapper">

		<div class="row">
			<div class="desktop-header__logo desktop-header__left">
				<a href="<?php echo site_url(); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/bressman-law-logo.png" alt="Bressman Law Logo">
				</a>
			</div>


			<div class="desktop-header__right">

				<?php //Contact Text ?>

				<div class="desktop-header__contact-text">
					<span class="desktop-header__contact-intro-text">
						Call Us Today:
					</span>
					<a href="tel:+1-877-538-1116" class="desktop-header__contact-phone-number">
						877-538-1116
					</a>
				</div>

			</div>
		</div>	<?php //End .row ?>

		<?php //Main Navigation ?>
		<div class="top-nav-wrapper">
			<nav role="navigation" class="wrapper">
				<?php wp_nav_menu(array(
				'container' => false,                           // remove nav container
				'container_class' => 'menu cf',                 // class of container (should you choose to use it)
				'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
				'menu_class' => 'nav top-nav cf',               // adding custom nav class
				'theme_location' => 'main-nav',                 // where it's located in the theme
				'before' => '',                                 // before the menu
    			'after' => '',                                  // after the menu
    			'link_before' => '',                            // before each link
    			'link_after' => '',                             // after each link
    			'depth' => 0,                                   // limit the depth of the nav
				'fallback_cb' => ''                             // fallback function (if there is one)
				)); ?>
			</nav>
		</div> <?php //End Main Nav ?>

		</div>
	</header>


<div class="sticky-footer-container" id="sb-site">


	<div id="container">
