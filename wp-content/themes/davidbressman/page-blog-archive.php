<?php
//Template Name: Blog Archive

$args = array(
  'post_type'              => 'post',
  'posts_per_page'         => -1,
  'order'                  => 'ASC',
  'orderby'                => 'date'
);

$query = new WP_Query( $args );
?>
<?php get_header(); ?>
	<div id="inner-content" class="wrapper">
			<div id="main" class="content-container">
				<div class="breadcrumbs-wrapper">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>
					<h1 class="archive-title">
						<?php the_title(); ?>
					</h1>
          <ul>
				<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
          <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
				<?php endwhile; ?>
      </ul>
				<?php else : ?>
						<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<p><?php _e( 'This is the error message in the archive.php template.', 'bonestheme' ); ?></p>
							</footer>
						</article>
				<?php endif; ?>
			</div>
		<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>
