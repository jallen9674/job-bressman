<?php //Template Name: Page w/ Centered Title ?>
<?php get_header(); ?>

	<div id="inner-content" class="wrapper">

			<div id="main" class="content-container">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?>>

					<header class="article-header">
						<h1 class="page-title" style="text-align: center" itemprop="headline"><?php the_title(); ?></h1>
					</header>

					<section class="entry-content cf" itemprop="articleBody">

						<?php
							if ( has_post_thumbnail() ) {
								echo '<div class="page-featured-image">';
								the_post_thumbnail('bones-thumb-page');
								echo '</div>';
							}

							the_content();
						?>
					</section>

					<?php comments_template(); ?>

				</article>

				<?php endwhile; else : ?>

						<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
							</footer>
						</article>

				<?php endif; ?>

			</div>

			<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>
