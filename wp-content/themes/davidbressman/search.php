<?php get_header(); ?>

	<div id="inner-content" class="wrapper">

		<div id="main" class="content-container">

			<div class="breadcrumbs-wrapper">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>

			<h1 class="page-title h2"><span><?php _e( 'Search Results for:', 'bonestheme' ); ?></span> "<?php echo esc_attr(get_search_query()); ?>"</h1>

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" class="cf">

					<header class="article-header">

						<h3 class="search-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

		        		<a href="<?php echo the_permalink(); ?>" class="search-link linebreak"><?php echo the_permalink(); ?></a>
					</header>

					<section class="entry-content">
							<?php //The length of this is modified by a filter in functions.php ?>
							<?php the_excerpt( '<span class="read-more">' . __( 'Read more &raquo;', 'bonestheme' ) . '</span>' ); ?>
					</section>


				</article>

			<?php endwhile; ?>

					<?php numeric_posts_nav(); ?>



						<article id="post-not-found" class="cf">
							<header class="article-header">
								<p class="h3"><?php _e( 'Sorry, no results matched your search criteria.', 'bonestheme' ); ?></p>
							</header>
							<section class="entry-content">
								<p>Please try your search again.</p>
							</section>
						</article>

				<?php endif; ?>


			</div> <?php //end .main ?>

			<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>
