<?php //Template Name: Testimonials Page ?>
<?php get_header(); ?>

	<div id="inner-content" class="wrapper">

			<div id="main" class="content-container">

				<div class="breadcrumbs-wrapper">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?>>

					<header class="article-header">
						<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
					</header>

					<section class="entry-content cf" itemprop="articleBody">

						<?php
							the_content();
						?>

						<h2>Here is what clients have to say about the Law Office of David A. Bressman:</h2>

						<div class="iframe-wrapper iframe-wrapper-16x9" style="margin-bottom: 30px;">
							<iframe src="https://www.youtube.com/embed/CCfKOa8MxvQ" frameborder="0" allowfullscreen></iframe>
						</div>

						<?php echo do_shortcode('[bressman-reviews count="-1"]'); ?>

					</section>

				</article>

				<?php endwhile; else : ?>
					<p>Something went wrong. Please try again later.</p>
				<?php endif; ?>

			</div>

			<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>
