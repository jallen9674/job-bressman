<?php //Template Name: Reviews Page ?>
<?php get_header(); ?>

<div class="main has-sidebar has-sidebar-right">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php //Subheader ?>
	<div class="subheader-wrapper">

		<div class="container_12">
			<div class="grid_12">
				<div id="subheader">
					<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
		<div class="clear"></div>

	</div>
	<?php //Subheader End ?>


	<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?>>

		<section itemprop="articleBody">
			<?php
			the_content();
			?>
		</section>

		<div class="content-wrapper">
			<div class="clearfix page-container row-fluid">
				<section class="entry-content review-wrapper span8">
					<div class="prime-page">
						<div class="review-video-tagLine">
							<h4>Here are what my clients have to say about the Law Office of David A. Bressman:</h4>
							<div class="review-video-wrapper">
								<iframe src="https://www.youtube.com/embed/CCfKOa8MxvQ" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>

						<?php echo do_shortcode('[bressman-reviews count="-1"]'); ?>
				</section>
			</div> <?php //end .page-container ?>
		</div> <?php //end content-wrapper?>

		<?php //comments_template(); ?>

	</article>

	<?php endwhile; else : ?>

		<article id="post-not-found" class="hentry cf">
			<header class="article-header">
				<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
			</header>
			<section class="entry-content">
				<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
			</section>
			<footer class="article-footer">
				<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
			</footer>
		</article>

	<?php endif; ?>

	<div class="prime-page">
		<div class="review-footerStatement">
			<em>All testimonials are on file and used with written permission.<br />
			Many More Available For Your Review, As Well Examples Of Cases Settled And Cases Tried To Judges/Juries.</em>
		</div>
	</div>

	<?php roots_sidebar_before(); ?>
	<div class="span4 sidebar-wrapper review-sidebar">
		<div id="sidebar">
			<?php roots_sidebar_inside_before(); ?>
			<?php get_sidebar(); ?>
			<?php roots_sidebar_inside_after(); ?>
		</div>
		<?php roots_sidebar_after(); ?>
	</div>

	<?php get_footer(); ?>
</div>
<?php roots_main_after(); ?>
<?php roots_content_after(); ?>
