<?php
/*--------------------------------
Example Shortcode Wrapper
[personal-injury-grid]
---------------------------------*/

function personalinjurygrid($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT
?>

<?php //Output Goes Here ?>
<h4 style="text-align: center;">Causes of Personal Injury</h4>
<div class="personal-injury-grid">
  <a class="personal-injury-grid__single" href="https://www.bressmanlaw.com/columbus-injury/wrongful-death-lawyer/">
    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
    Wrongful Death
  </a>
  <a class="personal-injury-grid__single" href="https://www.bressmanlaw.com/columbus-injury/car-accident-lawyer/">
    <i class="fa fa-car" aria-hidden="true"></i>
    Car Accident
  </a>
  <a class="personal-injury-grid__single" href="https://www.bressmanlaw.com/columbus-injury/product-liability-lawyer/">
    <i class="fa fa-product-hunt" aria-hidden="true"></i>
    Product Liablity
  </a>
</div>
<div class="personal-injury-grid">
  <a class="personal-injury-grid__single" href="https://www.bressmanlaw.com/dublin-injury/construction-accident-lawyer/">
    <i class="fa fa-plus-square" aria-hidden="true"></i>
    Construction Injuries To Passersby
  </a>
  <a class="personal-injury-grid__single" href="https://www.bressmanlaw.com/dublin-injury/dog-bite-lawyer/">
    <i class="fa fa-paw" aria-hidden="true"></i>
    Animal & Dog Bites
  </a>
  <a class="personal-injury-grid__single" href="https://www.bressmanlaw.com/columbus-injury/traumatic-brain-injury-tbi-lawyer/">
    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
    Traumatic Brain Injury
  </a>
</div>

<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('personal-injury-grid', 'personalinjurygrid');


?>
