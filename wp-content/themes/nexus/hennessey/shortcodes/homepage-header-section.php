<?php
/*--------------------------------
Rebuilt Homepage Header Section w/o JS
[bressman-homepage-header]
---------------------------------*/

function bressmanHomepageHeader($atts = null) {
    global $post;
    ob_start();
    //BEGIN OUTPUT
?>

<div class="main homepage-header-wrapper"> <?php //Note: main is from main theme and used as wrapper ?>
    <div class="homepage-header-section">
        <div class="homepage-header-section__video">
          <div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
            <meta itemprop="name" content="Bressman Law What To Look For">
            <meta itemprop="thumbnailUrl" content="http://i3.ytimg.com/vi/Q1jGChnLQ68/hqdefault.jpg" />
            <meta itemprop="contentURL" content="https://www.youtube.com/embed/Q1jGChnLQ68" />
            <meta itemprop="uploadDate" content="2013-01-14T16:00:00-08:00" />
            <div class="iframe-wrapper iframe-wrapper-16x9"><iframe width="500" height="350" frameborder="0" src="https://www.youtube.com/embed/Q1jGChnLQ68?rel=0"></iframe></div>
                <br>
            <meta itemprop="description" content="Intro to the Bressman Law website">
          </div>
        </div>
        <div class="homepage-header-section__contact-form">
            <h3 class="homepage-header-section__form-heading">Have a legal question? Fill out the form below to get an answer!</h3>
            <?php echo do_shortcode('[contact-form-7 id="37" title="General Contact Form" html_class="wdwc-contact-form"]'); ?>
        </div>
    </div>
</div>

<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('bressman-homepage-header', 'bressmanHomepageHeader');


?>
