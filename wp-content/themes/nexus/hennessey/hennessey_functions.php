<?php
/************************************
**
** BEGIN Optimizations
** OF Hennessey Consulting
**
************************************/


/*---------------------------------
Dequeue Scripts and Styles
----------------------------------*/

function hennessey_enqueue_cleanup() {
    //wp_dequeue_style( 'ls-google-fonts' );
    //wp_deregister_style( 'ls-google-fonts' );
}
add_action( 'wp_enqueue_scripts', 'hennessey_enqueue_cleanup', 2000 );

function hennessey_scripts_enqueue_cleanup() {
    //wp_dequeue_script( 'cu3er' );
    //wp_deregister_script( 'cu3er' );
}
add_action( 'wp_print_scripts', 'hennessey_scripts_enqueue_cleanup', 2000 );


/*---------------------------------
Enqueue of Optimized Styles
----------------------------------*/
function hennessey_load_styles(){
    wp_enqueue_style( 'hennessey-css', get_stylesheet_directory_uri() .'/hennessey/css/styles.css' );
}
add_action( 'wp_enqueue_scripts', 'hennessey_load_styles', 10000);


/*---------------------------------
Enqueue of Optimized Scripts
/wp-includes/js/jquery/jquery.js
----------------------------------*/

function hennessey_load_scripts(){
  wp_enqueue_script('lightbox-js', get_stylesheet_directory_uri() . '/hennessey/js/vendor/lightbox.js', array('jquery'), '1.0', true);
  wp_enqueue_script('hennessey-js', get_stylesheet_directory_uri() . '/hennessey/js/scripts.js', array('jquery'), '1.0', true);
}
add_action( 'wp_enqueue_scripts', 'hennessey_load_scripts', 40);


/*---------------------------------
Remove Emoji
----------------------------------*/
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


/*----------------------------------------------
- TEMP: To find all script handles
- Should be disabled for production
-----------------------------------------------*/
function hennessey_inspect_scripts() {
    global $wp_scripts;
    foreach( $wp_scripts->queue as $handle ) :
        echo $handle,' ';
    endforeach;
}
//add_action( 'wp_print_scripts', 'hennessey_inspect_scripts' );


/*---------------------------------
Cleaning Up WP Head
----------------------------------*/
function removeHeadLinks() {
  remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
  remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
  remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
  remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
  remove_action( 'wp_head', 'index_rel_link' ); // index link
  remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
  remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
  remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
  remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
}
add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

/*---------------------------------
Adding Custom Metaboxes 2 (CB2) For Custom Post Types
----------------------------------*/
require_once dirname( __FILE__ ) . '/cmb2/init.php';

function update_cmb_meta_box_url( $url ) {
    $url = '/wp-content/themes/nexus/hennessey/cmb2/';
    return $url;
}
add_filter( 'cmb2_meta_box_url', 'update_cmb_meta_box_url' );

/*---------------------------------
Adding Post Types
----------------------------------*/

require_once('post-types/reviews.php');

/*---------------------------------
Adding Cropped Images
----------------------------------*/

add_image_size( 'review-image', 120, 120, true );

/*---------------------------------
Adding Icon For Admin
----------------------------------*/

function add_menu_icons_styles(){ ?>
  <style>
      #adminmenu .menu-icon-bressman_reviews div.wp-menu-image:before { content: "\f313"; }
  </style>
  <?php }
  add_action( 'admin_head', 'add_menu_icons_styles' );

/*---------------------------------
Adding Shortcodes
----------------------------------*/

//require_once('shortcodes/example-shortcode.php');
require_once('shortcodes/reviews.php');
require_once('shortcodes/homepage-header-section.php');
require_once('shortcodes/personal-injury-grid.php');


/*---------------------------------
Adding Header Section to Homepage
----------------------------------*/

function hennessey_add_new_header(){
  //Check if Homepage
  if ( is_page('2') ) {
    echo do_shortcode('[bressman-homepage-header]');
  }
}
add_action('roots_header_after', 'hennessey_add_new_header');


/*--------------------------------------------
Adding CSS/JS Admin Panel
---------------------------------------------*/

function load_custom_wp_admin_style() {
        wp_register_style( 'hennessey_wp_admin_css', get_template_directory_uri() . '/hennessey/css/admin.css', false, '1.0.0' );
        wp_enqueue_style( 'hennessey_wp_admin_css' );
        wp_enqueue_script( 'hennessey_wp_admin_js', get_template_directory_uri() . '/hennessey/js/admin.js', false, '1.0.0' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

/*--------------------------------------------
Adding Page Number to Description
---------------------------------------------*/

if ( ! function_exists( 't5_add_page_number' ) )
{
    function t5_add_page_number( $s )
    {
        global $page;
        $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
        ! empty ( $page ) && 1 < $page && $paged = $page;

        $paged > 1 && $s .= ' | ' . sprintf( __( 'Page: %s' ), $paged );

        return $s;
    }

    //add_filter( 'wpseo_title', 't5_add_page_number', 100, 1 );
    //add_filter( 'wpseo_metadesc', 't5_add_page_number', 100, 1 );
}

/*--------------------------------------------
Adding Business Schema
---------------------------------------------*/

function hennessey_add_business_schema(){
?>

  <script type='application/ld+json'>
  {
    "@context": "http://www.schema.org",
    "@type": "Attorney",
    "name": "Bressman Law",
    "url": "<?php echo site_url(); ?>",
    "sameAs": [
      "https://www.facebook.com/Bressman-Law-143111162614/",
      "https://twitter.com/dabressman",
      "https://www.linkedin.com/in/david-bressman-2a77258",
      "https://plus.google.com/100302340475839470715",
      "https://www.youtube.com/user/DavidBressman/videos"
    ],
    "logo": "<?php echo site_url(); ?>/wp-content/uploads/2013/08/BressmanLogo-420px.png",
    "image": "<?php echo site_url(); ?>/wp-content/uploads/2013/08/BressmanLogo-420px.png",
    "address": [
        {
          "@type": "PostalAddress",
          "streetAddress": "5186 Blazer Pkwy",
          "addressLocality": "Dublin",
          "addressRegion": "OH",
          "postalCode": "43017",
          "addressCountry": "United States"
        },
        {
          "@type": "PostalAddress",
          "streetAddress": "9435 Waterstone Blvd #130",
          "addressLocality": "Cincinnati",
          "addressRegion": "OH",
          "postalCode": "45249",
          "addressCountry": "United States"
        }
    ],
    "openingHoursSpecification" : {
      "@type" : "OpeningHoursSpecification",
      "dayOfWeek" : [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ],
      "opens" : "00:00:00",
      "closes" : "23:59:59"
    },
    "telephone": ["1-877-538-1116"],
    "priceRange": "$$"
  }

   </script>

<?php
}

add_action('wp_head', 'hennessey_add_business_schema');

/*--------------------------------------------
Adding Page Number to Title for Blogs
---------------------------------------------*/
function hennessey_output_page_number(){
  if( is_paged() ){
    $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
    return ' - Page ' . $currentPageNum;
  }
}

/*--------------------------------------------
Improved Page Navigation
---------------------------------------------*/

function numeric_posts_nav() {

  if( is_singular() )
    return;

  global $wp_query;

  /** Stop execution if there's only 1 page */
  if( $wp_query->max_num_pages <= 1 )
    return;

  $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
  $max   = intval( $wp_query->max_num_pages );

  /** Add current page to the array */
  if ( $paged >= 1 )
    $links[] = $paged;

  /** Add the pages around the current page to the array */
  if ( $paged >= 3 ) {
    $links[] = $paged - 1;
    $links[] = $paged - 2;
  }

  if ( ( $paged + 2 ) <= $max ) {
    $links[] = $paged + 2;
    $links[] = $paged + 1;
  }

  echo '<div class="pagination"><ul>' . "\n";

  /** Previous Post Link */
  if ( get_previous_posts_link() )
    printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

  /** Link to first page, plus ellipses if necessary */
  if ( ! in_array( 1, $links ) ) {
    $class = 1 == $paged ? ' class="active"' : '';

    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

    if ( ! in_array( 2, $links ) )
      echo '<li><span>...</span></li>';
  }

  /** Link to current page, plus 2 pages in either direction if necessary */
  sort( $links );
  foreach ( (array) $links as $link ) {
    $class = $paged == $link ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
  }

  /** Link to last page, plus ellipses if necessary */
  if ( ! in_array( $max, $links ) ) {
    if ( ! in_array( $max - 1, $links ) )
      echo '<li><span>...</span></li>' . "\n";

    $class = $paged == $max ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
  }

  /** Next Post Link */
  if ( get_next_posts_link() )
    printf( '<li>%s</li>' . "\n", get_next_posts_link() );

  echo '</ul></div>' . "\n";

}

/*--------------------------------------------
Favicons
---------------------------------------------*/

function hennessey_add_favicons(){
?>
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/hennessey/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/hennessey/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/hennessey/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/hennessey/favicon/manifest.json">
    <meta name="theme-color" content="#ffffff">
<?php
}

add_action('wp_head', 'hennessey_add_favicons');

/************************************
**
** Hennessey Consulting
**
************************************/
?>
