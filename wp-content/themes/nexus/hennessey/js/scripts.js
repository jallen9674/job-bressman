/*--------------------------------------------
Square4 General JavaScript
---------------------------------------------*/

/*--------------------------------------------
 ___ _            _     ___ _     _   _
 / __| |_ __ _ _ _| |_  |_ _| |_  | | | |_ __
 \__ \  _/ _` | '_|  _|  | ||  _| | |_| | '_ \
 |___/\__\__,_|_|  \__| |___|\__|  \___/| .__/
                                        |_|
---------------------------------------------*/

jQuery(document).ready(function($) {

// http://lokeshdhakar.com/projects/lightbox2/
// Initiating Lightbox
  lightbox.option({
    'resizeDuration': 200,
    'wrapAround': true,
    'fitImagesInViewport': false
  })

});

/*****************************************
Delaying Lucky Orange Loading
*****************************************/

function downloadJSAtOnload() {
    var element = document.createElement("script");
    element.innerHTML = " window.__lo_site_id = 77236;(function() { var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true; wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);})();";
    document.body.appendChild(element);
}

//if (jQuery(window).width() > 767) {
    if (window.addEventListener) window.addEventListener("load", downloadJSAtOnload, false);
    else if (window.attachEvent) window.attachEvent("onload", downloadJSAtOnload);
    else window.onload = downloadJSAtOnload;
//}
