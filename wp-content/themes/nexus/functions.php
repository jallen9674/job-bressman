<?php  // https://github.com/retlehs/roots/wiki

//REVISION: 0ab8e17a5746



if (!function_exists('_log')) {

    function _log($message)

    {

        if (is_array($message) || is_object($message)) {

            error_log(print_r($message, true));

        }

        else {

            error_log($message);

        }

    }

}



load_theme_textdomain('prime', get_template_directory() . '/lang');



if (!isset($content_width))

    $content_width = 584;



//Set some global vars for PRIME's usage

define('PRIME_THEME_ROOT_URI', trailingslashit(get_template_directory_uri()));

define('PRIME_THEME_NAME', 'nexus');

define('PRIME_THEME_DISPLAY_NAME', 'Nexus');

define('PRIME_THEME_VERSION', '0.0.0.1');

define('THEME_DIR', get_template_directory());

define('PRIME_DEVELOPMENT_MODE', true);

define('PRIME_OPTIONS_KEY', 'prime_options');

define('PRIME_PREVIEW', false);







//Import frontend-strings that have been centrally defined

require_once get_template_directory() . '/frontend-strings.php';

//import OptionTree

require_once get_template_directory() . '/prime-option/index.php';

//Import Roots

require_once get_template_directory() . '/inc/index.php';

//import Prime

require_once get_template_directory() . '/prime/index.php';

/*---------------------------------
Address Meta Tags
----------------------------------*/

function hennessey_add_geo(){
?>
   <?php //dublin ?>
   <?php if ( is_page('2170') ) : ?>
      <meta name="zipcode" content="43064" />
      <meta name="city" content="dublin" />
      <meta name="state" content="ohio" />
      <meta name="country" content="usa, united states of america" />
      <meta name="geo.region" content="US-OH" />
      <meta name="geo.placename" content="Dublin" />
      <meta name="geo.position" content="40.089229;-83.127907" />
      <meta name="ICBM" content="40.089229, -83.127907" />
  <?php endif; ?>

  <?php //cincinnati ?>
  <?php if ( is_page('2172') ) : ?>
      <meta name="zipcode" content="45202, 45203, 45204, 45205, 45206, 45207, 45208, 45209, 45212, 45214, 45216, 45217, 45219, 45220, 45223, 45224, 45225, 45226, 45227, 45229, 45230, 45231, 45232, 45239, 45243" />
      <meta name="city" content="cincinnati" />
      <meta name="state" content="ohio" />
      <meta name="country" content="usa, united states of america" />
      <meta name="geo.region" content="US-OH" />
      <meta name="geo.placename" content="Cincinnati" />
      <meta name="geo.position" content="39.299744;-84.302505" />
      <meta name="ICBM" content="39.299744, -84.302505" />
  <?php endif; ?>


<?php
}
add_action('wp_head', 'hennessey_add_geo');

//Hennessey Adding Lucky Orange
function hennessey_add_lucky_orange(){
    ?>
        <script type='text/javascript'>
        window.__lo_site_id = 77236;

        (function() {
        var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
        wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
         })();
        </script>
    <?php
}

//add_action('wp_footer', 'hennessey_add_lucky_orange');


// Bringing in Hennessey Optimizations

require_once('hennessey/hennessey_functions.php');
?>
