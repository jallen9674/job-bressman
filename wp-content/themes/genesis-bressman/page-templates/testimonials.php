<?php
// Template name: Testimonials

remove_action( 'genesis_loop', 'genesis_do_loop', 10 );
add_action( 'genesis_loop', 'custom_testimonials_loop', 10 );
function custom_testimonials_loop(){
    global $post;
?>
    <div class="innerPage">
        <article <?php post_class(); ?>>
            <header class="article-header">
    			<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
    		</header>
            <section class="entry-content cf">
                <?php
                    the_content();
                    $testiPageArgs = array(
                        'post_type' => 'bressman_reviews',
                        'posts_per_page' => -1
                    );
                    $tpa_query = new WP_Query($testiPageArgs);
                    while ($tpa_query->have_posts()) : $tpa_query->the_post();
                ?>
                <div class="single-review" itemscope="" itemtype="http://schema.org/Review">
                    <?php
                        if ( has_post_thumbnail() ) {
                          $image_array = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'review-image' );
                          $image = $image_array[0];
                          echo '<img src="' . $image .'" class="single-review-image" alt="Client Review">';
                        } else {
                          echo '<img src="'.CHILD_URL. '/assets/app/images/review-unknown.png" class="single-review-image" alt="Client '.get_post_meta( $post->ID, '_bressman_review_name', true ).' Review">';
                        }
                    ?>
                <div class="review-right">
                <div class="review-name">
                      <h3 itemprop="author" class="review-name-content"><?php echo get_post_meta( $post->ID, '_bressman_review_name', true ); ?></h3>
                    <div class="review-rating" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating">
                        <meta itemprop="worstRating" content="1">
                        <?php
                            if ( get_post_meta( $post->ID, '_bressman_review_rating', true ) ) {
                                $reviewRating = get_post_meta( $post->ID, '_bressman_review_rating', true );
                                $reviewRatingEmpty = 5 - $reviewRating;

                                for($i = 0; $i < intval($reviewRating); $i++){
                                    echo '<img src="'.CHILD_URL.'/assets/app/images/review-star.png" alt="Star" class="review-star no-border" /> ';
                                }
                                for($i = 0; $i < intval($reviewRatingEmpty); $i++){
                                    echo '<img src="'.CHILD_URL.'/assets/app/images/review_star_empty.png" alt="Star" class="review-star no-border" /> ';
                                }
                            }
                            if ( get_post_meta( $post->ID, '_bressman_review_rating', true ) ) {
                                $reviewRating = get_post_meta( $post->ID, '_bressman_review_rating', true );
                                echo '<meta itemprop="ratingValue" content="'.$reviewRating.'">
                                     <meta itemprop="bestRating" content="5">';
                            }
                        ?>
                    </div>
                </div>
                  <div class="review-date"><?php echo 'Posted '. human_time_diff( get_the_date( 'U' ) ) . ' ago'; ?></div>


                  <div itemprop="itemReviewed" itemscope="" itemtype="http://schema.org/Service" class="item-reviewed">
                      <meta itemprop="name" content="Legal Services">
                  </div>

                  <?php
                      if ( get_post_meta( $post->ID, '_bressman_review_feedback', true ) ) {
                          ?>

                          <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Service" class="item-reviewed">
                              <meta itemprop="name" content="Legal Services">
                          </div>

                          <?php
                          echo '<p class="review-content" itemprop="description">'.get_post_meta( $post->ID, '_bressman_review_feedback', true ).'</p>';
                       }
                  ?>
                </div>
                <hr class="review-list-seperator">
                </div>
                <?php endwhile; wp_reset_query(); ?>
            </section>
        </article>
    </div>
<?php
}

genesis();
