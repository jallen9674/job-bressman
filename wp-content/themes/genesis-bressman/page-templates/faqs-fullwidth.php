<?php
/*
 * Template Name: FAQs Fullwidth
 *
 */

 remove_action( 'genesis_sidebar', 'genesis_do_sidebar', 10 );
 remove_action( 'genesis_entry_content', 'custom_post_thumbnail', 8 );
 remove_action( 'genesis_entry_content', 'genesis_do_post_content', 10 );

 add_action( 'genesis_entry_content', 'faqs_content', 0 );
 function faqs_content(){
     global $post;
     ?>
        <section class="faqs_content">
            <div class="faq-filter-portfolio-wrapper">

	            <div id="filters" class="faq-filter-category-list filters-button-group">
	                <?php //Output all Categories
	                echo '<button class="isotope-link is-checked" data-filter="*">All</button>';
	                 $terms = get_terms( 'bressman_faq');
	                 if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){

	                     foreach ( $terms as $term ) {
	                       echo '<button class="isotope-link" data-filter=".' . $term->slug . '"><span>' . $term->name . '</span></button>';

	                     }

	                 }
	                ?>
	            </div>

	            <div class="faq-filter-list">
	                <?php
	                		$faqTerms = get_terms( 'bressman_faq' );
						// convert array of term objects to array of term IDs
						$faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

	                    $args = array(
	                      'posts_per_page' => -1,
	                      'post_type' => 'page',
	                      'tax_query' => array(
								array(
									'taxonomy' => 'bressman_faq',
									'field' => 'term_id',
									'terms' => $faqTermIDs
								),
							),
	                      'order' => 'DSC',
	                    );

	                    $the_query = new WP_Query( $args );
	                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
	                  ?>
	                    <?php //Getting Category for Filtering
	                        $postTerms =  wp_get_object_terms($post->ID, 'bressman_faq');
	                        $categoryFilterSlug = '';
	                        $categoryPrettyName = '';
	                        if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
	                             foreach ( $postTerms as $term ) {
	                               $categoryFilterSlug .= ' ' . $term->slug;
	                               $categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
	                             }
	                         }
	                     ?>

	                    <div class="faq-filter-listing  grid-item <?php echo $categoryFilterSlug; ?> ">

		                    <a href="<?php the_permalink(); ?>" class="faq-filter-image-link">
		                    <?php
		                        if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		                            the_post_thumbnail('faq-thumb');
		                        } else{
		                        	echo '<img alt="faq thumb" src="' . CHILD_URL . '/assets/app/images/default-faq-thumb.jpg" />';
		                        }
		                    ?>
							<span class="faq-date"><?php echo get_the_date('m-j-Y');  ?></span>
		                    </a>
		                   <div class="faq-meta">
		                    	<span class="faq-category"><?php echo $categoryPrettyName; ?></span>
		                    </div>
		                    <a href="<?php the_permalink(); ?>" class="faq-filter-title-link"><?php the_title(); ?></a>

							<a href="<?php the_permalink(); ?>" class="continue-reading-button">Continue Reading &raquo;</a>

	                    </div>
	                  <?php endwhile; else : ?>
	                    <!-- IF NOTHING FOUND CONTENT HERE -->
	                  <?php endif; ?>
	                  <?php wp_reset_query(); ?>
	            </div>

	        </div>
        </section>
     <?php
 }

 genesis();
