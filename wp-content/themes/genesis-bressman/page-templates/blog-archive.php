<?php
/*
 * Template Name: Blog Archive
 *
 */

 remove_action( 'genesis_entry_content', 'genesis_do_post_content', 10 );

 add_action( 'genesis_entry_content', 'blog_archive_content', 10 );
 function blog_archive_content(){
     $args = array(
         'post_type' => 'post',
         'posts_per_page' => -1
     );
     $bp_query = new WP_Query($args);
     echo '<ul class="bp_list">';
         while ($bp_query->have_posts()) : $bp_query->the_post();
            echo '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>';
         endwhile;
     echo '</ul>';
 }

 genesis();
