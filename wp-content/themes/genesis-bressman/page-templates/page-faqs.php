<?php
/*
 * Template Name: FAQs Listing
 * */

add_action('genesis_entry_content', 'custom_faqs_content', 20);

function custom_faqs_content() {
    global $post;
    global $in_single_content;
    global $hc_settings;
    $in_single_content = false;
    ?>


                <div id="filters" class="faq-filter-terms">
                    <?php echo '<a href="#" class="filter-active" data-filter="all">All</a>';
                    $terms = get_terms($hc_settings['faqs_category_taxonomy']);
                    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){

                        foreach ( $terms as $term ) {
                            echo '<a href="#" data-filter="' . $term->slug . '">' . $term->name . '</a>';

                        }

                    }
                    ?>
                </div>

                <div id="items" class="faq-filter-items">
                    <?php

                    $args = array(
                        'posts_per_page' => -1,
                        'post_type' => 'page',
                        'tax_query' => array(
                            array(
                                'taxonomy' => $hc_settings['faqs_category_taxonomy'],
                                'operator' => "EXISTS"
                            ),
                        ),
                        'order' => 'DSC',
                    );

                    $the_query = new WP_Query( $args );
                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();

                        $the_terms = get_the_terms($post->ID, $hc_settings['faqs_category_taxonomy']);

                        if($the_terms) {
                            $the_terms = current($the_terms);
                            $term = $the_terms->slug;
                        } else {
                            $term = '';
                        }

                        $pid = $post->ID; ?>

                        <div class="faq-page-listing" data-filter-item="<?=$term?>">

                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="faq-page__link-env">
                                <h4 class="faq-page__title"><?php the_title(); ?></h4>
                                <?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('medium', ['alt' => get_the_title(), 'title' => get_the_title()]);
                                } else {
                                    echo '<figure class="alignleft archive-entry-content-thumbnail"><img alt="faq thumb" src="' . CHILD_URL . '/assets/app/img/default-thumb.jpg"></figure>';
                                } ?>
                            </a>
                            <div class="faq-meta">
                                <?php echo get_the_term_list( $pid, $hc_settings['faqs_category_taxonomy'] ); ?>
                            </div>

                        </div>


                    <?php endwhile; else : ?>
                        <!-- IF NOTHING FOUND CONTENT HERE -->
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>

    <?php

    unset($in_single_content);

}



genesis();
