<?php


remove_action('genesis_loop', 'genesis_do_loop', 10);
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

remove_action('genesis_before_content_sidebar_wrap', 'custom_do_breadcrumbs', 5);

include('parts/header-home.php');

add_action( 'genesis_loop', 'front_page_content', 10 );
function front_page_content(){
    global $post;
?>
    <section class="homepage-section-2 homepage-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="homepage-section-title">
    					<div class="homepage-section-title__inner">
    						<h1 class="homepageTitle">
                                Serving Ohio for Over 25 Years
                            </h1>
    					</div>
    				</h2>
                    <div class="homepage-section-2__intro">
    					<span class="homepage-section-2__subtitle">
    						A Columbus Personal Injury Firm Built on 3 Things
						</span>
						<div class="homepage-section-2__content">
							<p style="text-align: center;">Experience. Communication. Transparency.</p>
							<p>We work with the insurance company to settle your case, but if we can’t reach a fair settlement, we use our 25 years of courtroom experience to <strong>get you the money you need</strong>. Our firm’s unique communication system guarantees <strong>you won’t play phone tag when you call us</strong>. And our policy of transparency means we share all documents and keep you updated on your case’s status, so <strong>you aren’t left in the dark</strong>.</p>
							<p><strong>Watch the videos below to learn more about what makes a good personal injury attorney.</strong></p>

						</div>
    				</div>
                    <div class="row homepage-section-2__row-1">
                        <div class="one-half">
                            <div class="arve-wrapper" data-mode="normal" data-provider="html5" id="arve-httpswwwbressmanlawcomwp-contentuploads201812BressmanLawIntro-webmp4" style="max-width:640px;" itemscope="" itemtype="http://schema.org/VideoObject">
                                <div class="arve-embed-container lazyloaded" style="height:auto;padding:0">
                                    <meta itemprop="thumbnailUrl" content="<?php echo home_url(); ?>/wp-content/uploads/2018/12/Bressman-Intro-Video-thumbnail.jpg">
                                    <video controls <?php //echo 'controlsList="nodownload"'; ?> preload="none" poster="<?php echo home_url(); ?>/wp-content/uploads/2018/12/Bressman-Intro-Video-thumbnail.jpg" class="arve-video fitvidsignore">
                                        <source type="video/mp4" src="<?php echo home_url(); ?>/wp-content/uploads/2018/12/BressmanLaw_Intro-web.mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
    					<div class="one-half">
    						<div class="homepage-section-2__video">
    							<div class="iframe-wrapper iframe-wrapper-16x9">
    								<iframe width="560" height="315" src="https://www.youtube.com/embed/Q1jGChnLQ68" <?php //echo 'frameborder="border"'; ?> allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    							</div>
    						</div>
    					</div>
    				</div>
                    <div class="row homepage-section-2__row-2">
                        <div class="col-md-6">
                            <img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/no-fee-guarantee.png" alt="No fee guarantee">
                        </div>
                        <div class="col-md-6">
                            <div class="homepage-section-2__contact-form standard-form theme_1 noErrorMsg">
    							<span class="homepage-section-2__contact-form-evaluation-text">
    								FREE CASE EVALUATION
    							</span>
    							<span class="homepage-section-2__contact-form-title">
    								Call <a href="tel:+18777071385" data-wpel-link="internal" data-ctm-watch-id="5" data-ctm-tracked="1" data-observe="1" data-observer-id="2">1-877-707-1385</a>
    							</span>
    							<span class="homepage-section-2__contact-form-subtitle">
    								- or fill out the form below -
    							</span>
                            	<?php echo do_shortcode( '[contact-form-7 id="2412" title="Homepage Contact Form"]' ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row homepage-section-2__row-3">
    					<h2 class="homepage-section-2__scholarship-title">
    						Announcing the Bressman Law Traumatic Brain Injury Scholarship
    					</h2>
    					<h3 class="homepage-section-2__scholarship-subtitle">
    						Applications Can Be Submitted through December 15th, 2020.
    					</h3>
    					<div class="homepage-section-2__scholarship-button-wrapper">
    						<a href="<?php echo home_url('/scholarship/'); ?>" class="button-orange button homepage-section-2__scholarship-button" data-wpel-link="internal">
    							<div class="button-orange__gradient-helper">
    							</div>
    							<span class="text-outer">
    								<span class="text-inner">View Scholarship Details »</span>
    							</span>
    						</a>
    					</div>
    				</div>
                </div>
            </div>
        </div>
    </section>
    <section class="section3">
        <div class="homepage-section-3 homepage-section parallax-window" data-parallax="scroll" data-z-index="1" data-image-src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/practice-areas/practice-areas-bg.jpg" id="homepage-section-3">
			<div class="homepage-section-3__inner wrapper">

				<h2 class="homepage-section-title">
					<span class="homepage-section-title__inner">
						Have Legal Questions? We Have Answers
					</span>
				</h2>

				<div class="homepage-section-3__callout">
					If you or someone you love has sustained an injury in any of these types of accidents caused by negligence, contact the personal injury team at Bressman Law to find out how we can help you get compensation.
				</div>

				<div class="homepage-section-3__practice-area-grid">

										<a href="<?php echo home_url(); ?>/columbus-injury/car-accident-lawyer/" class="homepage-practice-area" data-wpel-link="internal">
						<span class="homepage-practice-area__title">Car Accidents</span>
						<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/practice-areas/car.png" alt="Car Accidents" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							Your car accident case has to prove fault to hold the negligent driver liable. We can help you get the money you deserve.
						</p>
											</a>
										<a href="<?php echo home_url(); ?>/columbus-injury/truck-accident-lawyer/" class="homepage-practice-area" data-wpel-link="internal">
						<span class="homepage-practice-area__title">Truck Accidents</span>
						<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/practice-areas/truck.png" alt="Truck Accidents" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							The truck carrier is liable if a trucker caused your accident. We help you navigate complicated truck accident cases.
						</p>
											</a>
										<a href="<?php echo home_url(); ?>/columbus-injury/bus-accident-lawyer/" class="homepage-practice-area" data-wpel-link="internal">
						<span class="homepage-practice-area__title">Bus Accidents</span>
						<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/practice-areas/bus.png" alt="Bus Accidents" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							Passengers, pedestrians, and other drivers can hold bus companies liable for accidents. We make sure you get what you deserve.
						</p>
											</a>
										<a href="<?php echo home_url(); ?>/columbus-injury/motorcycle-accident-lawyer/" class="homepage-practice-area" data-wpel-link="internal">
						<span class="homepage-practice-area__title">Motorcycle Accidents</span>
						<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/practice-areas/motorbike.png" alt="Motorcycle Accidents" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							Motorcycle cases come with complexities and biases that other accidents do not have. We help you get the money you need.
						</p>
											</a>
										<a href="<?php echo home_url(); ?>/" class="homepage-practice-area" data-wpel-link="internal">
						<span class="homepage-practice-area__title">Personal Injury</span>
						<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/practice-areas/hospital.png" alt="Personal Injury" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							If negligence caused your accident, you can hold the at-fault party liable for bills, losses, and other damages. We can help.
						</p>
											</a>
										<a href="<?php echo home_url(); ?>/columbus-injury/product-liability-lawyer/" class="homepage-practice-area" data-wpel-link="internal">
						<span class="homepage-practice-area__title">Product Liability</span>
						<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/practice-areas/airbag.png" alt="Product Liability Lawyer" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							If a defective product injured you, the manufacturer could be liable. We help you get money for your losses.
						</p>
											</a>
										<a href="<?php echo home_url(); ?>/columbus-injury/traumatic-brain-injury-tbi-lawyer/" class="homepage-practice-area" data-wpel-link="internal">
						<span class="homepage-practice-area__title">Traumatic Brain Injury</span>
						<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/practice-areas/brain-mri.png" alt="Traumatic Brain Injury" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							A TBI has long-lasting effects. We help you get money to cover your current and future bills, losses, and needs.
						</p>
											</a>
										<a href="<?php echo home_url(); ?>/columbus-injury/wrongful-death-lawyer/" class="homepage-practice-area" data-wpel-link="internal">
						<span class="homepage-practice-area__title">Wrongful Death</span>
						<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/practice-areas/death.png" alt="Wrongful Death" class="homepage-practice-area__image">
						<p class="homepage-practice-area__description">
							If negligence caused your loved one’s death, we hold the liable party responsible for your family’s losses.
						</p>
											</a>
				</div>

			</div>
		</div>
    </section>
    <section class="section4">
        <div class="homepage-section-4 homepage-section">

			<div class="homepage-section-4__inner wrapper">

				<h2 class="homepage-section-title">
					<span class="homepage-section-title__inner">
						Available 24/7 - Will Travel To You
					</span>
				</h2>

				<span class="homepage-section-4__callout">
					Proudly Serving Ohio For Over 25 Years
				</span>

				<div class="homepage-city-list">

				<a href="<?php echo home_url(); ?>" class="homepage-city" data-wpel-link="internal">
					<span class="homepage-city__title">Columbus</span>
					<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/locations/columbus.png" alt="Columbus Location" class="homepage-city__image">
					<p class="homepage-city__description">
						We serve the people of Columbus and the surrounding metro area within and outside the I-270 Outerbelt.
					</p>
				</a>

									<a href="<?php echo home_url(); ?>/dublin-injury/" class="homepage-city" data-wpel-link="internal">
					<span class="homepage-city__title">Dublin</span>
					<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/locations/dublin.png" alt="Dublin Location" class="homepage-city__image">
					<p class="homepage-city__description">
						Our main office is in Dublin west of the Scioto River and easily accessible from I-270 and SR-33.
					</p>
				</a>

									<a href="<?php echo home_url(); ?>/hilliard-injury/" class="homepage-city" data-wpel-link="internal">
					<span class="homepage-city__title">Hilliard</span>
					<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/locations/hilliard.png" alt="Hilliard Location" class="homepage-city__image">
					<p class="homepage-city__description">
						Injured in Hilliard? Come to our main office location, accessible via the I-270 Outerbelt or we can come to you.
					</p>
				</a>

									<a href="<?php echo home_url(); ?>/upper-arlington-injury/car-accident-lawyer/" class="homepage-city" data-wpel-link="internal">
					<span class="homepage-city__title">Upper Arlington</span>
					<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/locations/upper-arlington.png" alt="Upper Arlington Location" class="homepage-city__image">
					<p class="homepage-city__description">
						We invite those in Upper Arlington to visit our main office, accessible via SR-33 or the I-270 Outerbelt, or we can come to you.
					</p>
				</a>

									<a href="<?php echo home_url(); ?>/gahanna-injury/" class="homepage-city" data-wpel-link="internal">
					<span class="homepage-city__title">Gahanna</span>
					<img src="<?php echo home_url(); ?>/wp-content/themes/davidbressman/assets/images/homepage/locations/gahanna.png" alt="Gahanna Location" class="homepage-city__image">
					<p class="homepage-city__description">
						Residents of Gahanna can access our main office via the I-270 Outerbelt, or we can come to you.
					</p>
				</a>

				</div>
				<p class="homepage-section-4__city-service-link-wrapper">
					<a href="<?php echo home_url(); ?>/areas-we-serve/" class="homepage-section-4__city-service-link" data-wpel-link="internal">See Full List of Cities We Service »</a>
				</p>

			</div>
		</div>
    </section>
    <section class="section5">
        <!-- Results -->
        <div class="homepage-section-5 homepage-section" data-parallax="scroll" data-z-index="1" data-bleed="60" data-image-src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/homepage-verdict-bg.jpg" id="homepage-section-5">
			<div class="homepage-section-5__inner wrapper">

				<h2 class="homepage-section-title">
					<span class="homepage-section-title__inner">
						We've Helped Thousands of People Just Like You
					</span>
				</h2>

				<span class="homepage-section-5__callout">
					Case Results
				</span>

				<div class="homepage-case-results-list">

					<div class="homepage-case-result">
						<span class="homepage-case-result__verdict">
							Verdict of $1,450,000
						</span>
						<span class="homepage-case-result__description">
							Insurance company offered nothing. We won a $1.45 million verdict.
						</span>
					</div>

					<div class="homepage-case-result">
						<span class="homepage-case-result__verdict">
							Verdict of $415,576
						</span>
						<span class="homepage-case-result__description">
							Insurance company offered $0. We won $415,576 in court.
						</span>
					</div>

					<div class="homepage-case-result">
						<span class="homepage-case-result__verdict">
							Verdict of $180,000
						</span>
						<span class="homepage-case-result__description">
							Insurance company offered $0. We won $180,000 at trial.
						</span>
					</div>

					<div class="homepage-case-result">
						<span class="homepage-case-result__verdict">
							Verdict of $100,000
						</span>
						<span class="homepage-case-result__description">
							Insurance company offered $5,000. We won $100,000 in court.
						</span>
					</div>

				</div>
 				<div class="homepage-section-5__button-wrapper">
					<a href="https://www.bressmanlaw.com/verdicts/" class="button-orange button" data-wpel-link="internal">
						<div class="button-orange__gradient-helper">
						</div>
						<span class="text-outer">
							<span class="text-inner">See More Case Results »</span>
						</span>
					</a>
				</div>

			</div>
		</div>
    </section>
    <section class="section6">
        <div class="homepage-section-6 homepage-section">

			<div class="homepage-section-6__inner wrapper">

				<h2 class="homepage-section-title">
					<span class="homepage-section-title__inner">
						Testimonials
					</span>
				</h2>

								<div class="homepage-section-6__review-wrapper">

                <?php
                    $testiArgs = array(
                        'post_type' => 'bressman_reviews',
                        'posts_per_page' => 4
                    );
                    $testi_query = new WP_Query($testiArgs);
                    while ($testi_query->have_posts()) : $testi_query->the_post();
                ?>

                    <div class="single-review" itemscope="" itemtype="http://schema.org/Review">
                        <?php
                            if ( has_post_thumbnail() ) {
                              $image_array = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'review-image' );
                              $image = $image_array[0];
                              echo '<img src="' . $image .'" class="single-review-image" alt="Client Review">';
                            } else {
                              echo '<img src="'.CHILD_URL. '/assets/app/images/review-unknown.png" class="single-review-image" alt="Client '.get_post_meta( $post->ID, '_bressman_review_name', true ).' Review">';
                            }
                        ?>
                        <div class="review-right">
                            <div class="review-name">
                                <div class="review-rating" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating">
                                    <meta itemprop="worstRating" content="1">
                                    <?php
                                        if ( get_post_meta( $post->ID, '_bressman_review_rating', true ) ) {
                                            $reviewRating = get_post_meta( $post->ID, '_bressman_review_rating', true );
                                            $reviewRatingEmpty = 5 - $reviewRating;

                                            for($i = 0; $i < intval($reviewRating); $i++){
                                                echo '<img src="'.CHILD_URL.'/assets/app/images/review-star.png" alt="Star" class="review-star no-border" /> ';
                                            }
                                            for($i = 0; $i < intval($reviewRatingEmpty); $i++){
                                                echo '<img src="'.CHILD_URL.'/assets/app/images/review_star_empty.png" alt="Star" class="review-star no-border" /> ';
                                            }
                                        }
                                        if ( get_post_meta( $post->ID, '_bressman_review_rating', true ) ) {
                                            $reviewRating = get_post_meta( $post->ID, '_bressman_review_rating', true );
                                            echo '<meta itemprop="ratingValue" content="'.$reviewRating.'">
                                                 <meta itemprop="bestRating" content="5">';
                                        }
                                    ?>
                                </div>
                                <h3 itemprop="author" class="review-name-content"><?php echo get_post_meta( $post->ID, '_bressman_review_name', true ); ?></h3>
                            </div>
                          <div class="review-date"><?php echo 'Posted '. human_time_diff( get_the_date( 'U' ) ) . ' ago'; ?></div>


                          <?php
                              if ( get_post_meta( $post->ID, '_bressman_review_feedback', true ) ) {
                                  ?>

                                  <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Service" class="item-reviewed">
                                      <meta itemprop="name" content="Legal Services">
                                  </div>

                                  <?php
                                  echo '<p class="review-content" itemprop="description">'.wp_trim_words( get_post_meta( $post->ID, '_bressman_review_feedback', true ), 45, '...').'</p>';
                               }
                          ?>
                      </div>
                    </div>

                <?php endwhile; wp_reset_query(); ?>

				</div>


 				<div class="homepage-section-6__button-wrapper">
					<a href="<?php echo home_url('/testimonials/'); ?>" class="button-orange button" data-wpel-link="internal">
						<div class="button-orange__gradient-helper">
						</div>
						<span class="text-outer">
							<span class="text-inner">See More Testimonials »</span>
						</span>
					</a>
				</div>

			</div>

		</div>
    </section>
<?php
}
genesis();
