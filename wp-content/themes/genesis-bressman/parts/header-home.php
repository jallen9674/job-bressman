<?php
    add_action( 'genesis_header', 'home_header', 10 );
    function home_header(){
        global $hc_settings;
?>
<section class="mobile_menu_layer hidden-md hidden-lg">
    <?php
        wp_nav_menu(
            array(
                'theme_location' => 'primary',
                'menu_class' => 'menu-main-mobile',
                'menu_id' => 'menu-main-mobile'
            )
        );
    ?>
</section>
<header class="header">
    <div class="container">
        <div class="row middleHeader">
            <div class="col-xs-2 col-sm-2 hidden-md hidden-lg">
                <a href="tel:<?php echo $hc_settings['phone_number']; ?>" class="mobile_btn mobile_menu_btn">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                </a>
            </div>
            <div class="col-md-6 col-sm-8 col-xs-8">
                <div class="site_logo">
                    <a href="<?php echo home_url(); ?>">
                        <img height="50" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/app/images/bressman-law-logo.png" alt="<?php bloginfo('name'); ?>">
                    </a>
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 hidden-md hidden-lg">
                <button type="button" name="button" class="mobile_btn mobile_menu_btn">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
            </div>
            <div class="col-md-6 hidden-xs hidden-sm">
                <div class="headerPhone">
                    <span class="hPtext">
                        Call Us Today:
                    </span>
                    <a href="tel:<?php echo $hc_settings['phone_number']; ?>"><?php echo $hc_settings['phone_number']; ?></a>
                </div>
            </div>
        </div>
        <div class="row hidden-xs hidden-sm">
            <div class="col-md-12">
                <div class="top-nav-wrapper">
                    <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'primary',
                                'menu_class' => 'nav top-nav cf',
                                'menu_id' => 'menu-main'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="homeBanner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="latestTestimonial">
                    <?php
                        $tmArgs = array(
                            'post_type' => 'bressman_reviews',
                            'posts_per_page' => 1
                        );
                        $tmQuery = new WP_Query($tmArgs);
                        while ($tmQuery->have_posts()) : $tmQuery->the_post();
                    ?>
                        <div class="tmText">
                            <?php echo wp_trim_words( get_post_meta( get_the_ID(), '_bressman_review_feedback', true), 35, '...' ); ?>
                        </div>
                        <div class="tmName">
                            <?php the_title(); echo ' - '; the_time('Y'); ?>
                        </div>
                    <?php endwhile; wp_reset_query(); ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="attorneysPhotos" style="justify-content: center">
<!--                     <div class="attorneyBlock attorneyLeft">
                        <img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/tom-martello-portrait.png" alt="David Bressman" />
                    </div> -->
                    <div class="attorneyBlock attorneyMiddle">
                        <div class="testimonialsViewAllLink">
                            <a href="<?php echo home_url('/testimonials/'); ?>" class="button-orange button homepage-section-1__testimonials-button" data-wpel-link="internal">
								<div class="button-orange__gradient-helper">
								</div>
								<span class="text-outer">
									<span class="text-inner">View More Testimonials »</span>
								</span>
							</a>
                        </div>
<!--                         <img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/logo-top100.png" alt="Top 100 Logo" class="homepage-section-1__logo">
    					<img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/logo-aaj.png" alt="AAJ Logo" class="homepage-section-1__logo">
    					<img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/logo-bbb.png" alt="BBB Logo" class="homepage-section-1__logo">
    					<img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/logo-clf-seal.png" alt="CLF Seal" class="homepage-section-1__logo">
    					<img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/logo-aplita-america.png" alt="APLITAmerica Logo" class="homepage-section-1__logo"> -->
                    </div>
<!--                     <div class="attorneyBlock attorneyRight">
                        <img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/david-bressman-portrait.png" alt="David Bressman">
                    </div> -->
                </div>
            </div>
            <!-- <div class="col-md-4 attorneyLeft">
                <img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/tom-martello-portrait.png" alt="David Bressman" />
            </div>
            <div class="col-md-4">
                <div class="homepage-section-1__logos">
					<img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/logo-top100.png" alt="Top 100 Logo" class="homepage-section-1__logo">
					<img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/logo-aaj.png" alt="AAJ Logo" class="homepage-section-1__logo">
					<img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/logo-bbb.png" alt="BBB Logo" class="homepage-section-1__logo">
					<img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/logo-clf-seal.png" alt="CLF Seal" class="homepage-section-1__logo">
					<img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/logo-aplita-america.png" alt="APLITAmerica Logo" class="homepage-section-1__logo">
				</div>
            </div>
            <div class="col-md-4 attorneyRight">
                <img src="<?php echo CHILD_URL; ?>/assets/app/images/homepage/david-bressman-portrait.png" alt="David Bressman">
            </div> -->
        </div>
    </div>
</section>
<?php } ?>
