<?php global $hc_settings; ?>
<section class="mobile_menu_layer hidden-md hidden-lg">
    <?php
        wp_nav_menu(
            array(
                'theme_location' => 'primary',
                'menu_class' => 'menu-main-mobile',
                'menu_id' => 'menu-main-mobile'
            )
        );
    ?>
</section>
<header class="header solidBgInner">
    <div class="container">
        <div class="row middleHeader">
            <div class="col-xs-2 col-sm-2 hidden-md hidden-lg">
                <a href="tel:<?php echo $hc_settings['phone_number']; ?>" class="mobile_btn mobile_menu_btn">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                </a>
            </div>
            <div class="col-md-6 col-sm-8 col-xs-8">
                <div class="site_logo">
                    <a href="<?php echo home_url(); ?>">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/app/images/bressman-law-logo.png" alt="<?php bloginfo('name'); ?>">
                    </a>
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 hidden-md hidden-lg">
                <button type="button" name="button" class="mobile_btn mobile_menu_btn">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
            </div>
            <div class="col-md-6 hidden-xs hidden-sm">
                <div class="headerPhone">
                    <span class="desktop-header__contact-intro-text">
						Call Us Today:
					</span>
                    <a href="tel:<?php echo $hc_settings['phone_number']; ?>"><?php echo $hc_settings['phone_number']; ?></a>
                </div>
            </div>
        </div>
        <div class="row hidden-xs hidden-sm">
            <div class="col-md-12">
                <div class="top-nav-wrapper">
                    <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'primary',
                                'menu_class' => 'nav top-nav cf',
                                'menu_id' => 'menu-main'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
</header>
