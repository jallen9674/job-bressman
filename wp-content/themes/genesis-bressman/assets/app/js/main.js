"use strict";

(function ($) {



    $('#filters a').click(function(e){
        e.preventDefault();

        var term = $(this).data('filter')
        $(this).addClass('filter-active').siblings().removeClass('filter-active');

        if(term === 'all') {
            $('#items > div').fadeIn();
        } else {
            $('#items > div').fadeOut(0);
            $('#items > div[data-filter-item="'+term+'"]').fadeIn();

            if($('#items > div[data-filter-item="'+term+'"]').length == 2) {
                $('#items').css('justify-content', 'space-around');
            } else {
                $('#items').css('justify-content', 'space-between');
            }
        }
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 10) {
            $('header.header').addClass('solidBg');
        } else {
            $('header.header').removeClass('solidBg');
        }
    });

    $('button.mobile_menu_btn').toggle(function() {
        $(this).html('<i class="fa fa-times" aria-hidden="true"></i>');
        $('.mobile_menu_layer').addClass('active');
    }, function() {
        $(this).html('<i class="fa fa-bars" aria-hidden="true"></i>');
        $('.mobile_menu_layer').removeClass('active');
    });



})(jQuery);
