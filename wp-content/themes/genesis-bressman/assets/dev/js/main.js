"use strict";

(function ($) {



    $('#filters a').click(function(e){
        e.preventDefault();

        var term = $(this).data('filter')
        $(this).addClass('filter-active').siblings().removeClass('filter-active');

        if(term === 'all') {
            $('#items > div').fadeIn();
        } else {
            $('#items > div').fadeOut(0);
            $('#items > div[data-filter-item="'+term+'"]').fadeIn();

            if($('#items > div[data-filter-item="'+term+'"]').length == 2) {
                $('#items').css('justify-content', 'space-around');
            } else {
                $('#items').css('justify-content', 'space-between');
            }
        }
    });



})(jQuery);