<?php

add_action( 'wp_enqueue_scripts', 'custom_include_assets' );

function custom_include_assets() {
    $jsArray = [
//        CHILD_URL . '/assets/libs/izimodal/js/iziModal.min.js',
//        CHILD_URL . '/assets/libs/swiper/js/swiper.min.js',
//        CHILD_URL . '/assets/libs/page-scroll-to-id/js/jquery.malihu.PageScroll2id.min.js'
    ];

    $cssArray = [
//        CHILD_URL . '/assets/libs/izimodal/css/iziModal.min.css',
//        CHILD_URL . '/assets/libs/swiper/css/swiper.min.css'
    ];

    foreach($cssArray as $css_asset) {
        wp_enqueue_style( basename($css_asset), $css_asset );
    }

    foreach($jsArray as $js_asset) {
        wp_enqueue_script( basename($js_asset), $js_asset, array('jquery'), '1.0.0', true );
    }


    // include general theme files
    wp_enqueue_style( 'theme-fonts', '//fonts.googleapis.com/css?family=Cormorant+Garamond:300,300i,400,400i,500,500i,600,600i,700,700i|Exo+2:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Exo:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Oswald:200,300,400,500,600,700&display=swap&subset=cyrillic,cyrillic-ext,latin-ext,vietnamese' );
    wp_enqueue_style( 'theme-main', CHILD_URL . '/assets/app/css/main.css' );
    wp_enqueue_style( 'theme-style-old', CHILD_URL . '/assets/app/css/style.old.css' );
    wp_enqueue_style( 'theme-grid', CHILD_URL . '/assets/app/css/grid.css' );
    wp_enqueue_style( 'theme-style', CHILD_URL . '/assets/app/css/style.css' );

    wp_enqueue_script( 'theme-parallax', CHILD_URL . '/assets/app/js/simpleparallax.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'theme-combined', CHILD_URL . '/assets/app/js/scripts.combined.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'theme-main', CHILD_URL . '/assets/app/js/main.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'theme-apex-chat', '//www.apex.live/scripts/invitation.ashx?company=bressmanlaw', array('jquery'), '1.0.0', true );
}

// Lighthouse
if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') !== false) {
    add_filter('wpcf7_load_js', '__return_false');
    add_filter('wpcf7_load_css', '__return_false');

    remove_action('wp_footer', 'usw_addplugin_footer_notice');

    remove_action('wp_footer', 'add_footer_chat');

    remove_action('genesis_footer', 'hd_genesis_footer_scripts', 15);

    remove_action('wp_head', 'hd_tracking_js', 6);
    remove_action('wp_head', 'custom_analytics');

    remove_action( 'wpcf7_init', 'wpcf7_recaptcha_add_form_tag_recaptcha', 10 );
    remove_action( 'wpcf7_init', 'wpcf7_add_form_tag_quiz', 10 );
    remove_action( 'wpcf7_init', 'wpcf7_add_form_tag_captcha', 10 );

    remove_action( 'wp_body_open', 'google_tag_manager', 10 );
    remove_action('wp_head', 'custom_head_scripts_and_styles');

    remove_action( 'wp_print_styles', 'print_emoji_styles', 10 );
    //remove_action( 'genesis_footer', 'custom_footer', 8 );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

    add_filter( 'script_loader_tag', function ( $tag, $handle ) {

        if(
            stripos($tag, 'recaptcha') !== false
            || stripos($tag, 'apex') !== false
            || stripos($tag, 'facebook') !== false
            || stripos($tag, 'analytics') !== false
            || stripos($tag, 'tracking') !== false
            || stripos($tag, 'lazysizes') !== false
        )
            return "";

        return $tag;
    }, 10, 2 );
}
