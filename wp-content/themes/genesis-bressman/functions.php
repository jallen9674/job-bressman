<?php

global $hc_settings;

$hc_settings = [
    'location_taxonomy' => 'hc_city_location',
    'location_widget_title' => '_hc_location_widget_title',
    'faqs_category_taxonomy' => 'bressman_faq',
    'state' => 'Ohio',
    'stateabbr' => 'OH',
    'phone_number' => '877-707-1385',
    'primary_menu' => "Main",
    'practice_areas_menu_item' => "menu-item-28",
];


// generate widget title field
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array (
        'key' => 'group_5dcda98072a23',
        'title' => 'City Widget Title',
        'fields' => array (
            array (
                'key' => 'field_5dcda98c6d1dd',
                'label' => 'Location Widget Title',
                'name' => $hc_settings['location_widget_title'],
                'type' => 'text',
                'value' => NULL,
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
endif;





if(is_admin()):
// setup plugins
require_once 'plugin-activator/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 *  <snip />
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function my_theme_register_required_plugins() {

    $plugins = array(

        array(
            'name'      => 'Custom Permalinks',
            'slug'      => 'custom-permalinks',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Yoast SEO',
            'slug'      => 'wordpress-seo',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Redirection',
            'slug'      => 'redirection',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Custom Fields',
            'slug'      => 'advanced-custom-fields',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Simply Show Hooks',
            'slug'      => 'simply-show-hooks',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'reSmush.it',
            'slug'      => 'resmushit-image-optimizer',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Autoptimize',
            'slug'      => 'autoptimize',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Classic Editor',
            'slug'      => 'classic-editor',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Accessibility by UserWay',
            'slug'      => 'userway-accessibility-widget',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'CallTrackingMetrics',
            'slug'      => 'call-tracking-metrics',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'WP External Links',
            'slug'      => 'wp-external-links',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

    );

    $config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'themes.php',            // Parent menu slug.
        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => true,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        /*
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'theme-slug' ),
            'menu_title'                      => __( 'Install Plugins', 'theme-slug' ),
            // <snip>...</snip>
            'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
        */
    );

    tgmpa( $plugins, $config );

}
endif;



/*
 *
 * AUTO INCLUDE - BE CAREFUL!
 *
 * */

$autoiclude_folders = [
    '/lib/snippets/',
    '/lib/taxonomy/',
    '/lib/shortcodes/',
    '/lib/widgets/',
];

foreach($autoiclude_folders as $folder) {
    foreach (scandir(dirname(__FILE__) . $folder) as $filename) {
        $path = dirname(__FILE__) . $folder . $filename;
        if (is_file($path)) {
            require_once $path;
        }
    }
}


// include layout
require_once 'lib/layout/layout.php';


// include frontend assets
require_once 'assets/assets.php';


add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode');

/*-------------------------------------------------------------------------
*
* START OF HENNESSEY FUNCTION AND INCLUDES
*
-------------------------------------------------------------------------*/
    // Post Types
    //include('lib/post-type/custom-post-type.php');
    include('lib/post-type/reviews.php');

    // Shortcodes
    include('lib/shortcodes/old/location.php');
    include('lib/shortcodes/old/locations.php');
    include('lib/shortcodes/old/reviews-homepage.php');
    include('lib/shortcodes/old/reviews.php');
    include('lib/shortcodes/old/sidebar-chat-live-button.php');
    include('lib/shortcodes/old/sidebar-location-listing.php');
    include('lib/modules/rs-related-pages-module.php');

    // Custom actions
    remove_action( 'genesis_before_content_sidebar_wrap', 'custom_do_breadcrumbs', 5 );
    add_action( 'genesis_before_loop', 'yoast_breadcrumbs', 5 );
    function yoast_breadcrumbs(){
        if ( function_exists('yoast_breadcrumb') && !is_front_page() ) {
            yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        }
    }

    add_action( 'genesis_before_loop', 'custom_page_title', 12 );
    function custom_page_title(){
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        if ($paged > 1){
            $pagedNum = ' - page ' . $paged;
        }
        if (is_home()){
            echo '<h1 class="entry-title">Blog'.$pagedNum.'</h1>';
        }
        if (!is_single() && !is_page() && !is_home()) {
            $page = '';

            if(is_paged()) {
                $paged = get_query_var('paged') ?: 0;

                if($paged) {
                    $page = ' - Page ' . $paged;
                }
            }

            $heading = "Blog";

            if(is_archive()) {
                $heading = get_the_archive_title();
            }

            if(is_search()) {
                $heading = "You searched for: " . get_search_query();
            }

            if($page && $heading) {
                $heading = $heading . $page;
            }
            echo "<h1 class='entry-title'>" . $heading . "</h1>";
        }
    }

    remove_action( 'genesis_after_header', 'custom_inner_page_after_header', 10 );
    add_action( 'genesis_header', 'custom_inner_page_header', 10 );
    function custom_inner_page_header(){
        if (!is_front_page()){
            include('parts/header-inner.php');
        }
    }

    // remove_action( 'genesis_entry_content', 'custom_do_thumbnail', 8 );
    // add_action( 'genesis_entry_content', 'custom_post_thumbnail', 8 );
    // function custom_post_thumbnail(){
    //     if (is_home()){
    //         if (has_post_thumbnail()) {
    //             the_post_thumbnail();
    //         } else {
    //             echo '<img src="'.CHILD_URL.'/assets/app/images/nothumb.gif" class="alignleft blog_post_thumbnail post_thumbnail post_thumbnail_'.get_the_ID().'" alt="'.get_the_title().'">';
    //         }
    //     }
    // }
/*-------------------------------------------------------------------------
*
* END OF HENNESSEY FUNCTION AND INCLUDES
*
-------------------------------------------------------------------------*/
