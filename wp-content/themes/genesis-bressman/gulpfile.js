'use strict';

// Variables

const gulp = require('gulp');
const watch = require('gulp-watch');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const imagemin = require('gulp-imagemin');
const concat = require('gulp-concat');
const cleanCss = require('gulp-clean-css');
const sftp = require('gulp-sftp-up4');



// TEMPLATE CONFIG HERE
const theme_dir = 'hc_genesis_boilerplate';

const sftp_host = "";
const sftp_user = "";
const sftp_pass = "";
// END


// Remote Path

let pathCss = 'wp-content/themes/'+theme_dir+'/assets/app/css/';
let pathJs = 'wp-content/themes/'+theme_dir+'/assets/app/js/';
let pathImg = 'wp-content/themes/'+theme_dir+'/assets/app/img/';

// JavaScript

gulp.task('javascript', function () {
    return gulp.src([
        './assets/dev/js/main.js'])
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest('./assets/app/js'))
        .pipe(sftp({
            host: sftp_host,
            user: sftp_user,
            pass: sftp_pass,
            port: 2222,
            remotePath: pathJs
        }));
});

// Return javascript

gulp.task('return:javascript', function() {
   return gulp.src([
       './assets/app/js/main.js',
        ])
       .pipe(gulp.dest('./assets/dev/js/'))
});


// Css to sass

gulp.task('css-to-sass', function () {
    return gulp.src('./assets/app/css/main.css')
        .pipe(rename({
            extname: '.scss'
        }))
        .pipe(gulp.dest('./assets/dev/scss'));
});

// Images

gulp.task('images', function() {
   return gulp.src([
       './assets/dev/img/*'
        ])
       .pipe(imagemin([
           imagemin.gifsicle({interlaced: true}),
           imagemin.jpegtran({progressive: true}),
           imagemin.optipng({optimizationLevel: 5}),
           imagemin.svgo({
               plugins: [
                   {removeViewBox: true},
                   {cleanupIDs: false}
               ],
           })
       ]))
       .pipe(gulp.dest('./assets/app/img'))
       .pipe(sftp({
           host: sftp_host,
           user: sftp_user,
           pass: sftp_pass,
           port: 2222,
           remotePath: pathImg
       }));
});

// Sass to css

gulp.task('sass-to-css', function () {
    return gulp.src('./assets/dev/scss/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(plumber())
        .pipe(cleanCss({
            format: 'beautify'
        }))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./assets/app/css'))
        .pipe(sftp({
            host: sftp_host,
            user: sftp_user,
            pass: sftp_pass,
            port: 2222,
            remotePath: pathCss
        }));

});

// Watch

gulp.task('watch', function() {
    gulp.watch('./assets/dev/js/main.js', gulp.series('javascript'));
    gulp.watch('./assets/dev/scss/main.scss', gulp.series('sass-to-css'));
    gulp.watch('./assets/dev/img/*', gulp.series('images'));
});

// Gulp default

gulp.task('default', gulp.parallel('watch', 'javascript', 'sass-to-css', 'images'));

// Gulp build

gulp.task('build', gulp.parallel('javascript', 'sass-to-css', 'images'));

// Gulp return files on website

gulp.task('return-compiled', gulp.parallel('return:javascript', 'css-to-sass'));