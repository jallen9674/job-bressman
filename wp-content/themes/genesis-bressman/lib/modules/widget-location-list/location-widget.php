<?php

// Creating the widget
class hc_location_widget extends WP_Widget {

    function __construct() {
    parent::__construct(

    // Base ID
    'hc_location_widget',

    // Widget
    'HC Location Widget',

    // Widget description
    array( 'description' => 'A widget that will display related practice area pages.')
    );
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );


    /* Get Current Post ID (For Grabbing Which Location to Display) */
    global $post;
    $locationCurrentPost = $post->ID;

    /*
    Get Current Location Taxonomy On Page
    */
    $postTerms =  wp_get_object_terms($post->ID, 'hc_location');
    $categoryFilterSlug = '';
    $categoryPrettyName = '';
    if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
         foreach ( $postTerms as $term ) {
           $categoryFilterSlug .= '' . $term->slug;
           $categoryPrettyName .= ' ' . $term->name;
         }
     }

   //Widget Toggle
    if($postTerms) :


    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )

    //echo $args['before_title'] . $title . $args['after_title'];

    //BEGIN FRONTEND OUTPUT
    ?>


    <div class="location-widget-outer">
        <div class="location-widget-inner">
            <span class="location-widget-title"><?php echo $categoryPrettyName; ?>, OH<br> Practice Areas</span>
            <ul class="location-widget-links location-listing">

            <?php //BEGIN LOOP FOR MATCHING PAGES ?>
            <?php




               $args = array(
                'posts_per_page' => -1,
                'post_type' => 'page',
                'tax_query' => array(
                      array(
                          'taxonomy' => 'hc_location',
                          'field' => 'slug',
                          'terms' => $categoryFilterSlug
                      ),
                  ),
                 'meta_key'   => '_hc_location_widget_title',
                  'orderby'    => 'meta_value',
                  'order'      => 'ASC',
                'post__not_in' => array($locationCurrentPost)
              );


            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
            ?>
            <?php //BEGIN OUTPUT ?>
                <?php //Get Pretty Title
                    //Resetting Link Title For Errors
                    $linkTitle = 'Not Set';

                    //Updating Title For Current Link
                    if ( get_post_meta( $post->ID, '_hc_location_widget_title', true ) ){
                         $linkTitle = get_post_meta( $post->ID, '_hc_location_widget_title', true );
                      }
                ?>
                <li class="single-location-link">
                    <a href="<?php the_permalink(); ?>"><?php echo $linkTitle; ?></a>
                </li>
            <?php //END OUTPUT ?>
            <?php endwhile; else : ?>
            <!-- IF NOTHING FOUND CONTENT HERE -->
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <?php //END LOOP FOR MATCHING PAGES ?>

            </ul>
        </div>
    </div>
    </div>

    <?php
    //END FRONTEND OUTPUT

    echo $args['after_widget'];

     endif;
}

// Widget Backend
public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
        $title = $instance[ 'title' ];
    }
    else {
        $title = __( 'New title', 'wpb_widget_domain' );
    }

    // Widget admin form
    ?>
    <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    <?php
}

// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
    }
} // Class wpb_widget ends here

// Register and load the widget
function hc_load_location_widget() {
    register_widget( 'hc_location_widget' );
}

add_action( 'widgets_init', 'hc_load_location_widget' );

?>