<?php


add_filter( 'get_the_archive_title', function ($title) {
    if ( is_category() ) {
        $title = "Blog: " . single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = "Tag: " . single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>' ;
    } elseif ( is_tax() ) {
        $title = sprintf( __( '%1$s' ), single_term_title( '', false ) );
    }
    return $title;
});

//internal hero
add_action('genesis_after_header', 'custom_inner_page_after_header', 10);
function custom_inner_page_after_header() {

    if(is_front_page()) return "";

    if(is_single() || is_page()) {
        remove_action('genesis_entry_header', 'genesis_do_post_format_image', 4);
        remove_action('genesis_entry_header', 'genesis_entry_header_markup_open', 5);
        remove_action('genesis_entry_header', 'genesis_do_post_title', 10);
        remove_action('genesis_entry_header', 'genesis_do_post_info', 12);
        remove_action('genesis_entry_header', 'genesis_entry_header_markup_close', 15);

        $page = '';

        if(is_paged()) {
            $paged = get_query_var('paged') ?: 0;

            if($paged) {
                $page = ' - Page ' . $paged;
            }
        }

        ?>

        <div class="internal-hero-image" style="background: #f1f1f1; text-align: left; padding: 20px 0;">
            <div class="site-inner">
                <?php

                ob_start();

                genesis_entry_header_markup_open();
                genesis_do_post_title();
                genesis_entry_header_markup_close();

                $heading = ob_get_contents();
                ob_clean();

                echo $heading . $page;

                ?>
            </div>
        </div>
        <?php

    } else {

        remove_action('genesis_before_loop', 'genesis_do_cpt_archive_title_description', 10);
        remove_action('genesis_before_loop', 'genesis_do_date_archive_title', 10);
        remove_action('genesis_before_loop', 'genesis_do_blog_template_heading', 10);
        remove_action('genesis_before_loop', 'genesis_do_posts_page_heading', 10);
        remove_action('genesis_before_loop', 'genesis_do_search_title', 10);

        ?>

        <div class="internal-hero-image" style="background: #f1f1f1; text-align: left; padding: 20px 0;">
            <div class="site-inner">

                <?php $page = '';

                if(is_paged()) {
                    $paged = get_query_var('paged') ?: 0;

                    if($paged) {
                        $page = ' - Page ' . $paged;
                    }
                }

                $heading = "Blog";

                if(is_archive()) {
                    $heading = get_the_archive_title();
                }

                if(is_search()) {
                    $heading = "You searched for: " . get_search_query();
                }

                if($page && $heading) {
                    $heading = $heading . $page;
                }
                echo "<h1>" . $heading . "</h1>"; ?>

            </div>
        </div>

        <?php

    }
}