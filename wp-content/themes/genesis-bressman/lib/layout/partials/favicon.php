<?php


/*
 * generate favicon here: https://realfavicongenerator.net/
 * place files in /assets/favicon/ folder
 *
 * */

function custom_add_favicon() {
    /** Remove default favicon */
    remove_action('wp_head', 'genesis_load_favicon', 10); ?>


    <link rel="apple-touch-icon" sizes="180x180" href="<?=CHILD_URL?>/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=CHILD_URL?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="194x194" href="<?=CHILD_URL?>/assets/favicon/favicon-194x194.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?=CHILD_URL?>/assets/favicon/android-chrome-192x192.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=CHILD_URL?>/assets/favicon/favicon-16x16.png">
    <link rel="mask-icon" href="<?=CHILD_URL?>/assets/favicon/safari-pinned-tab.svg" color="#222222">
    <link rel="shortcut icon" href="<?=CHILD_URL?>/assets/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="<?=CHILD_URL?>/assets/favicon/mstile-144x144.png">
    <meta name="msapplication-config" content="<?=CHILD_URL?>/assets/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <?php
}

add_action('wp_head', 'custom_add_favicon', 9);
