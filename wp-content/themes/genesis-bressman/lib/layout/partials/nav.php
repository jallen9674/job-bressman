<?php

function custom_remove_nav() {
    remove_action('genesis_after_header', 'genesis_do_nav', 10);
    remove_action('genesis_after_header', 'genesis_do_subnav', 10);

    genesis_do_nav();
}

add_action('genesis_header', 'custom_remove_nav', 12);


// DYNAMIC NAV MENU

function getElementsByClass(&$parentNode, $tagName, $className) {
    $nodes=array();

    $childNodeList = $parentNode->getElementsByTagName($tagName);
    for ($i = 0; $i < $childNodeList->length; $i++) {
        $temp = $childNodeList->item($i);
        if (stripos($temp->getAttribute('class'), $className) !== false) {
            $nodes[]=$temp;
        }
    }

    return $nodes;
}
// local menu items in header
add_filter( 'wp_nav_menu', 'filter_function_name_1676', 10, 2 );
function filter_function_name_1676( $nav_menu, $args ){
    global $hc_settings;

    if(is_object($args->menu) && $args->menu->name === $hc_settings['primary_menu']) {
        global $post;

        $terms = get_the_terms( $post->ID, $hc_settings['location_taxonomy']);

        if(!$terms || !is_singular()) {
            return $nav_menu;
        }


        if($terms) {
            $city = current($terms)->name;

            $piurl = strtolower(str_replace(' ', '-', $city).'-personal-injury-lawyer/');

            $url_exist = get_posts([
                'post_type' => 'page',
                'meta_key' => 'custom_permalink',
                'meta_value' => str_replace(' ', '-', $city).'-personal-injury-lawyer/'
            ]);

            if(url_to_postid(home_url('/') . $piurl) || $url_exist) {
                $html = "<a href='/".str_replace(' ', '-', strtolower($city))."-personal-injury-lawyer/'>$city Practice Areas</a><ul class='sub-menu dynamic-sub-menu'>";

            } else {

                $html = "<a href='#'>$city Practice Areas</a><ul class='sub-menu dynamic-sub-menu'>";

            }

            $children = get_posts([
                'posts_per_page' => -1,
                'post_type' => 'page',
                'post_status' => 'publish',
                'tax_query' => [
                    [
                        'taxonomy' => $hc_settings['location_taxonomy'],
                        'terms' => [current($terms)->term_id]
                    ]
                ]
            ]);


            $main_prc = [
                "Bicycle Accidents",
                "Birth Injury",
                "Birth Injuries",
                "Boating Accidents",
                "Car Accidents",
                "Construction Accidents",
                "Dog Bites",
                "Medical Malpractice",
                "Motorcycle Accidents",
                "Pedestrian Accidents",
//                "Personal Injury",
                "Slip and Fall Accidents",
                "Truck Accidents",
                "Workers Compensation",
                "Workers' Compensation",
                "Wrongful Death",
            ];

            $children = array_filter($children, function($c)use($main_prc,$hc_settings){
                return in_array(get_post_meta($c->ID, $hc_settings['location_widget_title'], true), $main_prc);
            });

            usort($children, function($a, $b){
                return $a->post_title > $b->post_title ? 1 : -1;
            });

            foreach($children as $child) {
                $html .= '<li><a href="'.get_permalink($child->ID).'">'.get_post_meta($child->ID, $hc_settings['location_widget_title'], true).'</a></li>';
            }


            $html .= "</ul>";

            $doc = new DOMDocument();

            $doc->loadHTML(mb_convert_encoding($nav_menu, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);


            $content_node=$doc;

            $element = getElementsByClass($content_node, 'li', $hc_settings['practice_areas_menu_item']);
            if($element) {
                $element = current($element);
            }
            if(isset($element)){

                while($element->childNodes->length){
                    $element->removeChild($element->firstChild);
                }

                $fragment = $doc->createDocumentFragment();
                $fragment->appendXML($html);
                $element->appendChild($fragment);
            }

            $html = $doc->saveHTML();

            if($html) $nav_menu = $html;

        }
    }

    return $nav_menu;
}

// END DYNAMIC NAV MENU



// practice areas - double columns
add_action('wp_head', function(){
    global $hc_settings; ?>

    <style>

        @media screen and (min-width: 991px) {
            <?=".".$hc_settings['practice_areas_menu_item']?> .sub-menu,
            .dynamic-sub-menu{
                width: 400px;
            }

            <?=".".$hc_settings['practice_areas_menu_item']?> .sub-menu li,
            .dynamic-sub-menu li {
                width: 49%;
                display: inline-block;
            }

            <?=".".$hc_settings['practice_areas_menu_item']?> .sub-menu li a,
            .dynamic-sub-menu li a{
                width: 100%;
                display: block;
            }
        }

    </style>

    <?php
});