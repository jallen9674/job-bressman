<?php

// breadcrumbs
if ( function_exists('yoast_breadcrumb') ) {
    function custom_do_breadcrumbs() {
        remove_action('genesis_before_loop', 'genesis_do_breadcrumbs', 10);

        yoast_breadcrumb( '<p class="breadcrumb" id="breadcrumbs">','</p>' );
    }
    add_action('genesis_before_content_sidebar_wrap', 'custom_do_breadcrumbs', 5);
}

//add proper breadcrumbs for location pages
function filter_wpseo_breadcrumb_links($crumbs) {
    global $post, $hc_settings;

    $terms = get_the_terms($post->ID, $hc_settings['faqs_category_taxonomy']);

    if(is_singular('page') && $terms) {

    	$term = current($terms);

        $add_crumbs = [
            [
                'text' => $term->name,
                'url' => get_term_link($term, $hc_settings['faqs_category_taxonomy'])
            ]
        ];


        array_splice($crumbs, -1, 0, $add_crumbs);
    }

    if(is_tax($hc_settings['faqs_category_taxonomy'])) {

        $add_crumbs = [
            [
                'text' => 'Frequently Asked Questions',
                'url' => '/faqs/'
            ]
        ];


        array_splice($crumbs, -1, 0, $add_crumbs);
    }

    return $crumbs;
}

add_filter( 'wpseo_breadcrumb_links', 'filter_wpseo_breadcrumb_links', 10, 1 );

// Before sidebar wrap
add_action('genesis_archive_title_descriptions', function(){

    global $post;
    global $hc_settings;

    if(is_archive()) {

        $post_cats = wp_get_post_categories($post->ID);
		
		if(!$post_cats) {
			$post_cats = get_the_terms($post->ID, $hc_settings['faqs_category_taxonomy']);
		}

        if($post_cats) {
            $category = get_category(current($post_cats));
			
			if(!$category) {
				$category = get_term(current($post_cats));
			}

            $p = get_posts([
                'post_type' => 'page',
                'meta_key' => $hc_settings['location_widget_title'],
				'meta_value' => $category->name,	
				$hc_settings['location_taxonomy'] => 'columbus'
            ]);
		
            if($p) {
                $p = current($p);
            } else {
                return '';
            }

            ?>
			<a href="<?=get_permalink($p->ID)?>" title="<?=get_the_title($p->ID)?>"><?=get_the_title($p->ID)?></a>
            <?php
        }

    }

}, 20);


if(!empty($_GET['tstng'])) {
	$psts = get_posts([
		'post_type'=>'page',
		'posts_per_page'=>-1
	]);
	
	foreach($psts as $p) {
		echo get_permalink($p->ID), " -=- ", get_field($hc_settings['location_widget_title'], $p->ID)."<br>";
	}
	die;
}