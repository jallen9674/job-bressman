<?php

function custom_footer(){

    remove_action('genesis_footer', 'genesis_do_footer', 10); ?>

    <div class="footer page-footer" id="page-footer">

    			<div id="inner-footer" class="wrapper page-footer__inner">

    				<h2 class="page-footer__section-title">
    					<span class="page-footer__section-title-inner">
    						Contact Us For a Free Case Evaluation
    					</span>
    				</h2>

    				<div class="row">
    					<div class="">
    							<div class="one-third">
    								<div class="page-footer__contact-form-wrapper standard-form theme_1">
                                        <?php echo do_shortcode( '[contact-form-7 id="2412" title="Homepage Contact Form"]' ); ?>
                                    </div>
    					</div>
    					<div class="one-third">

    						<div class="page-footer__address-box">
    							<span class="page-footer__address-name">
                                    <a href="https://www.bressmanlaw.com/dublin-injury/" title="Dublin Personal Injury Lawyer" style="color:rgb(228, 106, 16);" data-wpel-link="internal">
                                        Dublin Location
                                    </a>
                                </span>
    							<address class="page-footer_address-location">
    								5186 Blazer Pkwy<br>
    								Dublin, OH 43017<br>
    								Phone: <a href="tel:+6145381116" data-wpel-link="internal" data-ctm-watch-id="7" data-ctm-tracked="1" data-observe="1" data-observer-id="3">614-538-1116</a>
    							</address>
    							<div class="iframe-wrapper page-footer__map-embed iframe-wrapper-16x9">
    								<!-- <iframe data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3052.3530689384297!2d-83.13033858385934!3d40.08984038337776!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88389323f6b0c569%3A0xd62231ee529bd85a!2s5186+Blazer+Pkwy%2C+Dublin%2C+OH+43017!5e0!3m2!1sen!2sus!4v1508171676685" width="600" height="450" <?php //echo frameborder="0"; ?> style="border:0" allowfullscreen="" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3052.3530689384297!2d-83.13033858385934!3d40.08984038337776!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88389323f6b0c569%3A0xd62231ee529bd85a!2s5186+Blazer+Pkwy%2C+Dublin%2C+OH+43017!5e0!3m2!1sen!2sus!4v1508171676685"></iframe> -->
                                    <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12209.41790522732!2d-83.12808100000001!3d40.089809!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xdbdb10eec4b6175a!2sBressman%20Law!5e0!3m2!1sen!2sua!4v1578085111580!5m2!1sen!2sua" width="600" height="450" style="border:0;" allowfullscreen=""></iframe> -->
                                    <a href="https://www.google.com/maps?ll=40.089809,-83.128081&z=13&t=m&hl=en&gl=EN&mapclient=embed&cid=15842274731894380378" target="_blank">
                                        <img src="/wp-content/themes/genesis-bressman/assets/app/images/map1.jpg" alt="Dublin Location">
                                    </a>
                                </div>
    						</div>

    					</div>
							<div class="one-third">
    							<div class="page-footer__address-box">
    								<span class="page-footer__address-name">
                                        <a href="https://www.bressmanlaw.com/cincinnati-injury/" title="Personal Injury Lawyers in Cincinnati" style="color:rgb(228, 106, 16);" data-wpel-link="internal">
                                            Cincinnati Location
                                        </a>
                                    </span>
    								<address class="page-footer_address-location">
    									9435 Waterstone Blvd #130<br>
    									Cincinnati, OH 45249<br>
    									Phone: <a href="tel:+5134341818" data-wpel-link="internal" data-ctm-watch-id="9" data-ctm-tracked="1" data-observe="1" data-observer-id="4">513-434-1818</a>
    								</address>
    								<div class="iframe-wrapper page-footer__map-embed iframe-wrapper-16x9">
    									<!-- <iframe data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3087.492945822731!2d-84.30469358387072!3d39.29974833073046!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88405651eeb35c8f%3A0x5acf0a092a7ceb54!2s9435+Waterstone+Blvd+%23130%2C+Cincinnati%2C+OH+45249!5e0!3m2!1sen!2sus!4v1508171713981" width="600" height="450" <?php //echo frameborder="0"; ?> style="border:0" allowfullscreen="" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3087.492945822731!2d-84.30469358387072!3d39.29974833073046!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88405651eeb35c8f%3A0x5acf0a092a7ceb54!2s9435+Waterstone+Blvd+%23130%2C+Cincinnati%2C+OH+45249!5e0!3m2!1sen!2sus!4v1508171713981"></iframe> -->
                                        <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12349.96460832814!2d-84.30267!3d39.299789!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x47d07dedd6013078!2sBressman%20Law!5e0!3m2!1sen!2sua!4v1578085148917!5m2!1sen!2sua" width="600" height="450" style="border:0;" allowfullscreen=""></iframe> -->
                                        <a href="https://www.google.com/maps?ll=39.299789,-84.30267&z=13&t=m&hl=en&gl=EN&mapclient=embed&cid=5174774432299823224" target="_blank">
                                            <img src="/wp-content/themes/genesis-bressman/assets/app/images/map2.jpg" alt="Cincinnati Location">
                                        </a>
                                    </div>
    						</div>
    					</div>
    			</div>


    		</div>

    	</div>

    	<div class="page-footer__disclaimer-section">
    		<div class="wrapper">
    			<img src="<?php echo CHILD_URL; ?>/assets/app/images/bressman-law-logo.png" alt="David Bressman Law" class="page-footer__footer-logo">

    								<div class="page-footer__social-icons">
    				<span class="page-footer__social-intro-text">Follow Us On:</span>
    					<a target="_blank" href="https://www.facebook.com/bressmanlawfirm/" class="page-footer__social-icon" data-wpel-link="external" rel="external noopener noreferrer"><i class="fa fa-facebook"></i></a>
    					<a target="_blank" href="https://twitter.com/dabressman" class="page-footer__social-icon" data-wpel-link="external" rel="external noopener noreferrer"><i class="fa fa-twitter"></i></a>
    					<a target="_blank" href="https://www.linkedin.com/pub/david-bressman/8/725/2a7" class="page-footer__social-icon" data-wpel-link="external" rel="external noopener noreferrer"><i class="fa fa-linkedin"></i></a>
    					<a target="_blank" href="https://www.youtube.com/user/DavidBressman/videos" class="mobile-header__social-icon" data-wpel-link="external" rel="external noopener noreferrer"><i class="fa fa-play"></i></a>
    			</div>

    			<p class="page-footer__disclaimer-text">
    				<a rel="nofollow external noopener noreferrer" href="https://m.me/143111162614/" target="_blank" data-wpel-link="external">Message Us on Facebook</a> |
                    <a href="<?php echo home_url(); ?>/disclaimer/" data-wpel-link="internal">Disclaimer</a> |
                    <a href="<?php echo home_url(); ?>/site-map/" data-wpel-link="internal">Site Map</a>
    			</p>
    		</div>
    	</div>

    </div>

<?php }

add_action('genesis_footer', 'custom_footer', 8);

function custom_copyright(){

}
add_action('genesis_footer', 'custom_copyright', 20);
