<?php
    function wptp_create_post_type() {
        $labels = array(
            'name' => __( 'Testimonials' ),
            'singular_name' => __( 'Testimonials' ),
            'add_new' => __( 'New Testimonial' ),
            'add_new_item' => __( 'New Testimonial' ),
            'edit_item' => __( 'Edit Testimonial' ),
            'new_item' => __( 'New Testimonial' ),
            'view_item' => __( 'View Testimonial' ),
            'search_items' => __( 'Search Testimonial' ),
            'not_found' =>  __( 'Nothing Found...' ),
            'not_found_in_trash' => __( 'Trash is Empty' ),
            );
        $args = array(
            'labels' => $labels,
            'has_archive' => false,
            'public' => true,
            'map_meta_cap' => true,
            'supports' => array(
              'title',
              'editor'
              ),
            'taxonomies' => array(),
            );
        register_post_type( 'testimonials', $args );
    }
    add_action( 'init', 'wptp_create_post_type' );
