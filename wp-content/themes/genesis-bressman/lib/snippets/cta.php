<?php
// adding CTA's
add_filter('the_content', function($content){
    global $post;
    global $hc_settings;


    $terms = get_the_terms( $post->ID, $hc_settings['location_taxonomy'] );

    $phoneNumber = $hc_settings['phone_number'];

    $cta = '';

    if(!$terms && !is_singular('post') && !has_term('', $hc_settings['faqs_category_taxonomy'])) {
        return $content;
    } else if($terms) {

        $terms = current($terms);

        $page_h1 = str_ireplace('lawyer', '', $terms->name . " " . get_the_title($post->ID));

        $cta = '<p class="rs-content-cta">'. $page_h1 .' Lawyer Near Me <a href="tel:'.$phoneNumber.'">'.$phoneNumber.'</a></p>';

        if(!$post->post_parent) {
            $page_h1 = get_the_title($post->ID);

            $cta = '<p class="rs-content-cta">'. $page_h1 . ' Near Me <a href="tel:'.$phoneNumber.'">'.$phoneNumber.'</a></p>';
        }

        if($short = get_post_meta($post->ID, $hc_settings['location_widget_title'], true)) {
            $cta = '<p class="rs-content-cta">'. $page_h1 . ' Near Me <a href="tel:'.$phoneNumber.'">'.$phoneNumber.'</a></p>';
            $cta = '<p class="rs-content-cta">'.$terms->name . ' ' . $short.' Lawyer Near Me <a href="tel:'.$phoneNumber.'">'.$phoneNumber.'</a></p>';
        }

    }

    $cta_third_add = '<a href="/">personal injury lawyers</a>';
    $cta_first_add = ',';

    if($terms && $post->post_parent !== 0) {
        $p_tmp = $post;
        $p_tmp_lvl = $post;
        $levels = 1;
        while($p_tmp->post_parent !== 0) {
            $p_tmp_lvl = $p_tmp;
            $p_tmp = get_post($p_tmp->post_parent);
            $levels++;
        }

        if (stripos($post->post_title, 'personal injury') !== false) {
            $link = get_permalink($post->ID);
            $title = str_replace('Lawyer', 'Lawyers', get_the_title($post->ID));
        } else {
            $link = get_permalink($post->post_parent);
            $title = str_replace('Lawyer', 'Lawyers', get_the_title($post->post_parent));
        }

        if($title == 'Locations') {
            $link = '/';
            $title = $hc_settings['state'].' Personal Injury Lawyers';
        }

        $cta_third_add = '<a href="'.$link.'" title="'.$title.'">'.$title.'</a>';
        $cta_first_add = " with a ".strtolower(get_post_meta($post->ID, $hc_settings['location_widget_title'], true))." lawyer serving {$terms->name},";
    }
    else if($terms && $post->post_parent == 0) {
        $cta_first_add = " with a personal injury lawyer serving {$terms->name},";
    }



    $cta_first = '<p class="rs-content-cta rs-content-cta-small">For a free legal consultation'.$cta_first_add.' call <a href="tel:'.$phoneNumber.'">'.$phoneNumber.' </a></p>';

    $cta_third = '<p class="rs-content-cta rs-content-cta-small"><a href="/contact/">Click to contact</a> our '.$cta_third_add.' today</p>';

    $cta_fourth = '<p class="rs-content-cta rs-content-cta-small">Complete a <a href="/contact/">Free Case Evaluation form</a> now</p>';

    preg_match_all('/<h2>/', $content, $h2_matches, PREG_OFFSET_CAPTURE);
    if(count($h2_matches[0]) <= 2) {
        preg_match_all('/<h(2|3|4)>/', $content, $h2_matches, PREG_OFFSET_CAPTURE);
    }
     if(count($h2_matches[0]) <= 1) {
       preg_match_all('/<p>.{300,}<\/p>/', $content, $h2_matches, PREG_OFFSET_CAPTURE);

         if(count($h2_matches[0]) > 2) {
             array_shift($h2_matches[0]);
         }

     }

     if($terms && !get_post_meta($post->ID, $hc_settings['location_widget_title'], true)) {
         $cta = '<p class="rs-content-cta">Personal Injury Lawyer Near Me <a href="tel:'.$phoneNumber.'">'.$phoneNumber.'</a></p>';
     }

    if(count($h2_matches[0]) > 1) {
        foreach(array_reverse($h2_matches[0]) as $k => $h2_match) {

            $k = count($h2_matches[0]) - $k - 1;

            if($h2_match[1] && $k == 1) {
                $content = substr_replace($content, $cta_first, $h2_match[1], 0);
                // break;
            }

            if($h2_match[1] && $k == 2 && $cta) {
                $content = substr_replace($content, $cta, $h2_match[1], 0);
                // break;
            }

            if($h2_match[1] && $k == 3) {
                $content = substr_replace($content, $cta_third, $h2_match[1], 0);
                // break;
            }

            if($h2_match[1] && $k == 4) {
                $content = substr_replace($content, $cta_fourth, $h2_match[1], 0);
                // break;
            }
        }
    }

    // last CTA
    $last_cta = '<p class="rs-content-cta rs-content-cta-small">Call or text <a href="tel:'.$phoneNumber.'">'.$phoneNumber.'</a> or complete a <a href="/contact/">Free Case Evaluation form</a></p>';

    $content .= $last_cta;

    return $content;
});