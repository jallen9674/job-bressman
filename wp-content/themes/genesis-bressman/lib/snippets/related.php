<?php

// add related widgets
add_action('genesis_after_loop', function(){

    global $hc_settings;
    global $in_single_content;

    $in_single_content = false;

    if(!is_singular('post') && !is_singular('page') && !is_single())
        return '';

    if(is_singular('page') && !has_term('', $hc_settings['location_taxonomy']) && !has_term('', $hc_settings['faqs_category_taxonomy']))
        return '';

    //related posts
    global $post;

    $cat = get_the_terms( $post->ID, 'category' ) ?
        get_the_terms( $post->ID, 'category' ) :
        (get_the_terms( $post->ID, $hc_settings['faqs_category_taxonomy'] ) ? get_the_terms( $post->ID, $hc_settings['faqs_category_taxonomy'] ) : false);

    if(get_post_meta($post->ID, $hc_settings['location_widget_title'], true)) {
        $c = new stdClass();
        $c->name = get_post_meta($post->ID, $hc_settings['location_widget_title'], true);
        $cat = [$c];
    }

    if(!$cat && !is_singular('post') && !is_singular('page') && is_single()) {

        $taxes = get_post_taxonomies($post->ID);
        $tax_names = array_filter($taxes, function($t)use($post,$hc_settings){
            return !in_array($t, [
                    'post_tag',
                    'post_format',
                    $hc_settings['location_taxonomy']
                ]) && has_term('', $t, $post->ID);
        });

        foreach ($tax_names as $tax) {
            $cat = get_the_terms($post->ID, $tax);
            if($cat) {
                continue;
            }
        }
    }

    if($cat) {
        $cat = current($cat);
    } else {
        return '';
    }

    $taxes = [
        ['tax' => 'category', 'post_type'=>'post', 'header' => $cat->name.' Blog Posts:'],
       ['tax'=>$hc_settings['faqs_category_taxonomy'], 'post_type' => 'page', 'header' => $cat->name.' FAQ:'],
    ];

    $i = 0;

    foreach($taxes as $key => $header) {

        $args = [
            'post_type' => [
                $header['post_type']
            ],
            'posts_per_page' => -1,
            'post__not_in' => [$post->ID],
            'tax_query' => [
                [
                    'taxonomy' => $header['tax'],
                    'field' => 'name',
                    'terms' => $cat->name
                ]
            ],
            'orderby' => 'rand',
        ];

        $city_term = false;

        if(has_term('', $hc_settings['location_taxonomy'])) {
            $city_terms = get_the_terms($post->ID, $hc_settings['location_taxonomy']);

            if($city_terms) {
                $city_term = current($city_terms);
                $header['header'] = $city_term->name . ' ' . $header['header'];
            }
        }

        $posts = get_posts($args);

        if(!$posts && has_term('', $hc_settings['location_taxonomy']) && get_post_meta($post->post_parent, $hc_settings['location_widget_title'], true)){
            $tit = get_post_meta($post->post_parent, $hc_settings['location_widget_title'], true);

            if($tit) {
                $args['tax_query'][0]['terms'] = $tit;
                $header['header'] = str_replace($cat->name, $tit, $header['header']);
                $posts = get_posts($args);
            }
        }

        if(!$posts && $city_term) {
            unset($args['tax_query']);
            $posts = get_posts($args);

            $header['header'] = str_replace($cat->name, "", $header['header']);
        }

        if($posts && $city_term) {
            $posts_tmp = $posts;
            $posts = array_filter($posts, function($p)use($city_term){
                return strpos($p->post_content, $city_term->name) !== false;
            });

            if(!$posts) {
                $posts = $posts_tmp;

                $header['header'] = str_replace($city_term->name.' ', '', $header['header']);
            }
        }

        $post_count = 4;
        $posts = array_slice($posts, 0, $post_count);

        if($posts) {

            echo "<div class='related-articles'><h2>{$header['header']}</h2>";
            foreach($posts as $p) { ?>
                <div class="related-video clearfix">
                    <a href="<?=get_permalink($p->ID)?>" title="<?=get_the_title($p->ID)?>" class="img-wrp">
                        <?php

                        if ( has_post_thumbnail($p->ID) ) {
                            echo get_the_post_thumbnail($p->ID, 'medium');
                        }
                        else { ?>
                            <img src="<?php echo CHILD_URL; ?>/assets/app/img/default-thumb.jpg" alt="<?php echo get_the_title($p->ID); ?>" />
                            <?php
                        }

                        ?>
                    </a>
                    <a href="<?=get_permalink($p->ID)?>"><?=get_the_title($p->ID)?></a>
                    <p><?=get_the_excerpt($p->ID); ?></p>
                </div>
                <?php
            }

            echo '</div>';

            wp_reset_query();
        }

    }


    unset($in_single_content);

}, 1);