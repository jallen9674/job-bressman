<?php
    add_shortcode( 'latest_posts', 'latest_posts_shortcode' );
    function latest_posts_shortcode(){
        if (is_home() OR is_singular('post')) {
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 3
            );
            $query = new WP_Query($args);

            echo '<div class="lp_widget">';
                echo '<span class="location-widget-title">Latest Blog Posts</span>';
                echo '<ul class="location-widget-links">';
                    while($query->have_posts()) : $query->the_post();
                        echo '<li class="lp_item single-location-link">';
                            echo '<a href="'.get_the_permalink().'">'.get_the_title().'</a>';
                        echo '</li>';
                    endwhile; wp_reset_query();
                echo '</ul>';
            echo '</div>';
        }
    }
