<?php
function hcFindLawyerShortcode($atts = null) {

    global $hc_settings;

    $posts = get_posts([
        'posts_per_page' => 10,
        'post_type' => 'page',
        'meta_key' => $hc_settings['location_widget_title'],
        'meta_value' => 'Personal Injury',
        'orderby' => 'rand'
    ]);

    ob_start();

    if($posts): ?>
        <ul>
        <?php
        foreach($posts as $p) {

            $city_terms = get_the_terms($p->ID, $hc_settings['location_taxonomy']);
            if($city_terms) {
                $city = current($city_terms)->name;
            } else {
                continue;
            }

            ?>
            <li><a href="<?=get_permalink($p->ID)?>"><?=$city, " Personal Injury Lawyer Near Me" ?></a></li>
            <?php
        } ?>
        </ul>
    <?php
    endif;

    $content = ob_get_contents();
    ob_clean();
    return $content;

}


add_shortcode('find-lawyer', 'hcFindLawyerShortcode');

