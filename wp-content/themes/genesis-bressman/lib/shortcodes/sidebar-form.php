<?php

function sidebarForm($atts = null) {

    global $hc_settings;

    ob_start();
    //BEGIN OUTPUT

    ?>

    <div class="sidebar-contact-form standard-form">

            <div class="sidebar-contact-form__phone-heading">
                Call <a href="tel:<?=$hc_settings['phone_number']?>"><?=$hc_settings['phone_number']?></a>
            </div>

            <div class="sidebar-contact-form__subheading">
                Free Case Evaluation
            </div>

        <?php // add CF7 shortcode and remove those lines: ?>

        <div>

            <form class="form-info" action="#" method="post" name="formInfo[]">
                <label class="form-info__label">
                    <input class="form-info__input text-18" type="text" name="firstName" placeholder="First Name*">
                    <svg class="icon icon-user ">
                        <use xlink:href="<?=CHILD_URL?>/assets/app/img/sprite.svg#user"></use>
                    </svg>
                </label>
                <label class="form-info__label">
                    <input class="form-info__input text-18" type="text" name="lastName" placeholder="Last Name*">
                    <svg class="icon icon-user ">
                        <use xlink:href="<?=CHILD_URL?>/assets/app/img/sprite.svg#user"></use>
                    </svg>
                </label>
                <label class="form-info__label">
                    <input class="form-info__input text-18" type="email" name="email" placeholder="Email*">
                    <svg class="icon icon-envelope ">
                        <use xlink:href="<?=CHILD_URL?>/assets/app/img/sprite.svg#envelope"></use>
                    </svg>
                </label>
                <label class="form-info__label">
                    <input class="form-info__input text-18" type="tel" name="phone" placeholder="Phone*">
                    <svg class="icon icon-phone-modal ">
                        <use xlink:href="<?=CHILD_URL?>/assets/app/img/sprite.svg#phone-modal"></use>
                    </svg>
                </label>
                <div class="form-info__textarea-env">
                    <textarea class="form-info__textarea text-18" name="message" placeholder="How can we help you?"></textarea>
                    <svg class="icon icon-comments ">
                        <use xlink:href="<?=CHILD_URL?>/assets/app/img/sprite.svg#comments"></use>
                    </svg>
                </div>
                <div class="form-info__button-submit">
                    <input class="button-submit text-18" type="submit" value="Send message now">
                </div>
            </form>
            
            
        <?php // end ?>

        <?php //echo do_shortcode( '[contact-form-7 id="" title=""]' ); ?>
    </div>

    <?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}


add_shortcode('sidebar-form', 'sidebarForm');