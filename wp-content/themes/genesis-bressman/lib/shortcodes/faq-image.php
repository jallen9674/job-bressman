<?php
function hcRandomFaqImage($atts = null) {

    global $post;
    global $hc_settings;

    $amount = 3;

    extract(shortcode_atts(array(
        'amount' => '',
    ), $atts));

    $queryAmount = $amount;

    ob_start();
    //BEGIN OUTPUT
    ?>
    <div class="random-faq-loop  faq-filter-portfolio-wrapper">
        <?php $faqTerms = get_terms( $hc_settings['faqs_category_taxonomy'] );
        // convert array of term objects to array of term IDs
        $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

        $args = array(
            'posts_per_page' => $queryAmount,
            'post_type' => 'page',
            'tax_query' => array(
                array(
                    'taxonomy' => $hc_settings['faqs_category_taxonomy'],
                    'field' => 'term_id',
                    'terms' => $faqTermIDs
                ),
            ),
            'order' => 'DSC',
            'orderby' => 'rand',
        );

        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
            $pid = $post->ID; ?>


            <div class="faq-page-listing">

                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="faq-page__link-env">
                    <h4 class="faq-page__title"><?php the_title(); ?></h4>
                    <?php
                    if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                      the_post_thumbnail('medium', ['alt' => get_the_title(), 'title' => get_the_title()]);
                    } else{
                      echo '<img alt="faq thumb" src="' . CHILD_URL . '/assets/app/img/default-thumb.jpg">';
                    }
                    ?>
                </a>
                <div class="faq-meta">
                    <?php echo get_the_term_list( $pid, $hc_settings['faqs_category_taxonomy'] ); ?>
                </div>

            </div>
        <?php endwhile; else : ?>
            <!-- IF NOTHING FOUND CONTENT HERE -->
        <?php endif; ?>
        <?php wp_reset_query(); ?>

    </div>
    <!-- </div>


</div> --> <!-- end .random-faq-loop -->

    <?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}


add_shortcode('random-faq-image', 'hcRandomFaqImage');
