<?php
/*--------------------------------
Example Shortcode Wrapper
[hc-address
 map="#"
 address1="#"
 address2="#"
 name="#"
 phone="#"]
---------------------------------*/

function hennesseyLocalShortcode($atts = [], $content = null) {
    global $post;

    $address1 = "";
    $address2 = "";
    $phone = "";
    $name= "";
    $map = "";
    $additional = "";
    $link = "";
    $anchor_text = "";

    extract( shortcode_atts( array(
        'address1' => '',
        'address2' => '',
        'phone' => '',
        'name' => '',
        'map' => '',
        'additional' => '',
        'link' => '',
        'anchor_text' => '',
    ), $atts ) );

    ob_start();
    //BEGIN OUTPUT
    ?>

    <div class="hc-map">
        <div class="hc-map__inner">
            <div class="hc-map__address">
                <h3 class="hc-map__office-title">Office Information</h3>
                <strong>Address</strong>
                <address>
                    <?php echo $name; ?>
                    <?php echo $address1; ?>
                    <?php echo $address2; ?><br>

                    <?php echo $additional; ?>
                </address>
                <?php if($phone) {
                    ?>
                    <strong class="phone-heading">Phone</strong>
                    <a class="address__link" href="tel:+1-<?php echo $phone; ?>"><?php echo $phone; ?></a>
                    <?php
                } ?>
            </div>
            <div class="hc-map__map-embed">

                <div class="map-container">
                    <iframe src="<?php echo $map; ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

                <div style="text-align: center;margin-top: 10px;">
                    <a href="<?=$link?>" target="_blank" style="color: blue;"><?=$anchor_text?></a>
                </div>

            </div>
        </div>
    </div>

    <?php

/* Generating Location Schema


    $schemaAddress = explode(',', $address2);
    ?>
    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "@id": "<?php echo get_the_permalink(); ?>",
  "name": "<?php echo $name; ?>",
  "description": "The dedicated Fort Lauderdale personal injury attorneys at Chalik &amp; Chalik have decades of experience handling all injury cases.",
  "image": [
    "<?php echo site_url(); ?>/wp-content/themes/chalik_theme/media/logo-type.svg"
  ],
  "url": "<?php echo get_the_permalink(); ?>",
  "telephone": "<?php echo $phone; ?>",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "<?php echo $address1; ?>",
    "addressLocality": "<?php echo $schemaAddress[0]; ?>",
    "addressRegion": "<?php echo $schemaAddress[1]; ?>",
    "postalCode": "<?php echo $schemaAddress[2]; ?>",
    "addressCountry": "US"
  }
}
</script>
*/

?>

    <?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('hc-address', 'hennesseyLocalShortcode');