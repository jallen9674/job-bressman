<?php
/*---------------------------------
Sidebar Click Here to Chat Live Button
[bressman-sidebar-chat-live]
----------------------------------*/
function bressmanSidebarChatLive($atts = null) {
    global $post;
    extract(shortcode_atts(array(
        'count' => '-1'
    ), $atts));
    ob_start();
    //BEGIN OUTPUT
?>

<div class="sidebar-chat-button" onclick="window.open ('http://www.apexchat.net/pages/chat.aspx?companyId=16989&amp;requestedAgentId=25&amp;originalReferrer='+document.referrer+'&amp;referrer='+window.location.href,'','width=440,height=680');">

  <div class="sidebar-chat-button__inner">
    Click Here to Chat Live - 24/7
  </div>

</div>


<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('bressman-sidebar-chat-live', 'bressmanSidebarChatLive');
?>
