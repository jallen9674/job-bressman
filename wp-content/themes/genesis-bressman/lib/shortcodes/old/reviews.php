<?php
/*---------------------------------
Bressman Reviews for Interior Pages
[bressman-reviews]
----------------------------------*/
function bressmanReviews($atts = null) {
    global $post;
    extract(shortcode_atts(array(
        'count' => '-1'
    ), $atts));
    ob_start();
    //BEGIN OUTPUT
?>

<?php //Stick Review Code Here ?>

<?php
  $args = array(
  'posts_per_page' => $count,
  'post_type' => 'bressman_reviews',
  'order' => 'DSC',
  );

  $the_query = new WP_Query( $args );
  if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
?>

<div class="single-review" itemscope itemtype="http://schema.org/Review">
    <?php
           //Display Client Name
            if ( has_post_thumbnail() ) {
              $image_array = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'review-image' );
              $image = $image_array[0];
              echo '<img src="' . $image .'" class="single-review-image" alt="Client Review">';
            } else {
              echo '<img src="'. CHILD_URL . '/assets/app/images/review-unknown.png" class="single-review-image" alt="Client Review">';
            }
        ?>
<div class="review-right">
      <div class="review-name">
          <?php
             //Display Client Name
              if ( get_post_meta( $post->ID, '_bressman_review_name', true ) ) {
                  echo '<h3 itemprop="author" class="review-name-content">'.get_post_meta( $post->ID, '_bressman_review_name', true ) .'</h3>';
              }
          ?>

    <div class="review-rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
        <meta itemprop="worstRating" content = "1">
        <?php
         //Display Rating Stars
        if ( get_post_meta( $post->ID, '_bressman_review_rating', true ) ) {
            $reviewRating = get_post_meta( $post->ID, '_bressman_review_rating', true );
            $reviewRatingEmpty = 5 - $reviewRating;

            for($i = 0; $i < intval($reviewRating); $i++){
                echo '<img src="'.CHILD_URL.'/assets/app/images/review-star.png" alt="Star" class="review-star no-border" /> ';
            }
            for($i = 0; $i < intval($reviewRatingEmpty); $i++){
                echo '<img src="'.CHILD_URL.'/assets/app/images/review_star_empty.png" alt="Star" class="review-star no-border" /> ';
            }
        }
        ?>
        <?php
           //Output Meta Rating
            if ( get_post_meta( $post->ID, '_bressman_review_rating', true ) ) {
                $reviewRating = get_post_meta( $post->ID, '_bressman_review_rating', true );
                echo '<meta itemprop="ratingValue" content="'.$reviewRating.'">
                     <meta itemprop="bestRating" content="5">';
            }
        ?>
    </div> <?php //end rating stars ?>
    </div> <?php //end .review-name ?>

      <div class="review-date">
         <?php
             echo 'Posted '. human_time_diff( get_the_date( 'U' ) ) . ' ago';
          ?>
      </div>

  <?php
  //Display Feedback
  if ( get_post_meta( $post->ID, '_bressman_review_feedback', true ) ) {
      ?>

      <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Service" class="item-reviewed">
          <meta itemprop="name" content="Legal Services">
      </div>

      <?php
      echo '<p class="review-content" itemprop="description">'.get_post_meta( $post->ID, '_bressman_review_feedback', true ).'</p>';
   }
  ?>
</div> <?php //end .review-right ?>
<hr class="review-list-seperator" />
</div> <?php //end .single-review ?>


<?php //END OF SINGLE REVIEW ?>

<?php endwhile; else : ?>
    <p>Sorry no reviews found.</p>
<?php endif; ?>
<?php wp_reset_query(); ?>

<?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('bressman-reviews', 'bressmanReviews');
?>
