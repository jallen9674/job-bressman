<?php

function bressmanLocations_rs($atts = null) {

    global $post;

    $locationCurrentPost = $post->ID;

    /*
    Get Current Location Taxonomy On Page
    */
    $postTerms =  wp_get_object_terms($post->ID, 'hc_location');
    
   //Widget Toggle
    if($postTerms !== false) :



    $main = ['dublin', 'cincinnati', 'columbus'];
    $main_ids = [];

    foreach($main as $m) {

      $m_page = current(get_posts([
        'post_type' => 'page',
        'meta_query' => array(
            array(
                'key'     => '_hc_location_widget_title',
                'value'   => get_field('_hc_location_widget_title', $post->ID),
                'compare' => '=',
            ),
        ),
        'tax_query' => array(
            array(
                'taxonomy' => 'hc_location',
                'field' => 'slug',
                'terms' => $m
            ),
        ),
      ]));

      if($m_page) {
        $main_ids[] = $m_page->ID;
      }

    }
    

    ob_start();

?>


    

            <?php //BEGIN LOOP FOR MATCHING PAGES ?>
            <?php

             $args = array(
                  'posts_per_page' => 10,
                  'post_type' => 'page',
                  'meta_query' => array(
                        array(
                            'key'     => '_hc_location_widget_title',
                            'value'   => get_field('_hc_location_widget_title', $post->ID),
                            'compare' => '=',
                        ),
                   ),
                  'orderby' => 'rand',
                  'order' => 'asc',
                  'post__not_in' => array_merge(array($post->ID), $main_ids)
                );

            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) :  ?>
      
      <div class="location-widget-outer">
        <div class="location-widget-inner">

              <span class="location-widget-title">
                Locations
              </span>
            
            <ul class="location-widget-links">

              <?php 
              foreach($main_ids as $mid)  {
                echo '<li><a href="'.get_permalink($mid).'">'.get_the_title($mid).'</a></li>';
              } ?>

              <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <li class="single-location-link">
                    <a href="<?php the_permalink(); ?>"><?=get_the_title();?></a>
                </li>
            <?php //END OUTPUT ?>
            <?php endwhile; ?>

              </ul>
          </div>
      </div>
            <?php else : ?>
            <!-- IF NOTHING FOUND CONTENT HERE -->
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <?php //END LOOP FOR MATCHING PAGES ?>



<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
	
	endif;
}


add_shortcode('bressman-locations-rs', 'bressmanLocations_rs');