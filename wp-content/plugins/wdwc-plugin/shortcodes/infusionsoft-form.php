<?php
/*---------------------------------
BEGIN INFUSIONSOFT CONTACT FORM

[wdwc-contact-form]
---------------------------------*/
function wdwcContactForm($atts = null) {

    global $post;

    ob_start();
    //BEGIN OUTPUT
?>

<form accept-charset="UTF-8" action="https://gr214.infusionsoft.com/app/form/process/2140a3a9a41965ec5344ad857af4d4e3" class="wdwc-contact-form infusion-form" method="POST">
    <input name="inf_form_xid" type="hidden" value="2140a3a9a41965ec5344ad857af4d4e3" />
    <input name="inf_form_name" type="hidden" value="New Contact Us&#a;web form" />
    <input name="infusionsoft_version" type="hidden" value="1.60.0.55" />
    <div class="infusion-field">
        <label for="inf_field_FirstName">Your Name *</label>
        <input class="infusion-field-input-container" id="inf_field_FirstName" name="inf_field_FirstName" type="text" />
    </div>
    <div class="infusion-field">
        <label for="inf_field_Email">Your Email *</label>
        <input class="infusion-field-input-container" id="inf_field_Email" name="inf_field_Email" type="text" />
    </div>
    <div class="infusion-field">
        <label for="inf_custom_ContactUsComments">Your Message: *</label>
        <textarea cols="24" id="inf_custom_ContactUsComments" name="inf_custom_ContactUsComments" rows="5">
    </textarea></div>

    <div class="captcha-wrapper">
        <div class="infusion-captcha">
            <div>
                <img alt="captcha" border="0px" name="captcha" onclick="reloadJcaptcha();" src="https://gr214.infusionsoft.com/Jcaptcha/img.jsp" title="If you can't read the image, click it to get another one." />
                <script type="text/javascript">function reloadJcaptcha() {var now = new Date();if (document.images) {document.images.captcha.src = 'https://gr214.infusionsoft.com/Jcaptcha/img.jsp?reload=' + now}}</script>
            </div>
            <div>
                <label for="captcha.typed">Enter the above code:</label>
                <span class="captcha-refresh">If you are unable to read the image, please click it to generate a new code.</span>
            </div>
            <div>
                <input class="infusion-field-input-container" id="captcha.typed" name="captcha.typed" type="text" />
            </div>
        </div>
    </div>

    <div class="infusion-submit">
        <input type="submit" value="Send Message" />
    </div>
</form>
<script type="text/javascript" src="https://gr214.infusionsoft.com/app/webTracking/getTrackingCode"></script>


<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('wdwc-contact-form', 'wdwcContactForm');

