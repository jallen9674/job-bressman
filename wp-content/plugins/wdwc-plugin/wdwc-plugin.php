<?php
/**
* Plugin Name: WDWC Customizations
* Plugin URI: http://www.wedowebcontent.com/
* Description: Custom theme modifications for Bressman Law
* Version: 1.0
* Author: We Do Web Content
* Author URI: https://www.wedowebcontent.com
*/


/*---------------------------------
Enqueue of Styles
----------------------------------*/

function wdwc_load_styles(){
    //wp_enqueue_style( 'theme-style', plugin_dir_path( __FILE__ ) .'/style.css' );
    wp_enqueue_style( 'wdwc-css', plugin_dir_url( __FILE__ ) .'css/wdwc-style.css' );
}
add_action( 'wp_enqueue_scripts', 'wdwc_load_styles', 10000);


/*---------------------------------
Enqueue of Optimized Scripts
----------------------------------*/

function wdwc_load_scripts(){
    wp_enqueue_script('wdwc-js', plugin_dir_url( __FILE__ ) . 'js/wdwc-scripts.js', array('jquery'), '1.0', true);
    //wp_enqueue_script( 'comment-reply' );
}
add_action( 'wp_enqueue_scripts', 'wdwc_load_scripts', 40);


/*---------------------------------
Bringing in Shortcodes
----------------------------------*/

require_once('shortcodes/infusionsoft-form.php');




/*---------------------------------
Adding Geo Tagging
----------------------------------*/
function wdwc_add_geo() {
?>

<meta name="zipcode" content="43016, 43017, 43002, 43004, 43016, 43017, 43026, 43035, 43054, 43065, 43068, 43081, 43082, 43085, 43109, 43110, 43119, 43123, 43125, 43137, 43147, 43201, 43202, 43203, 43204, 43205, 43206, 43207, 43209, 43210, 43211, 43212, 43213, 43214, 43215, 43217, 43219, 43220, 43221, 43222, 43223, 43224, 43227, 43228, 43229, 43230, 43231, 43232, 43235, 43240" />
<meta name="city" content="dublin, westerville, columbus, grove city, gahanna" />
<meta name="state" content="ohio" />
<meta name="country" content="usa, united states of america" />
<meta name="geo.region" content="US-OH" />
<meta name="geo.placename" content="Dublin" />
<meta name="geo.position" content="40.089229;-83.127907" />
<meta name="ICBM" content="40.089229, -83.127907" />

<?php
}

// add_action('wp_head', 'wdwc_add_geo', 10);
