/*****************************************
Begin JS for Site
*****************************************/

jQuery(document).ready(function($) {


    /*-----------------------------------
    Infusionsoft Captcha Toggle
    ------------------------------------*/

    $('.wdwc-contact-form').on("click", function(e){
        $(this).find('.captcha-wrapper').addClass('visible');
        e.stopPropagation();
    });

    $(window).click(function() {
        $('.captcha-wrapper').removeClass('visible');
    });


}); /* end of as page load scripts */